<?php

namespace common\modules\vis\models;

use Yii;
use common\models\Discipline;
use common\models\Program;
use common\models\Grade;

/**
 * This is the model class for table "vis_discipline_map".
 *
 * @property integer $dm_id
 * @property integer $dm_vis_id
 * @property integer $dm_lms_id
 * @property integer $dm_grade_id
 *
 * @property Discipline $discipline
 * @property Grade $grade
 */
class DisciplineMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vis_discipline_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dm_vis_id', 'dm_lms_id'], 'required'],
            [['dm_vis_id', 'dm_lms_id', 'dm_grade_id'], 'integer'],
            [['dm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['dm_lms_id' => 'd_id']],
            [['dm_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['dm_grade_id' => 'g_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dm_id' => Yii::t('vis', 'Dm ID'),
            'dm_vis_id' => Yii::t('vis', 'Dm Vis ID'),
            'dm_lms_id' => Yii::t('vis', 'Dm Lms ID'),
            'dm_grade_id' => Yii::t('vis', 'Dm Grade ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['d_id' => 'dm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['g_id' => 'dm_grade_id']);
    }
}
