<?php

namespace common\modules\vis\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\vis\models\ProgramMap;

/**
 * ProgramMapSearch represents the model behind the search form about `common\modules\vis\models\ProgramMap`.
 */
class ProgramMapSearch extends ProgramMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pm_id', 'pm_lms_id'], 'integer'],
            // for use compare operators
            ['pm_vis_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = ProgramMap::find()->joinWith('program');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'pm_vis_id',
                    'pm_lms_id' => [
                        'asc' => ['program.p_name' => SORT_ASC],
                        'desc' => ['program.p_name' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => ['pm_lms_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pm_id' => $this->pm_id,
            'pm_lms_id' => $this->pm_lms_id,
        ]);

        $query->andFilterCompare('pm_vis_id', $this->pm_vis_id);

        return $dataProvider;
    }
}
