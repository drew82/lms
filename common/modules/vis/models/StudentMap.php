<?php

namespace common\modules\vis\models;

use Yii;
use common\models\Student;

/**
 * This is the model class for table "vis_student_map".
 *
 * @property integer $sm_id
 * @property integer $sm_vis_id
 * @property integer $sm_lms_id
 * 
 * @property Student $student
 */
class StudentMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vis_student_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sm_vis_id', 'sm_lms_id'], 'required'],
            [['sm_vis_id', 'sm_lms_id'], 'integer'],
            [['sm_vis_id'], 'unique'],
            [['sm_lms_id'], 'unique'],
            [['sm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['sm_lms_id' => 's_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sm_id' => Yii::t('vis', 'Sm ID'),
            'sm_vis_id' => Yii::t('vis', 'Sm Vis ID'),
            'sm_lms_id' => Yii::t('vis', 'Sm Lms ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['s_id' => 'sm_lms_id']);
    }
}
