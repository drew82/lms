<?php

namespace common\modules\vis\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\vis\models\DisciplineMap;

/**
 * DisciplineMapSearch represents the model behind the search form about `common\modules\vis\models\DisciplineMap`.
 */
class DisciplineMapSearch extends DisciplineMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dm_id', 'dm_lms_id', 'dm_grade_id'], 'integer'],
            // for use compare operators
            ['dm_vis_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = DisciplineMap::find()->joinWith(['discipline', 'grade.program']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'dm_vis_id',
                    'dm_lms_id' => [
                        'asc' => [
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_code' => SORT_ASC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH([[grade.g_name]])' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'discipline.d_name' => SORT_DESC,
                            'discipline.d_code' => SORT_DESC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH([[grade.g_name]])' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                    ],
                    'dm_grade_id' => [
                        'asc' => [
                            'program.p_name' => SORT_ASC,
                            'LENGTH([[grade.g_name]])' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_code' => SORT_ASC,
                        ],
                        'desc' => [
                            'program.p_name' => SORT_DESC,
                            'LENGTH([[grade.g_name]])' => SORT_DESC,
                            'grade.g_name' => SORT_DESC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_code' => SORT_ASC,
                        ],
                    ],
                ],
                'defaultOrder' => ['dm_lms_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dm_id' => $this->dm_id,
            'dm_lms_id' => $this->dm_lms_id,
            'dm_grade_id' => $this->dm_grade_id,
        ]);
        
        if ('-' == $this->dm_vis_id) {
            $query->andWhere(['dm_vis_id' => null]);
        } else {
            $query->andFilterCompare('dm_vis_id', $this->dm_vis_id);
        }

        return $dataProvider;
    }
}
