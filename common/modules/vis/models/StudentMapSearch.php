<?php

namespace common\modules\vis\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\vis\models\StudentMap;

/**
 * StudentMapSearch represents the model behind the search form about `common\modules\vis\models\StudentMap`.
 */
class StudentMapSearch extends StudentMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sm_id', 'sm_lms_id'], 'integer'],
            // for use compare operators
            ['sm_vis_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = StudentMap::find()->joinWith('student');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'sm_vis_id',
                    'sm_lms_id' => [
                        'asc' => [
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                        ],
                        'desc' => [
                            'student.s_name_f' => SORT_DESC,
                            'student.s_name_i' => SORT_DESC,
                            'student.s_name_o' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => ['sm_lms_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sm_id' => $this->sm_id,
            'sm_lms_id' => $this->sm_lms_id,
        ]);

        $query->andFilterCompare('sm_vis_id', $this->sm_vis_id);

        return $dataProvider;
    }
}
