<?php

namespace common\modules\vis\models;

use Yii;
use common\models\Program;

/**
 * This is the model class for table "vis_program_map".
 *
 * @property integer $pm_id
 * @property integer $pm_vis_id
 * @property integer $pm_lms_id
 * 
 * @property Program $program
 */
class ProgramMap extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vis_program_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pm_vis_id', 'pm_lms_id'], 'required'],
            [['pm_vis_id', 'pm_lms_id'], 'integer'],
            [['pm_lms_id'], 'unique'],
            [['pm_vis_id'], 'unique'],
            [['pm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['pm_lms_id' => 'p_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pm_id' => Yii::t('vis', 'Pm ID'),
            'pm_vis_id' => Yii::t('vis', 'Pm Vis ID'),
            'pm_lms_id' => Yii::t('vis', 'Pm Lms ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['p_id' => 'pm_lms_id']);
    }
}
