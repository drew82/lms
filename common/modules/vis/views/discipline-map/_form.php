<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\DisciplineMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discipline-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dm_vis_id')->textInput() ?>

    <?= $form->field($model, 'dm_lms_id')->dropDownList(\common\models\Discipline::listAll())->label(Yii::t('app', 'D Name')) ?>

    <?= $form->field($model, 'dm_grade_id')->dropDownList(\common\models\Grade::listAll())->label(Yii::t('app', 'G Name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
