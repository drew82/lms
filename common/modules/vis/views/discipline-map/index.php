<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\vis\models\DisciplineMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('vis', 'Discipline Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discipline-map-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'dm_vis_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'dm_lms_id',
                'value' => 'discipline.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'dm_lms_id',
                    'data' => \common\models\Discipline::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
            ],
            [
                'attribute' => 'dm_grade_id',
                'value' => function ($data) {
                    return $data->dm_grade_id ? $data->grade->name : Yii::t('vis', '=== ALL ===');
                },
                'filter' => \common\models\Grade::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
