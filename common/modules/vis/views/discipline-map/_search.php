<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\DisciplineMapSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discipline-map-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dm_id') ?>

    <?= $form->field($model, 'dm_vis_id') ?>

    <?= $form->field($model, 'dm_lms_id') ?>

    <?= $form->field($model, 'dm_grade_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
