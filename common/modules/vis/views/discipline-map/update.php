<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\DisciplineMap */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Discipline Map',
]) . $model->dm_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('vis', 'Discipline Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dm_id, 'url' => ['view', 'id' => $model->dm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="discipline-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
