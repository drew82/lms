<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\ProgramMap */

$this->title = Yii::t('vis', 'Create Program Map');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('vis', 'Program Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-map-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
