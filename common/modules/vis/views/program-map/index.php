<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\vis\models\ProgramMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('vis', 'Program Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-map-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'pm_vis_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'pm_lms_id',
                'value' => 'program.p_name',
                'filter' => \common\models\Program::listAll(),
                'label' => Yii::t('app', 'Pg Name'),
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
