<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\ProgramMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pm_vis_id')->textInput() ?>

    <?= $form->field($model, 'pm_lms_id')->dropDownList(\common\models\Program::listAll())->label(Yii::t('app', 'Pg Name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
