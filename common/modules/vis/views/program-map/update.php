<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\vis\models\ProgramMap */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Program Map',
]) . $model->pm_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('vis', 'Program Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pm_id, 'url' => ['view', 'id' => $model->pm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="program-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
