<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\vis\models\StudentMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('vis', 'Student Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-map-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'sm_vis_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'sm_lms_id',
                'value' => 'student.nameFull',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'sm_lms_id',
                    'data' => \common\models\Student::listAll('nameFull'),
                ]),
                'label' => Yii::t('app', 'S Name Full'),
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
