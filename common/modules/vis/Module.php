<?php

namespace common\modules\vis;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\vis\controllers';

    /**
     * @var \yii\db\Connection
     */
    public $db;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (YII_ENV_DEV) {
            Yii::warning('init  ' . $this->id);
        }
        
        parent::init();

        $this->registerTranslations();

        $this->db = \Yii::createObject($this->db);
    }
    
    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;

        if (!isset($i18n->translations['vis*'])) {
            $i18n->translations['vis*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }
    
    public function getName()
    {
        return $this->id;
    }

    /**
     * Return moodle course id
     * @param integer $visId VIS discipline id
     * @param string $mdlInstance moodle module id
     * @return integer or null
     */
    public function getMdlCourseId($visId, $mdlInstance)
    {
        $sql = 'SELECT * FROM {{%disci}} WHERE [[id]]=:id';
        $rst = $this->db->createCommand($sql)->bindValue(':id', $visId)->queryOne();
        
        if ($rst) {
            switch ($mdlInstance) {
                case 'mdl1':
                    return $rst['id_dis_moodle']>0 ? $rst['id_dis_moodle'] : null;
                case 'mdl2':
                    return $rst['id_dis_umcvpo']>0 ? $rst['id_dis_umcvpo'] : null;
            }
        }

        return null;
    }

    /**
     * Return moodle student id
     * @param integer $visId VIS student id
     * @param string $mdlInstance moodle module id
     * @return integer or null
     */
    public function getMdlStudentId($visId, $mdlInstance)
    {
        $sql = 'SELECT * FROM {{%students}} WHERE [[id]]=:id';
        $rst = $this->db->createCommand($sql)->bindValue(':id', $visId)->queryOne();
        
        if ($rst) {
            switch ($mdlInstance) {
                case 'mdl1':
                    return $rst['id_moodle2']>0 ? $rst['id_moodle2'] : null;
                case 'mdl2':
                    return $rst['id_umc']>0 ? $rst['id_umc'] : null;
            }
        }

        return null;
    }
}
