<?php

namespace common\modules\vis\controllers;

use yii\console\Exception;
use yii\helpers\VarDumper;

use common\models\Student;
use common\modules\vis\models\StudentMap;
use common\models\Program;
use common\modules\vis\models\ProgramMap;
use common\models\Plan;
use common\models\Discipline;
use common\models\Grade;
use common\models\Form;
use common\models\FormType;
use common\modules\vis\models\DisciplineMap;
use common\models\Daterange;
use common\models\Group;
use common\models\Raise;

/**
 * Import data from VIS DB (dao)
 * 
 * @property \common\modules\vis\Module $module
 */
class ImportController extends \yii\console\Controller
{

    /**
     * Import programs; map to vis
     */
    public function actionProgram()
    {
        $rst = $this->module->db->createCommand('SELECT * FROM {{%plans}}')->queryAll();

        foreach ($rst as $r) {
            // log
            printf("[%2d] %s\n", $r['id'], $r['discr']);
            // if mapping doesn't exist
            if (!ProgramMap::find()->where(['pm_vis_id' => $r['id']])->exists()) {
                // create program
                $program = new Program();
                $program->p_name = $r['discr'];
                $this->_checkSave($program);
                // map to vis
                $pm = new ProgramMap();
                $pm->pm_vis_id = $r['id'];
                $pm->pm_lms_id = $program->p_id;
                $this->_checkSave($pm);
            }
        }
    }

    /**
     * Import plan (grades, disciplines, qty); map discipline to vis
     */
    public function actionPlan()
    {
        $sql = 'SELECT * FROM {{%disci}} WHERE [[id]] IN (SELECT [[id_dis]] FROM {{%plans_dis}} WHERE [[id_plan]]=:plan)';
        // all mapped programs
        foreach (ProgramMap::find()->all() as $pm) {
            /* @var $pm ProgramMap */
            printf("=======\nprogram [%4d] %s\n=======\n", $pm->pm_lms_id, $pm->program->p_name);
            // get disciplines with plan inline
            $rst = $this->module->db->createCommand($sql)->bindValue(':plan', $pm->pm_vis_id)->queryAll();
            foreach ($rst as $r) {
                $d = $this->_getDiscipline($r['numb_id'], $r['disciplina']);
                $g = $this->_getGrade($pm->pm_lms_id, $r['semestr']);
                printf("[%s] %s / %s\n", $g->program->p_name, $g->g_name, $d->name);
                // if mapping doesn't exist
                if (!DisciplineMap::find()->where(['dm_vis_id' => $r['id'], 'dm_grade_id' => $g->g_id])->exists()) {
                    // map to vis
                    $dm = new DisciplineMap();
                    $dm->dm_vis_id = $r['id'];
                    $dm->dm_lms_id = $d->d_id;
                    $dm->dm_grade_id = $g->g_id;
                    $this->_checkSave($dm);
                    // fix clock in weeks
                    if (preg_match('%^(\S+)\s+нед%ui', $r['clock'], $out))
                        $r['clock'] = $out[1] * 40;
                    if (2109 == $r['id'])
                        $r['clock'] = 1.3 * 40;
                    // create plans
                    if ($r['clock'] > 0)
                        $f = $this->_createPlan($g, $d, $this->_getForm('ч.всего'), $r['clock']);
                    if ($r['clock_auditory'] > 0)
                        $f = $this->_createPlan($g, $d, $this->_getForm('ч.ауд.'), $r['clock_auditory']);
                    if ($r['kontrol_1'])
                        $f = $this->_createPlan($g, $d, $this->_getForm($r['kontrol_1']));
                    if ($r['kontrol_2'])
                        $f = $this->_createPlan($g, $d, $this->_getForm($r['kontrol_2']));
                    if ($r['kontrol_3'])
                        $f = $this->_createPlan($g, $d, $this->_getForm($r['kontrol_3']));
                    if ($r['itog_kontrol'])
                        $f = $this->_createPlan($g, $d, $this->_getForm($r['itog_kontrol']));
                }
            }
        }
    }

    /**
     * Import students; map to vis
     */
    public function actionStudent()
    {
        $rst = $this->module->db->createCommand('SELECT * FROM {{%students}}')->queryAll();

        foreach ($rst as $r) {
            // log
            printf("[%4d] %s %s %s\n", $r['id'], $r['fname'], $r['name'], $r['faname']);
            // if mapping doesn't exist
            if (!StudentMap::find()->where(['sm_vis_id' => $r['id']])->exists()) {
                // create student
                $student = new Student();
                $student->s_name_f = $r['fname'];
                $student->s_name_i = $r['name'];
                $student->s_name_o = $r['faname'] ? $r['faname'] : null;
                $student->s_email = $r['email'];
                $student->s_mobile = preg_replace('%^8%', '+7', $r['tel_mob']);
                $this->_checkSave($student);
                // map to vis
                $sm = new StudentMap();
                $sm->sm_vis_id = $r['id'];
                $sm->sm_lms_id = $student->s_id;
                $this->_checkSave($sm);
            }
        }
    }

    public function actionStudentUpdate()
    {
        /* @var $sm StudentMap */
        foreach (StudentMap::find()->with('student')->all() as $sm) {
            $visStudent = $this->module->db->createCommand('SELECT * FROM {{%students}} WHERE [[id]]=:id')->bindValue(':id', $sm->sm_vis_id)->queryOne();
            $s = $sm->student;
            // email
            $s->s_email = trim($visStudent['email']);
            if (!$s->validate(['s_email'])) {
                $s->s_email = null;
            }
            // phone
            $s->s_mobile = trim(preg_replace('%^8%', '+7', $visStudent['tel_mob']));
            if (!$s->validate(['s_mobile'])) {
                $s->s_mobile = null;
            }
            // update
            $this->_checkSave($s);
        }
    }

    /**
     * Import student's raises
     */
    public function actionRaise($correct = 1)
    {
        $rst = $this->module->db->createCommand('SELECT * FROM {{%sostoyanie}} WHERE [[correct]]=:correct ORDER BY [[dt]] DESC')->bindValue(':correct', $correct)->queryAll();

        foreach ($rst as $r) {
            // prevent or fix errors
            if (!$r['period'])
                continue;
            if (in_array($r['id'], [6968]))
                continue;
            // fix period
            $r['period'] = preg_replace('%^.*?(20[\d]{2}.+?)$%', '$1', $r['period']);
            $r['period'] = str_replace(' ', '', $r['period']);
            // fix vib_dis
            $r['vib_dis'] = preg_replace('%[^\d\-]%', '', $r['vib_dis']);
            if (!$r['vib_dis'])
                continue;

            // get vis student
            $vis_student = $this->module->db->createCommand('SELECT * FROM {{%students}} WHERE [[id]]=:id')->bindValue(':id', $r['id_st'])->queryOne();
            // log
            printf("sost=[%4d] stud=[%4d] plan=%d grade=%3s (%s %s %s)\n", $r['id'], $vis_student['id'], $vis_student['plan'], $r['vib_dis'], $vis_student['fname'], $vis_student['name'], $vis_student['faname']);
            // if student mapped
            if ($sm = StudentMap::findOne(['sm_vis_id' => $r['id_st']])) {
                // find program
                $pm = ProgramMap::findOne(['pm_vis_id' => $vis_student['plan']]);
                // find or create daterange
                $daterange = $this->_getDaterange($r['period']);
                // find or create group
                $group = $this->_getGroup($r['study_group'] ? $r['study_group'] : '-');
                // if few grades in one daterange
                if (strpos($r['vib_dis'], '-')) {
                    list($g_start, $g_end) = explode('-', $r['vib_dis']);
                    $grades = range($g_start, $g_end);
                // if one grade
                } else {
                    $grades = [$r['vib_dis']];
                }
                foreach ($grades as $g) {
                    // find grade
                    $grade = Grade::findOne(['g_program_id' => $pm->pm_lms_id, 'g_name' => $g]);
                    // create raise if not exist
                    $this->_createRaise($sm->sm_lms_id, $grade, $group, $daterange);
                }
            }
        }
    }

    protected function _createPlan(Grade $g, Discipline $d, Form $f, $qty = 0)
    {
        $p = new Plan();
        $p->p_grade_id = $g->g_id;
        $p->p_discipline_id = $d->d_id;
        $p->p_form_id = $f->f_id;
        $p->p_qty = $qty;
        $this->_checkSave($p);
    }

    protected function _getDiscipline($code, $name)
    {
        $d = Discipline::findOne([
                'd_code' => $code,
                'd_name' => $name,
        ]);

        if (!$d) {
            $d = new Discipline();
            $d->d_code = $code;
            $d->d_name = $name;
            $this->_checkSave($d);
        }

        return $d;
    }

    protected function _getGrade($prorgam_id, $name)
    {
        $g = Grade::findOne([
                'g_program_id' => $prorgam_id,
                'g_name' => $name,
        ]);

        if (!$g) {
            $g = new Grade();
            $g->g_program_id = $prorgam_id;
            $g->g_name = $name;
            $this->_checkSave($g);
        }

        return $g;
    }

    protected function _getForm($name)
    {
        $f = Form::findOne(['f_name' => $name]);

        if (!$f) {
            $f = new Form();
            $f->f_type_id = FormType::find()->orderBy('t_id')->one()->t_id;
            $f->f_name = $name;
            $this->_checkSave($f);
        }

        return $f;
    }

    protected function _getGroup($name)
    {
        $g = Group::findOne([
                'g_name' => $name,
        ]);

        if (!$g) {
            $g = new Group();
            $g->g_name = $name;
            $this->_checkSave($g);
        }

        return $g;
    }

    protected function _getDaterange($name)
    {
        $dr = Daterange::findOne(['d_name' => $name]);

        if (!$dr) {
            $dr = new Daterange();
            $dr->d_name = $name;
            $dr->d_from = '2000-01-01';
            $dr->d_till = '2000-01-02';
            $this->_checkSave($dr);
        }

        return $dr;
    }

    protected function _createRaise($s, Grade $g, Group $gp, Daterange $d)
    {
        if (!Raise::find()->where([
                'r_student_id' => $s,
                'r_grade_id' => $g->g_id,
//                'r_group_id' => $gp->g_id,
                'r_daterange_id' => $d->d_id,
            ])->exists()) {
            $r = new Raise();
            $r->r_student_id = $s;
            $r->r_grade_id = $g->g_id;
            $r->r_group_id = $gp->g_id;
            $r->r_daterange_id = $d->d_id;
            $this->_checkSave($r);
        }
    }

    protected function _checkSave(\yii\db\ActiveRecord $model)
    {
        $text = $model->isNewRecord ? 'New %s created:' : '%s updated: ';

        if (!$model->save()) {
            throw new Exception(VarDumper::dumpAsString($model->attributes) . VarDumper::dumpAsString($model->getErrors()));
        }

        error_log(sprintf($text . "%s\n", $model->className(), VarDumper::dumpAsString($model->attributes)));
    }
}
