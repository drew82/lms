<?php

namespace common\modules\mdl\models;

/**
 * @property DisciplineMap $map
 */
class Discipline extends \common\models\Discipline
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMap()
    {
        return $this->hasOne(DisciplineMap::className(), ['dm_lms_id' => 'd_id']);
    }

}
