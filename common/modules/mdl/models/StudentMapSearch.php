<?php

namespace common\modules\mdl\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mdl\models\StudentMap;

/**
 * StudentMapSearch represents the model behind the search form about `common\modules\mdl\models\StudentMap`.
 */
class StudentMapSearch extends StudentMap
{
    public $grade_id;
    public $daterange_id;
    /**
     * @var boolean query and show mdl student courses
     */
    public $show_courses;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sm_id', 'sm_lms_id'], 'integer'],
            // for use compare operators
            ['sm_mdl_id', 'safe'],
            //
            [['grade_id', 'daterange_id'], 'integer'],
            //
            ['show_courses', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = StudentMap::find()->joinWith(['student.raises', 'student.raises.daterange', 'student.raises.grade.program'])->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'sm_mdl_id',
                    'sm_lms_id' => [
                        'asc' => [
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                        ],
                        'desc' => [
                            'student.s_name_f' => SORT_DESC,
                            'student.s_name_i' => SORT_DESC,
                            'student.s_name_o' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => ['sm_lms_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sm_id' => $this->sm_id,
            'sm_lms_id' => $this->sm_lms_id,
        ]);

        if ('-' == $this->sm_mdl_id) {
            $query->andWhere(['sm_mdl_id' => null]);
        } else {
            $query->andFilterCompare('sm_mdl_id', $this->sm_mdl_id);
        }

        $query->andFilterWhere(['raise.r_grade_id' => $this->grade_id])
            ->andFilterWhere(['raise.r_daterange_id' => $this->daterange_id]);

        return $dataProvider;
    }
}
