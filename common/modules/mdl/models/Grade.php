<?php

namespace common\modules\mdl\models;

use common\models\Plan;

class Grade extends \common\models\Grade
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['d_id' => 'p_discipline_id'])->viaTable(Plan::tableName(), ['p_grade_id' => 'g_id']);
    }
}
