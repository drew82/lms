<?php

namespace common\modules\mdl\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Program;
use common\models\Grade;

/**
 * This is the model class for table "mdl_program_map".
 *
 * @property integer $pm_id
 * @property integer $pm_mdl_id
 * @property integer $pm_lms_id
 *
 * @property Program $program
 * 
 * @property GradeMap[] $gradesMap
 */
class ProgramMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_program_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pm_mdl_id', 'pm_lms_id'], 'integer'],
            [['pm_lms_id'], 'required'],
            [['pm_mdl_id'], 'unique'],
            [['pm_lms_id'], 'unique'],
            [['pm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['pm_lms_id' => 'p_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pm_id' => Yii::t('mdl', 'Pm ID'),
            'pm_mdl_id' => Yii::t('mdl', 'Pm Mdl ID'),
            'pm_lms_id' => Yii::t('mdl', 'Pm Lms ID'),
        ];
    }

    public function init()
    {
        // create nested grades mapping
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'createGradesMap']);
        // delete nested grades mapping
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'deleteGradesMap']);


        parent::init();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['p_id' => 'pm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradesMap()
    {
        return $this->hasMany(GradeMap::className(), ['gm_lms_id' => 'g_id'])->viaTable(Grade::tableName(), ['g_program_id' => 'pm_lms_id']);
    }

    protected function createGradesMap()
    {
        foreach (Program::findOne(['p_id' => $this->pm_lms_id])->grades as $g) {
            // if not exist
            if (!GradeMap::find()->where(['gm_lms_id' => $g->g_id])->exists()) {
                $gm = new GradeMap();
                $gm->gm_lms_id = $g->g_id;
                $gm->save();
            }
        }
    }

    protected function deleteGradesMap()
    {
        // delete nested where mdlID is not set
        foreach ($this->getGradesMap()->where(['gm_mdl_id' => null])->all() as $gm) {
            $gm->delete();
        }
    }
}
