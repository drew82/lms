<?php

namespace common\modules\mdl\models;

use Yii;
use common\models\Discipline;
use common\models\Grade;

/**
 * This is the model class for table "{{%discipline_map}}".
 *
 * @property integer $dm_id
 * @property integer $dm_mdl_id
 * @property integer $dm_lms_id
 * @property integer $dm_grade_id
 *
 * @property Discipline $discipline
 * @property Grade $grade
 * @property Enrollment[] $enrollments
 */
class DisciplineMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_discipline_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dm_lms_id'], 'required'],
            [['dm_mdl_id', 'dm_lms_id', 'dm_grade_id'], 'integer'],
            [['dm_lms_id', 'dm_grade_id'], 'unique', 'targetAttribute' => ['dm_lms_id', 'dm_grade_id'], 'message' => 'The combination of Dm Lms ID and Dm Grade ID has already been taken.'],
            [['dm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['dm_lms_id' => 'd_id']],
            [['dm_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['dm_grade_id' => 'g_id']],
            // set mdlId using vis model
            ['dm_mdl_id', 'default', 'value' => function ($model, $attributes) {
                    $visDm = \common\modules\vis\models\DisciplineMap::findOne(['dm_lms_id' => $model->dm_lms_id, 'dm_grade_id' => $model->dm_grade_id]);
                    return $visDm ? $this->module->getVisDisciplineMdlId($visDm->dm_vis_id) : null;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dm_id' => Yii::t('mdl', 'Dm ID'),
            'dm_mdl_id' => Yii::t('mdl', 'Dm Mdl ID'),
            'dm_lms_id' => Yii::t('mdl', 'Dm Lms ID'),
            'dm_grade_id' => Yii::t('mdl', 'Dm Grade ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['d_id' => 'dm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['g_id' => 'dm_grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnrollments()
    {
        return $this->hasMany(Enrollment::className(), ['e_dm_id' => 'dm_id']);
    }

    public function createMdlGroupingAndGroup($name)
    {
        // create grouping
        $groupingId = $this->client->createGrouping($this->dm_mdl_id, $name);
        // create group
        $groupId = $this->client->createGroup($this->dm_mdl_id, $name);
        // add group to grouping
        return $this->client->groupingAddGroup($groupingId, $groupId);
    }

    /**
     * Get mdl course groupings
     * @return array
     */
    public function getMdlGroupings()
    {
        return $this->dm_mdl_id ? $this->client->getCourseGroupings($this->dm_mdl_id) : [];
    }

    /**
     * Get mdl course groups
     * @return array
     */
    public function getMdlGroups()
    {
        return $this->dm_mdl_id ? $this->client->getCourseGroups($this->dm_mdl_id) : [];
    }

    public static function listAll($field = 'discipline.name')
    {
        $query = self::find()->joinWith(['discipline', 'grade.program'])->groupBy('dm_lms_id')
                             ->orderBy(['discipline.d_name' => SORT_ASC, 'discipline.d_natsort' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'dm_lms_id', $field);
    }
}
