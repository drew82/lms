<?php

namespace common\modules\mdl\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mdl\models\Enrollment;

/**
 * EnrollmentSearch represents the model behind the search form about `common\modules\mdl\models\Enrollment`.
 */
class EnrollmentSearch extends Enrollment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_id', 'e_sm_id', 'e_dm_id', 'e_is_enroll'], 'integer'],
            // virtual attributes
            [['student_id', 'discipline_id', 'grade_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Enrollment::find()->joinWith(['studentMap.student', 'disciplineMap.discipline', 'disciplineMap.grade.program']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'student_id' => [
                        'asc' => [
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH(grade.g_name)' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_natsort' => SORT_ASC,
                        ],
                        'desc' => [
                            'student.s_name_f' => SORT_DESC,
                            'student.s_name_i' => SORT_DESC,
                            'student.s_name_o' => SORT_DESC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH(grade.g_name)' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_natsort' => SORT_ASC,
                        ],
                    ],
                    'discipline_id' => [
                        'asc' => [
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_natsort' => SORT_ASC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH(grade.g_name)' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'discipline.d_name' => SORT_DESC,
                            'discipline.d_natsort' => SORT_DESC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'program.p_name' => SORT_ASC,
                            'LENGTH(grade.g_name)' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                    ],
                    'grade_id' => [
                        'asc' => [
                            'program.p_name' => SORT_ASC,
                            'LENGTH(grade.g_name)' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_natsort' => SORT_ASC,
                        ],
                        'desc' => [
                            'program.p_name' => SORT_DESC,
                            'LENGTH(grade.g_name)' => SORT_DESC,
                            'grade.g_name' => SORT_DESC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                            'discipline.d_natsort' => SORT_ASC,
                        ],
                    ],
                ],
                'defaultOrder' => ['student_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'e_id' => $this->e_id,
            'e_sm_id' => $this->e_sm_id,
            'e_dm_id' => $this->e_dm_id,
            'e_is_enroll' => $this->e_is_enroll,
        ]);

        $query->andFilterWhere([
            'sm_lms_id' => $this->student_id,
            'dm_lms_id' => $this->discipline_id,
            'dm_grade_id' => $this->grade_id,
        ]);

        return $dataProvider;
    }
}
