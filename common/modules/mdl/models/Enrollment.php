<?php

namespace common\modules\mdl\models;

use Yii;

/**
 * This is the model class for table "mdl_enrollment".
 *
 * @property integer $e_id
 * @property integer $e_sm_id
 * @property integer $e_dm_id
 * @property integer $e_is_enroll
 *
 * @property StudentMap $studentMap
 * @property DisciplineMap $disciplineMap
 */
class Enrollment extends ActiveRecord
{
    /**
     * @event Event an event that is triggered after enrollment is processed by mdl.
     */
    const EVENT_AFTER_PROCESS = 'afterProcess';

    // virtual attributes to be resolved in e_sm_id and e_dm_id
    public $student_id;
    public $discipline_id;
    public $grade_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_enrollment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // virtual attributes
            [['student_id', 'discipline_id', 'grade_id'], 'integer'],
            // set e_dm_id by discipline_id and grade_id
            ['e_dm_id', 'default', 'value' => function ($model, $attributes) {
                    $dm = DisciplineMap::findOne(['dm_lms_id' => $model->discipline_id, 'dm_grade_id' => $model->grade_id]);
                    return $dm ? $dm->dm_id : null;
            }],
            // set e_sm_id by student_id
            ['e_sm_id', 'default', 'value' => function ($model, $attributes) {
                    $sm = StudentMap::findOne(['sm_lms_id' => $model->student_id]);
                    return $sm ? $sm->sm_id : null;
            }],
            [['e_sm_id', 'e_dm_id', 'e_is_enroll'], 'required'],
            [['e_sm_id', 'e_dm_id'], 'integer'],
            [['e_is_enroll'], 'boolean'],
            [['e_sm_id', 'e_dm_id'], 'unique', 'targetAttribute' => ['e_sm_id', 'e_dm_id'], 'message' => 'The combination of E Sm ID, E Dm ID has already been taken.'],
            [['e_sm_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentMap::className(), 'targetAttribute' => ['e_sm_id' => 'sm_id']],
            [['e_dm_id'], 'exist', 'skipOnError' => true, 'targetClass' => DisciplineMap::className(), 'targetAttribute' => ['e_dm_id' => 'dm_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'e_id' => Yii::t('mdl', 'E ID'),
            'e_sm_id' => Yii::t('mdl', 'E Sm ID'),
            'e_dm_id' => Yii::t('mdl', 'E Dm ID'),
            'e_is_enroll' => Yii::t('mdl', 'E Is Enroll'),
        ];
    }

    public function init()
    {
        // add/remove groups
        $this->on(self::EVENT_AFTER_PROCESS, [$this, 'processGroups']);

        parent::init();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentMap()
    {
        return $this->hasOne(StudentMap::className(), ['sm_id' => 'e_sm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplineMap()
    {
        return $this->hasOne(DisciplineMap::className(), ['dm_id' => 'e_dm_id']);
    }

    public function process()
    {
        // enroll
        $this->client->enroll($this->studentMap->sm_mdl_id, $this->disciplineMap->dm_mdl_id, $this->module->roleStudent, !$this->e_is_enroll);
        // trigger an event
        $this->trigger(self::EVENT_AFTER_PROCESS);
        // delete after processing
        $this->delete();
    }

    protected function processGroups()
    {
        // get course groups
        $courseGroups = $this->disciplineMap->getMdlGroups();

        // enroll
        if ($this->e_is_enroll) {
            // add to default user groups
            foreach ($this->module->defaultUserGroups as $gName) {
                // if group exist in course
                if ($group = $this->_findGroupByName($courseGroups, $gName)) {
                    $this->studentMap->addToMdlGroup($group['id']);
                // group not exist
                } else {
                    throw new \yii\base\Exception(sprintf('Group "%s" does not exist in course id=%d', $gName, $this->disciplineMap->dm_mdl_id));
                }
            }
        // unenroll
        } else {
            // delete from all course groups
            foreach ($courseGroups as $group) {
                $this->studentMap->deleleFromMdlGroup($group['id']);
            }
        }
    }

    protected function _findGroupByName(array $groups, $name)
    {
        foreach ($groups as $g) {
            if ($name == $g['name']) {
                return $g;
            }
        }

        return null;
    }
}
