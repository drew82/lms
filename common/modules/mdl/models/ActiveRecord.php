<?php

namespace common\modules\mdl\models;

/**
 * @property \common\modules\mdl\Module $module
 * @property \common\modules\mdl\RestClient $client
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    protected static $prefix;

    public static function setPrefix($prefix)
    {
        self::$prefix = $prefix;
    }

    public static function getPrefix()
    {
        return self::$prefix ? self::$prefix : \Yii::$app->controller->module->id;
    }

    /**
     * @return \common\modules\mdl\Module
     */
    public function getModule()
    {
        return \Yii::$app->controller->module;
    }

    public function getClient()
    {
        return $this->getModule()->client;
    }
}
