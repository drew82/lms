<?php

namespace common\modules\mdl\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\mdl\models\GradeMap;

/**
 * GradeMapSearch represents the model behind the search form about `common\modules\mdl\models\GradeMap`.
 */
class GradeMapSearch extends GradeMap
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gm_id', 'gm_lms_id'], 'integer'],
            // for use compare operators
            ['gm_mdl_id', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = GradeMap::find()->joinWith('grade.program');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'gm_mdl_id',
                    'gm_lms_id' => [
                        'asc' => [
                            'program.p_name' => SORT_ASC,
                            'LENGTH([[grade.g_name]])' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'program.p_name' => SORT_DESC,
                            'LENGTH([[grade.g_name]])' => SORT_DESC,
                            'grade.g_name' => SORT_DESC,
                        ],
                    ],
                ],
                'defaultOrder' => ['gm_lms_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'gm_id' => $this->gm_id,
            'gm_lms_id' => $this->gm_lms_id,
        ]);

        if ('-' == $this->gm_mdl_id) {
            $query->andWhere(['gm_mdl_id' => null]);
        } else {
            $query->andFilterCompare('gm_mdl_id', $this->gm_mdl_id);
        }

        return $dataProvider;
    }
}
