<?php

namespace common\modules\mdl\models;

use Yii;
use common\models\Grade;

/**
 * This is the model class for table "mdl_grade_map".
 *
 * @property integer $gm_id
 * @property integer $gm_mdl_id
 * @property integer $gm_lms_id
 *
 * @property Grade $grade
 * 
 * @property DisciplineMap[] $disciplinesMap
 */
class GradeMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_grade_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gm_lms_id'], 'required'],
            [['gm_mdl_id', 'gm_lms_id'], 'integer'],
            [['gm_mdl_id'], 'unique'],
            [['gm_lms_id'], 'unique'],
            [['gm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['gm_lms_id' => 'g_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gm_id' => Yii::t('mdl', 'Gm ID'),
            'gm_mdl_id' => Yii::t('mdl', 'Gm Mdl ID'),
            'gm_lms_id' => Yii::t('mdl', 'Gm Lms ID'),
        ];
    }

    public function init()
    {
        // create parent program mapping
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'createProgramMap']);
        // create nested disciplines mapping
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'createDisciplinesMap']);
        // delete nested disciplines mapping
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'deleteDisciplinesMap']);
        // create nested students mapping
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'createStudentsMap']);
        // delete nested students mapping
        $this->on(self::EVENT_AFTER_DELETE, [$this, 'deleteStudentsMap']);

        parent::init();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['g_id' => 'gm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinesMap()
    {
        return $this->hasMany(DisciplineMap::className(), ['dm_grade_id' => 'gm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentsMap()
    {
        return $this->hasMany(StudentMap::className(), ['sm_lms_id' => 'r_student_id'])->viaTable(\common\models\Raise::tableName(), ['r_grade_id' => 'gm_lms_id']);
    }

    protected function createProgramMap()
    {
        $program = $this->grade->program;
        // if (lmsID is not set) and (program mapping doesn't not exist)
        if (!$this->gm_mdl_id && !ProgramMap::find()->where(['pm_lms_id' => $program->p_id])->exists()) {
            $pm = new ProgramMap();
            $pm->pm_lms_id = $program->p_id;
            // disable parent trigger for programm mapping don't create nested grades mapping
            $pm->off(ProgramMap::EVENT_AFTER_INSERT);
            $pm->save();
        }
    }

    protected function createDisciplinesMap()
    {
        foreach (Grade::findOne(['g_id' => $this->gm_lms_id])->disciplines as $d) {
            // if not exist
            if (!DisciplineMap::find()->where([
                    'dm_lms_id' => $d->d_id,
                    'dm_grade_id' => $this->gm_lms_id,
                ])->exists()) {
                $dm = new DisciplineMap();
                $dm->dm_lms_id = $d->d_id;
                $dm->dm_grade_id = $this->gm_lms_id;
                $dm->save();
            }
        }
    }

    protected function deleteDisciplinesMap()
    {
        // delete nested where mdlID is not set
        foreach ($this->getDisciplinesMap()->where(['dm_mdl_id' => null])->all() as $dm) {
            $dm->delete();
        }
    }

    protected function createStudentsMap()
    {
        foreach (Grade::findOne(['g_id' => $this->gm_lms_id])->students as $s) {
            // if not exist
            if (!StudentMap::find()->where(['sm_lms_id' => $s->s_id])->exists()) {
                $sm = new StudentMap();
                $sm->sm_lms_id = $s->s_id;
                $sm->save();
            }
        }
    }

    public function updateStudentsMap()
    {
        $this->createStudentsMap();
    }

    protected function deleteStudentsMap()
    {
        // delete nested where mdlID is not set
        foreach ($this->getStudentsMap()->where(['sm_mdl_id' => null])->all() as $sm) {
            $sm->delete();
        }
    }

    public static function listAll($field = 'grade.g_name')
    {
        $query = static::find()->joinWith('grade.program')->orderBy(['program.p_name' => SORT_ASC, 'LENGTH([[grade.g_name]])' => SORT_ASC, 'grade.g_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'gm_lms_id', $field, 'grade.program.p_name');
    }
}
