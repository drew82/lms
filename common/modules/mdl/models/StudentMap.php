<?php

namespace common\modules\mdl\models;

use Yii;
use common\modules\mdl\models\ActiveRecord;
use common\models\Student;

/**
 * This is the model class for table "mdl_student_map".
 *
 * @property integer $sm_id
 * @property integer $sm_mdl_id
 * @property integer $sm_lms_id
 *
 * @property Student $student
 * @property Enrollment[] $enrollments
 */
class StudentMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_student_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // set sm_mdl_id using vis model if it's null
            ['sm_mdl_id', 'default', 'value' => function ($model, $attributes) {
                    $visSm = \common\modules\vis\models\StudentMap::findOne(['sm_lms_id' => $model->sm_lms_id]);
                    return $this->module->getVisStudentMdlId($visSm->sm_vis_id);
            }],
            [['sm_lms_id'], 'required'],
            [['sm_mdl_id', 'sm_lms_id'], 'integer'],
            [['sm_mdl_id'], 'unique'],
            [['sm_lms_id'], 'unique'],
            [['sm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['sm_lms_id' => 's_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sm_id' => Yii::t('mdl', 'Sm ID'),
            'sm_mdl_id' => Yii::t('mdl', 'Sm Mdl ID'),
            'sm_lms_id' => Yii::t('mdl', 'Sm Lms ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['s_id' => 'sm_lms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnrollments()
    {
        return $this->hasMany(Enrollment::className(), ['e_sm_id' => 'sm_id']);
    }

    public function getMdlCourses()
    {
        return $this->sm_mdl_id ? $this->client->getUserCourses($this->sm_mdl_id) : [];
    }

    public function addToMdlGroup($id)
    {
        return $this->client->addUserToGroup($this->sm_mdl_id, $id);
    }

    public function deleleFromMdlGroup($id)
    {
        return $this->client->deleteUserFromGroup($this->sm_mdl_id, $id);
    }

    public static function listAll($field = 'student.nameFull')
    {
        $query = self::find()->joinWith('student')->orderBy(['student.s_name_f' => SORT_ASC, 'student.s_name_i' => SORT_ASC, 'student.s_name_o' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'sm_lms_id', $field);
    }
}
