<?php

namespace common\modules\mdl\models;

use Yii;
use common\modules\mdl\models\ActiveRecord;
use common\models\Worker;

/**
 * This is the model class for table "mdl_worker_map".
 *
 * @property integer $wm_id
 * @property integer $wm_mdl_id
 * @property integer $wm_lms_id
 *
 * @property Worker $worker
 */
class WorkerMap extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return parent::getPrefix() . '_worker_map';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wm_mdl_id', 'wm_lms_id'], 'required'],
            [['wm_mdl_id', 'wm_lms_id'], 'integer'],
            [['wm_mdl_id'], 'unique'],
            [['wm_lms_id'], 'unique'],
            [['wm_lms_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['wm_lms_id' => 'w_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wm_id' => Yii::t('mdl', 'Wm ID'),
            'wm_mdl_id' => Yii::t('mdl', 'Wm Mdl ID'),
            'wm_lms_id' => Yii::t('mdl', 'Wm Lms ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['w_id' => 'wm_lms_id']);
    }
}
