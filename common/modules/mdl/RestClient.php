<?php

namespace common\modules\mdl;

use yii\httpclient\Client;
use yii\helpers\Json;
use yii\base\Exception;

class RestClient
{
    /**
     * @var RestClient HTTP client
     */
    protected $httpclient;
    protected $path;
    protected $token;

    public function __construct($host, $path, $token)
    {
        $this->path = $path;
        $this->token = $token;

        $this->httpclient = new Client([
            'baseUrl' => $host,
        ]);
    }

    /**
     * @param string $method Moodle internal function
     * @param array $data Function parameters
     * @return array
     * @throws Exception
     */
    protected function request($method, array $data = [])
    {
        $def = [
            'moodlewsrestformat' => 'json',
            'wstoken' => $this->token,
        ];

        $resp = $this->httpclient->createRequest()
            ->setMethod('post')
            ->setUrl(sprintf('%s?wsfunction=%s', $this->path, $method))
            ->setData(array_merge($def, $data))
            ->setOptions(['userAgent' => 'LMS REST client'])
            ->send();

        if (!$resp->isOk) {
            throw new Exception('HTTP error: ' . $resp->statusCode);
        }

        $answer = Json::decode($resp->content);

        if (isset($answer['exception'])) {
            throw new Exception('Moodle error: ' . var_export($answer, true));
        }

        return $answer;
    }

    public function getCategories()
    {
        return $this->request('core_course_get_categories');
    }

    public function createCategories(array $data)
    {
        return $this->request('core_course_create_categories', ['categories' => $data]);
    }

    public function createCategory(array $data)
    {
        return $this->createCategories([$data])[0];
    }

    public function getCourses()
    {
        return $this->request('core_course_get_courses');
    }

    public function createCourses(array $data)
    {
        return $this->request('core_course_create_courses', ['courses' => $data]);
    }

    public function createCourse(array $data)
    {
        return $this->createCourses([$data])[0];
    }

    public function createGroupings(array $data)
    {
        return $this->request('core_group_create_groupings', ['groupings' => $data]);
    }

    /**
     * Create course grouping
     * @param integer $courseId
     * @param string $name
     * @param string $description
     * @param integer $descriptionFormat 1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN
     * @return integer ID of new grouping
     * @throws \yii\console\Exception
     */
    public function createGrouping($courseId, $name, $description = '', $descriptionFormat = 2)
    {
        $rst = $this->createGroupings([[
            'courseid' => $courseId,
            'name' => $name,
            'description' => $description ? $description : $name,
            'descriptionformat' => $descriptionFormat,
        ]]);

        if (!isset($rst[0]['id'])) {
            throw new Exception('Grouping ID was not returned.');
        }

        return $rst[0]['id'];
    }

    public function getCourseGroupings($courseId)
    {
        return $this->request('core_group_get_course_groupings', ['courseid' => $courseId]);
    }

    public function createGroups(array $data)
    {
        return $this->request('core_group_create_groups', ['groups' => $data]);
    }

    /**
     * Create course group
     * @param integer $courseId
     * @param string $name
     * @param string $description
     * @param integer $descriptionFormat 1 = HTML, 0 = MOODLE, 2 = PLAIN or 4 = MARKDOWN
     * @param string $enrollmentKey group enrol secret phrase
     * @return integer ID of new grouping
     * @throws \yii\console\Exception
     */
    public function createGroup($courseId, $name, $description = '', $descriptionFormat = 2, $enrollmentKey = '')
    {
        $rst = $this->createGroups([[
            'courseid' => $courseId,
            'name' => $name,
            'description' => $description ? $description : $name,
            'descriptionformat' => $descriptionFormat,
            'enrolmentkey' => $enrollmentKey,
        ]]);

        if (!isset($rst[0]['id'])) {
            throw new Exception('Group ID was not returned.');
        }

        return $rst[0]['id'];
    }

    public function getCourseGroups($courseId)
    {
        return $this->request('core_group_get_course_groups', ['courseid' => $courseId]);
    }

    public function groupingAddGroups(array $data)
    {
        return $this->request('core_group_assign_grouping', ['assignments' => $data]);
    }

    /**
     * Add group to grouping
     * @param type $groupingId
     * @param type $groupId
     * @return type
     */
    public function groupingAddGroup($groupingId, $groupId)
    {
        return $this->groupingAddGroups([[
            'groupingid' => $groupingId,
            'groupid' => $groupId,
        ]]);
    }

    public function addUsersToGroups(array $data)
    {
        $this->request('core_group_add_group_members', ['members' => $data]);
    }

    public function addUserToGroup($userId, $groupId)
    {
        $this->addUsersToGroups([[
            'userid' => $userId,
            'groupid' => $groupId,
        ]]);
    }

    public function deleteUsersFromGroups(array $data)
    {
        $this->request('core_group_delete_group_members', ['members' => $data]);
    }

    public function deleteUserFromGroup($userId, $groupId)
    {
        $this->deleteUsersFromGroups([[
            'userid' => $userId,
            'groupid' => $groupId,
        ]]);
    }

    public function getUsers()
    {
        return $this->request('core_user_get_users', [
                'criteria' => [[// required
                    'key' => 'email',
                    'value' => '%',
                    ]]
        ]);
    }

    public function getCourseUsers($courseId)
    {
        return $this->request('core_enrol_get_enrolled_users', ['courseid' => $courseId]);
    }

    public function getUserCourses($userId)
    {
        return $this->request('core_enrol_get_users_courses', ['userid' => $userId]);
    }

    public function enrolls($enrolments)
    {
        return $this->request('enrol_manual_enrol_users', ['enrolments' => $enrolments]);
    }

    public function enroll($userId, $courseId, $roleId, $suspend, $timeStart = null, $timeEnd = null)
    {
        return $this->enrolls([[
                'userid' => $userId,
                'courseid' => $courseId,
                'roleid' => $roleId,
                'suspend' => $suspend,
                'timestart' => $timeStart,
                'timeend' => $timeEnd,
        ]]);
    }
}
