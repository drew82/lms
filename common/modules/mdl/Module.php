<?php

namespace common\modules\mdl;

use Yii;
use yii\base\Exception;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\mdl\controllers';

    /**
     * @var string Moodle host
     */
    public $host;

    /**
     * @var string REST API path
     */
    public $path = 'webservice/rest/server.php';

    /**
     * @var string Auth token
     */
    public $token;

    /**
     * @var RestClient Moodle REST client
     */
    public $client;

    /**
     * @var integer Teacher role ID
     */
    public $roleTeacher;
    /**
     * @var integer Student role ID
     */
    public $roleStudent;

    /**
     * @var array Deafult user groups in any course
     */
    public $defaultUserGroups = [];

    /**
     * @var string VIS module ID [optional]
     */
    public $visModuleId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (YII_ENV_DEV) {
            Yii::warning('init  ' . $this->id);
        }

        if (!$this->host) {
            throw new Exception('Host is not set');
        }

        if (!$this->token) {
            throw new Exception('Token is not set');
        }

        if (!$this->roleTeacher) {
            throw new Exception('Teacher role ID is not set');
        }

        if (!$this->roleStudent) {
            throw new Exception('Student role ID is not set');
        }

        if ($this->visModuleId && !Yii::$app->hasModule($this->visModuleId)) {
            throw new Exception('VIS module doesn\'t exist');
        }

        $this->client = new RestClient($this->host, $this->path, $this->token);

        parent::init();

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;

        if (!isset($i18n->translations['mdl*'])) {
            $i18n->translations['mdl*'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }

    public function switchMoodleInstance()
    {
        models\ActiveRecord::setPrefix($this->id);
    }

    public function getName()
    {
        return parse_url($this->host, PHP_URL_HOST);
    }

    /**
     * Get MDL discipline id from VIS DB
     * @param integer $visId VIS discipline id
     * @return integer VIS discipline mdlID
     * @throws Exception
     */
    public function getVisDisciplineMdlId($visId)
    {
        if (!$this->visModuleId) {
            throw new Exception('VIS module is not set');
        }

        return Yii::$app->getModule($this->visModuleId)->getMdlCourseId($visId, $this->id);
    }

    /**
     * Get MDL student id from VIS DB
     * @param integer $visId VIS student id
     * @return integer VIS student mdlID
     * @throws Exception
     */
    public function getVisStudentMdlId($visId)
    {
        if (!$this->visModuleId) {
            throw new Exception('VIS module is not set');
        }

        return Yii::$app->getModule($this->visModuleId)->getMdlStudentId($visId, $this->id);
    }
}
