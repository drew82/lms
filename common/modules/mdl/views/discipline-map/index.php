<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\DisciplineMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Discipline Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discipline-map-index">

<?php $host = $this->context->module->host; ?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'dm_mdl_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'dm_lms_id',
                'value' => 'discipline.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'dm_lms_id',
                    'data' => \common\models\Discipline::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
            ],
            [
                'attribute' => 'dm_grade_id',
                'value' => 'grade.name',
                'filter' => common\modules\mdl\models\GradeMap::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'show_groupings',
                'filter' => Html::activeCheckbox($searchModel, 'show_groupings', ['label' => false]),
                'value' => function ($data) use ($searchModel) {
                    if (!$searchModel->show_groupings) return '';
                    $ret = [];
                    foreach ($data->getMdlGroupings() as $g) {
                        $ret[] = $g['name'];
                    }
                    return implode('<br />', $ret);
                },
                'label' => Yii::t('mdl', 'Groupings'),
                'format' => 'html',
            ],
            [
                'attribute' => 'show_groups',
                'filter' => Html::activeCheckbox($searchModel, 'show_groups', ['label' => false,]),
                'value' => function ($data) use ($searchModel) {
                    if (!$searchModel->show_groups) return '';
                    $ret = [];
                    foreach ($data->getMdlGroups() as $g) {
                        $ret[] = $g['name'];
                    }
                    return implode('<br />', $ret);
                },
                'label' => Yii::t('mdl', 'Groups'),
                'format' => 'html',
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'template' => '{view} {update} {delete} {show}',
                'buttons' => [
                    'show' => function ($url, $model, $key) use ($host) {
                        return !$model->dm_mdl_id ? null :
                            Html::a('<span class="glyphicon glyphicon-sunglasses"></span>',
                                sprintf('%s/course/view.php?id=%s', $host, $model->dm_mdl_id));
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
