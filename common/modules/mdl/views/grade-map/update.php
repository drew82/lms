<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\GradeMap */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Grade Map',
]) . $model->gm_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mdl', 'Grade Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gm_id, 'url' => ['view', 'id' => $model->gm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="grade-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
