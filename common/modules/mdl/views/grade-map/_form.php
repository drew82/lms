<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\GradeMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grade-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gm_mdl_id')->textInput() ?>

    <?= $form->field($model, 'gm_lms_id')->dropDownList(\common\models\Grade::listAll())->label(Yii::t('app', 'G Name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
