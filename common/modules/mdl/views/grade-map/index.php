<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\GradeMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Grade Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-map-index">

<?php $host = $this->context->module->host; ?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'gm_mdl_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'gm_lms_id',
                'value' => 'grade.name',
                'filter' => \common\models\Grade::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'template' => '{view} {update} {delete} {show}',
                'buttons' => [
                    'show' => function ($url, $model, $key) use ($host) {
                        return !$model->gm_mdl_id ? null :
                            Html::a('<span class="glyphicon glyphicon-sunglasses"></span>',
                                sprintf('%s/course/view.php?categoryid=%s', $host, $model->gm_mdl_id));
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
