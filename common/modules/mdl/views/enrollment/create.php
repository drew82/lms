<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\Enrollment */

$this->title = Yii::t('mdl', 'Create Enrollment');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mdl', 'Enrollments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enrollment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
