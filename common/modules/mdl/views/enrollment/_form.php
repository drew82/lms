<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\Enrollment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="enrollment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'student_id')->label(Yii::t('app', 'S Name Full'))->widget(Select2::className(), [
        'data' => common\modules\mdl\models\StudentMap::listAll(),
    ]) ?>

    <?= $form->field($model, 'discipline_id')->label(Yii::t('app', 'D Name'))->widget(Select2::className(), [
        'data' => common\modules\mdl\models\DisciplineMap::listAll(),
    ]) ?>

    <?= $form->field($model, 'grade_id')->dropDownList(common\modules\mdl\models\GradeMap::listAll())->label(Yii::t('app', 'G Name')) ?>

    <?= $form->field($model, 'e_is_enroll')->radioList(['1' => '+', '0' => '-']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
