<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\EnrollmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Enrollments');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enrollment-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'student_id',
                'value' => 'studentMap.student.nameFull',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'student_id',
                    'data' => \common\modules\mdl\models\StudentMap::listAll(),
                ]),
                'label' => Yii::t('app', 'S Name Full'),
            ],
            [
                'attribute' => 'discipline_id',
                'value' => 'disciplineMap.discipline.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'discipline_id',
                    'data' => common\modules\mdl\models\DisciplineMap::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
            ],
            [
                'attribute' => 'grade_id',
                'value' => 'disciplineMap.grade.name',
                'filter' => common\modules\mdl\models\GradeMap::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'e_is_enroll',
                'value' => function ($data) { return $data->e_is_enroll ? '+' : '&minus;'; },
                'filter' => ['1' => '+', '0' => '-'],
                'label' => '+/-',
                'format' => 'html',
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
