<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\Enrollment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Enrollment',
]) . $model->e_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mdl', 'Enrollments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->e_id, 'url' => ['view', 'id' => $model->e_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="enrollment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
