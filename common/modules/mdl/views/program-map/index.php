<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\ProgramMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Program Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-map-index">

<?php $host = $this->context->module->host; ?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'pm_mdl_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'pm_lms_id',
                'value' => 'program.p_name',
                'filter' => \common\models\Program::listAll(),
                'label' => Yii::t('app', 'Pg Name'),
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'template' => '{view} {update} {delete} {show}',
                'buttons' => [
                    'show' => function ($url, $model, $key) use ($host) {
                        return !$model->pm_mdl_id ? null :
                            Html::a('<span class="glyphicon glyphicon-sunglasses"></span>',
                                sprintf('%s/course/index.php?categoryid=%s', $host, $model->pm_mdl_id));
                    },
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
