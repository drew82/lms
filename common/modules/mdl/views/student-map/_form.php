<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\StudentMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sm_mdl_id')->textInput() ?>

    <?= $form->field($model, 'sm_lms_id')->dropDownList(\common\models\Student::listAll('nameFull'))->label(Yii::t('app', 'S Name Full')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
