<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\StudentMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Student Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-map-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'sm_mdl_id',
            ],
            [
                'attribute' => 'sm_lms_id',
                'value' => 'student.nameShort',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'sm_lms_id',
                    'data' => \common\modules\mdl\models\StudentMap::listAll('student.nameShort'),
                ]),
                'label' => Yii::t('app', 'S Name Short'),
            ],
            [
                'attribute' => 'show_courses',
                'visible' => $searchModel->grade_id && $searchModel->daterange_id,
                'filter' => Html::activeCheckbox($searchModel, 'show_courses', ['label' => false,]),
                'value' => function ($studentMap) use ($searchModel, $allDisciplineIds, $gradeDisciplineIds) {
                    // unchecked
                    if (!$searchModel->show_courses) return '';
                    // checked
                    return GridView::widget([
                        'dataProvider' => new yii\data\ArrayDataProvider([
                            'allModels' => $studentMap->getMdlCourses(),
                            'pagination' => false,
                            'sort' => [
                                'attributes' => [
                                    'def' => [
                                        'asc' => [
                                            'fullname' => SORT_ASC,
                                        ],
                                    ],
                                ],
                                'defaultOrder' => ['def' => SORT_ASC],
                            ],
                        ]),
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'fullname',
                            [
                                'value' => function ($mdlCourse) use ($allDisciplineIds, $gradeDisciplineIds) {
                                    if (!in_array($mdlCourse['id'], $allDisciplineIds)) {
                                        return '?';
                                    } elseif (in_array($mdlCourse['id'], $gradeDisciplineIds)) {
                                        return '';
                                    } else {
                                        return '!';
                                    }
                                },
                            ],
                        ],
                        'layout' => '{items}',
                        'showHeader' => false,
                    ]);    
                },
                'label' => Yii::t('mdl', 'Courses'),
                'format' => 'html',
            ],
            [
                'value' => function ($data) {
                    /* @var $data \common\modules\mdl\models\StudentMap */
                    return GridView::widget([
                        'dataProvider' => new yii\data\ArrayDataProvider([
                            'allModels' => $data->student->raises,
                            'pagination' => false,
                            'sort' => [
                                'attributes' => [
                                    'def' => [
                                        'asc' => [
                                            'daterange.d_till' => SORT_DESC,
                                            'grade.g_name' => SORT_DESC,
                                        ],
                                    ],
                                ],
                                'defaultOrder' => ['def' => SORT_ASC],
                            ],
                        ]),
                        'columns' => [
                            'grade.name:html',
                            'daterange.d_name',
                            [
                                'class' => 'backend\widgets\ActionColumn',
                                'visible' => null !== $data->sm_mdl_id,
                                'template' => '{minus} {plus}',
                                'buttons' => [
                                    'plus' => function ($url, $model, $key) {
                                        /* @var $model common\models\Raise */
                                        return Html::a('<span class="glyphicon glyphicon-plus-sign"></span>',
                                                    ['enrollment/grade-plus', 's_id' => $model->r_student_id, 'g_id' => $model->r_grade_id]);
                                    },
                                    'minus' => function ($url, $model, $key) {
                                        /* @var $model common\models\Raise */
                                        return Html::a('<span class="glyphicon glyphicon-minus-sign"></span>',
                                                    ['enrollment/grade-minus', 's_id' => $model->r_student_id, 'g_id' => $model->r_grade_id]);
                                    },
                                ],
                            ],
                        ],
                        'layout' => '{items}',
                        'showHeader' => false,
                    ]);
                },
                'filter' => Html::activeDropDownList($searchModel, 'grade_id', common\modules\mdl\models\GradeMap ::listAll(),
                                ['prompt' => Yii::t('app', 'G Name'), 'class' => 'form-control'])
                          . Html::activeDropDownList($searchModel, 'daterange_id', common\models\Daterange::listAll(),
                                ['prompt' => Yii::t('app', 'Dr Name'), 'class' => 'form-control']),
                'format' => 'html',
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>

<?php
$script = <<< JS
$('table table a').on('click', function(e) {
    e.preventDefault();
    $.ajax(this.href, {
        type: 'POST',
        context: this,
        success: function(data) {
            $(this).hide();
            $(this).after(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Request failed: " + errorThrown);
        }
    });
});
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
?>

<?php Pjax::end(); ?></div>
