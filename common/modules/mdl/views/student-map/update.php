<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\StudentMap */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Student Map',
]) . $model->sm_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mdl', 'Student Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sm_id, 'url' => ['view', 'id' => $model->sm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="student-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
