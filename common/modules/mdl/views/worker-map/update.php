<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\WorkerMap */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Worker Map',
]) . $model->wm_id;
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mdl', 'Worker Maps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wm_id, 'url' => ['view', 'id' => $model->wm_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="worker-map-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
