<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\mdl\models\WorkerMapSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mdl', 'Worker Maps');
$this->params['breadcrumbs'][] = ['label' => $this->context->module->name, 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-map-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'wm_mdl_id',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
            [
                'attribute' => 'wm_lms_id',
                'value' => 'worker.nameFull',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'wm_lms_id',
                    'data' => \common\models\Worker::listAll('nameFull'),
                ]),
                'label' => Yii::t('app', 'W Name Full'),
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
