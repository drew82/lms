<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\mdl\models\WorkerMap */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-map-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wm_mdl_id')->textInput() ?>

    <?= $form->field($model, 'wm_lms_id')->dropDownList(\common\models\Worker::listAll('nameFull', 'w_name_f'))->label(Yii::t('app', 'W Name Full')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
