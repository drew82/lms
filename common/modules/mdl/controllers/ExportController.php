<?php

namespace common\modules\mdl\controllers;

use yii\console\Exception;
use yii\helpers\VarDumper;

use common\modules\mdl\models\ProgramMap;
use common\modules\mdl\models\GradeMap;
use common\modules\mdl\models\DisciplineMap;
use common\modules\mdl\models\Enrollment;

/**
 * Export data into Moodle
 * 
 * @property \common\modules\mdl\Module $module
 */
class ExportController extends \yii\console\Controller
{

    public function actionPrograms()
    {
        /* @var $pm ProgramMap */
        foreach (ProgramMap::findAll(['pm_mdl_id' => null]) as $pm) {
            // set name
            $name = $pm->program->p_name;
            // create
            $rst = $this->module->client->createCategory(['name' => $name]);
            // map
            $pm->pm_mdl_id = $rst['id'];
            $this->_checkSave($pm);
            // log
            printf("[%4d] %s\n", $rst['id'], $name);
        }
    }

    public function actionGrades()
    {
        /* @var $gms GradeMap[] */
        $gms = GradeMap::find()
            ->joinWith('grade') // for sorting
            ->where(['gm_mdl_id' => null])
            ->all();

        foreach ($gms as $gm) {
            // set name
            $name = $gm->grade->g_name . ' семестр';
            // find mdl parent category
            $parentPm = ProgramMap::findOne(['pm_lms_id' => $gm->grade->program->p_id]);
            // create
            $rst = $this->module->client->createCategory(['name' => $name, 'parent' => $parentPm->pm_mdl_id]);
            // map
            $gm->gm_mdl_id = $rst['id'];
            $this->_checkSave($gm);
            // log
            printf("[%4d] [%4d] %s (%s)\n", $parentPm->pm_mdl_id, $rst['id'], $name, $gm->grade->program->p_name);
        }
    }

    public function actionDisciplines()
    {
        /* @var $dm DisciplineMap */
        $dms = DisciplineMap::find()
            ->joinWith('grade') // for sorting
            ->joinWith('discipline') // for sorting
            ->orderBy(['g_program_id' => SORT_ASC, 'g_name' => SORT_ASC, 'd_name' => SORT_ASC])
            ->where(['dm_mdl_id' => null])
            ->all();
        
        foreach ($dms as $dm) {
            // set name
            $name = $dm->discipline->d_name;
            // find mdl parent category
            $parentGm = GradeMap::findOne(['gm_lms_id' => $dm->grade->g_id]);
            // create
            $rst = $this->module->client->createCourse([
                'fullname' => $name,
                'shortname' => sprintf('%s [%s] %d', $name, $dm->grade->program->p_name, mt_rand(111,999)),
//                'idnumber' => $plan->discipline->d_code . $postfix . '_' . uniqid(), // unique
                'categoryid' => $parentGm->gm_mdl_id,
                'format' => 'topics',
                'courseformatoptions' => [
                    [
                        'name' => 'numsections',
                        'value' => 5,
                    ],
                    [
                        'name' => 'hiddensections',
                        'value' => 0,
                    ],
                    [
                        'name' => 'coursedisplay',
                        'value' => 1,
                    ],
                ],
            ]);
            // map
            $dm->dm_mdl_id = $rst['id'];
            $this->_checkSave($dm);
            // log
            printf("[%4d] [%4d] %s (%s)\n", $parentGm->gm_mdl_id, $rst['id'], $name, $dm->grade->name);
        }
    }

    /**
     * Create grouping and group; add group to grouping
     * @param string $name
     */
    public function actionGAndG($name)
    {
        $query = DisciplineMap::find();
        /* @var $dm DisciplineMap */
        foreach ($query->all() as $dm) {
            // log
            printf("[%4d] %s\n", $dm->dm_mdl_id, $dm->discipline->name);
            // create
            try {
                $dm->createMdlGroupingAndGroup($name);
            } catch (\yii\base\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
    }

    /**
     * Do enrollment and suspending
     */
    public function actionEnrollment($maxExecTime = 30)
    {
        $query = Enrollment::find()
            ->joinWith(['studentMap', 'disciplineMap'])
            ->where(['is not', 'sm_mdl_id', null])
            ->andWhere(['is not', 'dm_mdl_id', null])
            ->orderBy(new \yii\db\Expression('RAND()'))
            ->limit(500);
        /* @var $e Enrollment */
        foreach ($query->all() as $e) {
            // check time
            if (\Yii::getLogger()->getElapsedTime() >= $maxExecTime) {
                break;
            }
            // log
            printf("[%4d] %s [%4d] %s (%s)\n", $e->studentMap->sm_mdl_id, $e->e_is_enroll ? '+' : '-', $e->disciplineMap->dm_mdl_id, $e->studentMap->student->nameFull, $e->disciplineMap->discipline->name);
            // enroll
            $e->process();
        }
    }

    protected function _checkSave(\yii\db\ActiveRecord $model)
    {
        if (!$model->save()) {
            throw new Exception(VarDumper::dumpAsString($model->attributes) . VarDumper::dumpAsString($model->getErrors()));
        }
    }
}
