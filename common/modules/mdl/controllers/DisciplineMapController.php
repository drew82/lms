<?php

namespace common\modules\mdl\controllers;

use Yii;
use common\modules\mdl\models\DisciplineMap;
use common\modules\mdl\models\DisciplineMapSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DisciplineMapController implements the CRUD actions for DisciplineMap model.
 */
class DisciplineMapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisciplineMap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisciplineMapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisciplineMap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisciplineMap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DisciplineMap();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dm_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DisciplineMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dm_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DisciplineMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMdl($vis_id)
    {
        /* @var $vis_dm \common\modules\vis\models\DisciplineMap */
        /* @var $mdl_dm DisciplineMap */
        $ret = [];

        // if VIS mapping exist
        if (!$vis_dms = \common\modules\vis\models\DisciplineMap::findAll(['dm_vis_id' => $vis_id])) {
            throw new NotFoundHttpException('The requested vis ID does not exist.');
        } else {
            foreach ($vis_dms as $vis_dm) {
                if ($mdl_dm = DisciplineMap::findOne(['dm_lms_id' => $vis_dm->dm_lms_id, 'dm_grade_id' => $vis_dm->dm_grade_id])) {
                    $ret[] = [
                        'd' => $mdl_dm->discipline->name,
                        'p' => $mdl_dm->grade->program->p_name,
                        'g' => $mdl_dm->grade->g_name,
                        'mdl' => $mdl_dm->dm_mdl_id,
                    ];
                }
            }

            return \yii\helpers\Json::encode($ret);
        }
    }

    /**
     * Finds the DisciplineMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisciplineMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisciplineMap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
