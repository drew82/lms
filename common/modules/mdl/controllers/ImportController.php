<?php

namespace common\modules\mdl\controllers;

use yii\console\Exception;
use yii\helpers\VarDumper;

use common\models\Worker;
use common\modules\mdl\models\DisciplineMap;
use common\modules\mdl\models\WorkerMap;
use common\models\WorkerDiscipline;

/**
 * Import data from Moodle
 * 
 * @property \common\modules\mdl\Module $module
 */
class ImportController extends \yii\console\Controller
{

    public function actionWorkers()
    {
        foreach ($this->module->client->getCourses() as $c) {
            // discipline mapping exist
            if ($dm = DisciplineMap::findOne(['dm_mdl_id' => $c['id']])) {
                // get all enrolments
                foreach ($this->module->client->getCourseUsers($c['id']) as $u) {
                    foreach ($u['roles'] as $r) {
                        // filter teachers
                        if ($this->module->roleTeacher == $r['roleid']) {
                            // find or create worker
                            $worker = $this->_getWorker($u['lastname'], $u['firstname']);
                            // map worker
                            $this->_mapWorker($u['id'], $worker->w_id);
                            // set discipline<->worker
                            $this->_workerDiscipline($worker->w_id, $dm->dm_lms_id);
                            // log
                            printf("%s [%s] <-> %s [%s]\n", $c['fullname'], $c['id'], $u['fullname'], $u['id']);
                        }
                    }
                }
            }
        }
    }
    
    protected function _getWorker($f, $io)
    {
        @list($i, $o) = preg_split('%\s+%', $io);

        $w = Worker::findOne([
            'w_name_f' => $f,
            'w_name_i' => $i,
//            'w_name_o' => $o ? $o : '***',
        ]);

        if (!$w) {
            $w = new Worker();
            $w->w_name_f = $f;
            $w->w_name_i = $i;
            $w->w_name_o = $o;
            $this->_checkSave($w);
        }

        return $w;
    }

    protected function _mapWorker($mdlId, $lmsId)
    {
        if (!WorkerMap::find()->where(['wm_mdl_id' => $mdlId, 'wm_lms_id' => $lmsId])->exists()) {
            $wm = new WorkerMap();
            $wm->wm_mdl_id = $mdlId;
            $wm->wm_lms_id = $lmsId;
            $this->_checkSave($wm);
        }
    }

    protected function _workerDiscipline($workerId, $disciplineId)
    {
        if (!WorkerDiscipline::find()->where(['wd_worker_id' => $workerId, 'wd_discipline_id' => $disciplineId])->exists()) {
            $wd = new WorkerDiscipline();
            $wd->wd_worker_id = $workerId;
            $wd->wd_discipline_id = $disciplineId;
            $this->_checkSave($wd);
        }
    }

    protected function _checkSave(\yii\db\ActiveRecord $model)
    {
        if (!$model->save()) {
            throw new Exception(VarDumper::dumpAsString($model->attributes) . VarDumper::dumpAsString($model->getErrors()));
        }
    }
}
