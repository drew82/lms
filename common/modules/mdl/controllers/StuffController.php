<?php

namespace common\modules\mdl\controllers;

use common\modules\mdl\models\GradeMap;
use common\modules\mdl\models\StudentMap;

/**
 * Stuff
 */
class StuffController extends \yii\console\Controller
{
    /**
     * Add new students to studentMap when they raise to one of the mapped grades
     */
    public function actionStudentMap()
    {
        /* @var $gm GradeMap */
        foreach (GradeMap::find()->all() as $gm) {
            $gm->updateStudentsMap();
        }
    }

    /**
     * Set mdl_id to mapped students where it's null (get it from VIS on $sm->save())
     */
    public function actionStudentMdlId()
    {
        /* @var $sm StudentMap */
        foreach (StudentMap::find()->where(['sm_mdl_id' => null])->all() as $sm) {
            $sm->save();
        }
    }
}
