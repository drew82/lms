<?php

namespace common\modules\mdl\controllers;

use Yii;
use common\modules\mdl\models\StudentMap;
use common\modules\mdl\models\StudentMapSearch;
use common\modules\mdl\models\Grade;
use common\modules\mdl\models\Discipline;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StudentMapController implements the CRUD actions for StudentMap model.
 */
class StudentMapController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentMap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentMapSearch();
        // show mapped by default (using filter)
        $dataProvider = $searchModel->search(ArrayHelper::merge(['StudentMapSearch' => ['sm_mdl_id' => '>0']], Yii::$app->request->queryParams));

        // prepare data for show_courses
        if ($searchModel->show_courses && $searchModel->grade_id && $searchModel->daterange_id) {
            $gradeDisciplines = Grade::findOne($searchModel->grade_id)->getDisciplines()->with('map')->all();
            $allDisciplines = Discipline::find()->with('map')->all();
            $gradeDisciplineIds = ArrayHelper::getColumn($gradeDisciplines, 'map.dm_mdl_id');
            $allDisciplineIds = ArrayHelper::getColumn($allDisciplines, 'map.dm_mdl_id');
        } else {
            $gradeDisciplineIds = [];
            $allDisciplineIds = [];
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'gradeDisciplineIds' => $gradeDisciplineIds,
            'allDisciplineIds' => $allDisciplineIds,
        ]);
    }

    /**
     * Displays a single StudentMap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentMap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StudentMap();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sm_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StudentMap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sm_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StudentMap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentMap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentMap the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentMap::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
