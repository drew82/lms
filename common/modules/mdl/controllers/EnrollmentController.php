<?php

namespace common\modules\mdl\controllers;

use Yii;
use common\modules\mdl\models\Enrollment;
use common\modules\mdl\models\EnrollmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;

use common\modules\mdl\models\StudentMap;
use common\modules\mdl\models\GradeMap;
use common\modules\mdl\models\DisciplineMap;

/**
 * EnrollmentController implements the CRUD actions for Enrollment model.
 */
class EnrollmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'grade-plus'   => ['POST'],
                    'grade-minus'  => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Enrollment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EnrollmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Enrollment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Enrollment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Enrollment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->e_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Enrollment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->e_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Enrollment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPlus($s_id, $d_id)
    {
        $this->enrol($s_id, $d_id, true);
    }

    public function actionMinus($s_id, $d_id)
    {
        $this->enrol($s_id, $d_id, false);
    }

    /**
     * Enroll student to one discipline
     * @param integer $sm_mdl_id
     * @param integer $dm_mdl_id
     * @param boolean $is_enroll
     * @return boolean is enrollment created
     * @throws NotFoundHttpException
     */
    protected function enrol($sm_mdl_id, $dm_mdl_id, $is_enroll)
    {
        if (!$sm = StudentMap::findOne(['sm_mdl_id' => $sm_mdl_id])) {
            throw new NotFoundHttpException('The requested StudentMap does not exist.');
        }

        if (!$dm = DisciplineMap::findOne(['dm_mdl_id' => $dm_mdl_id])) {
            throw new NotFoundHttpException('The requested DisciplineMap does not exist.');
        }

        $e = new Enrollment();
        $e->e_sm_id = $sm->sm_id;
        $e->e_dm_id = $dm->dm_id;
        $e->e_is_enroll = $is_enroll;
        if (!$e->save()) {
            throw new BadRequestHttpException('Cannot create enrollment.');
        }
    }

    public function actionGradePlus($s_id, $g_id)
    {
        return $this->enrollGrade($s_id, $g_id, true);
    }

    public function actionGradeMinus($s_id, $g_id)
    {
        return $this->enrollGrade($s_id, $g_id, false);
    }

    /**
     * Enroll student to grade (all it's discicplines)
     * @param integer $student_id LMS student id
     * @param integer $grade_id LMS grade id
     * @param boolean $is_enroll true to enroll, false to unenroll
     * @return integer Number of enrolled disciplines
     * @throws NotFoundHttpException
     */
    protected function enrollGrade($student_id, $grade_id, $is_enroll)
    {
        if (!$sm = StudentMap::findOne(['sm_lms_id' => $student_id])) {
            throw new NotFoundHttpException('The requested StudentMap does not exist.');
        }

        if (!$gm = GradeMap::findOne(['gm_lms_id' => $grade_id])) {
            throw new NotFoundHttpException('The requested GradeMap does not exist.');
        }

        $i = 0;

        foreach ($gm->grade->disciplines as $d) {
            // if discipline mapped
            if ($dm = DisciplineMap::findOne(['dm_grade_id' => $gm->gm_lms_id, 'dm_lms_id' => $d->d_id])) {
                $e = new Enrollment();
                $e->e_sm_id = $sm->sm_id;
                $e->e_dm_id = $dm->dm_id;
                $e->e_is_enroll = $is_enroll;
                if ($e->save()) {
                    $i++;
                }
            }
        }

        return $i;
    }

    /**
     * Finds the Enrollment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Enrollment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Enrollment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
