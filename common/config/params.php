<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'baseCountry' => 'gb',
    'baseDomain' => 'university.edu',
    'name.short' => 'University Edu',
    'name.full' => 'University Edu Bla Bla Bla',
    'event.shift.before' => 0,
    'event.shift.after' => 0,
];
