<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'UTC',
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/site/auth', 'authclient' => 'google'],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => 'google_client_id',
                    'clientSecret' => 'google_client_secret',
                ],
            ],
        ],
        'sheets' => [
            'class' => 'machour\yii2\google\apiclient\components\GoogleApiClient',
            'clientSecretPath' => '@console/runtime/google-apiclient/auth.json',
            'credentialsPath' => '@console/runtime/google-apiclient/sheets.json',
            'api' => Google_Service_Sheets::class,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'dateFormat' => 'd MMM YYYY',
            'timeFormat' => 'HH:mm',
        ],
    ],
];
