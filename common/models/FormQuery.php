<?php

namespace common\models;

class FormQuery extends \yii\db\ActiveQuery
{

    public function study()
    {
        return $this->type(\Yii::$app->params['form-type.study']);
    }

    public function control()
    {
        return $this->type(\Yii::$app->params['form-type.control']);
    }

    public function type($type_id)
    {
        return $this->andWhere(['f_type_id' => $type_id]);
    }
}
