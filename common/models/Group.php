<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group_".
 *
 * @property integer $g_id
 * @property string $g_name
 *
 * @property EventGroup[] $eventGroups
 * @property Raise[] $raises
 * 
 * @property Grade[] $grades
 * @property Student[] $students
 * 
 * @property Raise[] $raisesCurrent
 * @property Student[] $studentsCurrent
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['g_name'], 'required'],
            [['g_name'], 'string', 'max' => 255],
            [['g_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'g_id' => Yii::t('app', 'Gp ID'),
            'g_name' => Yii::t('app', 'Gp Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventGroups()
    {
        return $this->hasMany(EventGroup::className(), ['eg_group_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaises()
    {
        return $this->hasMany(Raise::className(), ['r_group_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grade::className(), ['g_id' => 'r_grade_id'])->viaTable(Raise::tableName(), ['r_group_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['s_id' => 'r_student_id'])->viaTable(Raise::tableName(), ['r_group_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
        public function getRaisesCurrent()
    {
        return $this->getRaises()->onDate(date('Y-m-d'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentsCurrent()
    {
        return $this->hasMany(Student::className(), ['s_id' => 'r_student_id'])->via('raisesCurrent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    protected function mostRaised($daterangeId)
    {
        return $this->getRaises()->select(['r_grade_id', 'COUNT(*) AS cnt'])->andFilterWhere(['r_daterange_id' => $daterangeId])->orderBy(['cnt' => SORT_DESC]);
    }

    /**
     * @return Grade | false
     */
    public function getGradeProbable($daterangeId = null)
    {
        $query = $this->mostRaised($daterangeId)->joinWith('grade')->groupBy(Grade::tableName() . '.g_id');
        /* @var $topRaise Raise */
        $topRaise = $query->one();

        return $topRaise ? $topRaise->grade : false;
    }

    /**
     * @return Program | false
     */
    public function getProgramProbable($daterangeId = null)
    {
        $query = $this->mostRaised($daterangeId)->joinWith('grade.program')->groupBy(Program::tableName() . '.p_id');
        /* @var $topRaise Raise */
        $topRaise = $query->one();

        return $topRaise ? $topRaise->grade->program : false;
    }

    public static function listAll($field = 'g_name')
    {
        $query = self::find()->orderBy(['g_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'g_id', $field);
    }
}
