<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discipline".
 *
 * @property integer $d_id
 * @property string $d_code
 * @property string $d_name
 * @property integer $d_natsort
 * 
 * @property string $name
 *
 * @property Event[] $events
 * @property Plan[] $plans
 *
 * @property Form[] $forms
 * @property Grade[] $grades
 * @property Worker[] $workers
 */
class Discipline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_name'], 'required'],
            [['d_code', 'd_name'], 'string', 'max' => 255],
            [['d_natsort'], 'integer'],
            [['d_code', 'd_name'], 'unique', 'targetAttribute' => ['d_code', 'd_name'], 'message' => 'The combination of D Code and D Name has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'd_id' => Yii::t('app', 'D ID'),
            'd_code' => Yii::t('app', 'D Code'),
            'd_name' => Yii::t('app', 'D Name'),
            'd_natsort' => Yii::t('app', 'D Natsort'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['e_discipline_id' => 'd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plan::className(), ['p_discipline_id' => 'd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasMany(Form::className(), ['f_id' => 'p_form_id'])->viaTable(Plan::tableName(), ['p_discipline_id' => 'd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grade::className(), ['g_id' => 'p_grade_id'])->viaTable(Plan::tableName(), ['p_discipline_id' => 'd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['w_id' => 'wd_worker_id'])->viaTable(WorkerDiscipline::tableName(), ['wd_discipline_id' => 'd_id']);
    }

    public function getName()
    {
        return $this->d_code ? sprintf('%s [%s]', $this->d_name, $this->d_code) : $this->d_name;
    }

    public static function listAll($field = 'name')
    {
        $query = self::find()->orderBy(['d_name' => SORT_ASC, 'd_natsort' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'd_id', $field);
    }

    public static function natsort()
    {
        $i = 1;
        $disciplines = self::find()->where(['<>', 'd_code', ''])->all();

        $codes = \yii\helpers\ArrayHelper::getColumn($disciplines, 'd_code');
        natsort($codes);

        foreach ($codes as $code) {
            self::updateAll(['d_natsort' => $i++], ['d_code' => $code]);
        }
        // make last where code=''
        self::updateAll(['d_natsort' => $i++], ['d_code' => '']);
    }
}
