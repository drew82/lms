<?php

namespace common\models;

use yii\base\Exception;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const PREFIX_WORKER = 'w';
    const PREFIX_STUDENT = 's';

    public static function findIdentity($key)
    {
        list($prefix, $id) = preg_split('%^(\w)%', $key, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        if (static::PREFIX_WORKER == $prefix) {
            return Worker::findOne($id);
        } elseif (static::PREFIX_STUDENT == $prefix) {
            return Student::findOne($id);
        }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new Exception('Not implemented');
    }

    public function getId()
    {
        if ($this instanceof Worker) {
            return static::PREFIX_WORKER . $this->getPrimaryKey();
        } elseif ($this instanceof Student) {
            return static::PREFIX_STUDENT . $this->getPrimaryKey();
        }
    }

    public function getAuthKey()
    {
        return md5($this->getId());
    }

    public function validateAuthKey($authKey)
    {
        throw new Exception('Not implemented');
    }
}
