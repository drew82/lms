<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "grade".
 *
 * @property integer $g_id
 * @property integer $g_program_id
 * @property string $g_name
 * 
 * @property string $name
 * @property string $nameAbbr
 *
 * @property Program $program
 * @property Plan[] $plans
 * @property Price[] $prices
 * @property Raise[] $raises
 *
 * @property Group[] $groups
 * @property Student[] $students
 * @property Discipline[] $disciplines
 * @property Form[] $forms
 * 
 * @property Raise[] $raisesCurrent
 * @property Student[] $studentsCurrent
 * 
 * @property Grade $prev
 * @property Grade $next
 */
class Grade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['g_program_id', 'g_name'], 'required'],
            [['g_program_id'], 'integer'],
            [['g_name'], 'string', 'max' => 255],
            [['g_program_id', 'g_name'], 'unique', 'targetAttribute' => ['g_program_id', 'g_name'], 'message' => 'The combination of G Program ID and G Name has already been taken.'],
            [['g_program_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['g_program_id' => 'p_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'g_id' => Yii::t('app', 'G ID'),
            'g_program_id' => Yii::t('app', 'G Program ID'),
            'g_name' => Yii::t('app', 'G Name'),
        ];
    }

    public static function find()
    {
        return new GradeQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['p_id' => 'g_program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plan::className(), ['p_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(Price::className(), ['p_grade' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaises()
    {
        return $this->hasMany(Raise::className(), ['r_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['g_id' => 'r_group_id'])->viaTable(Raise::tableName(), ['r_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['s_id' => 'r_student_id'])->viaTable(Raise::tableName(), ['r_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['d_id' => 'p_discipline_id'])->viaTable(Plan::tableName(), ['p_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasMany(Form::className(), ['f_id' => 'p_form_id'])->viaTable(Plan::tableName(), ['p_grade_id' => 'g_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaisesCurrent()
    {
        return $this->getRaises()->onDate(date('Y-m-d'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentsCurrent()
    {
        return $this->hasMany(Student::className(), ['s_id' => 'r_student_id'])->via('raisesCurrent');
    }

    public function getNext()
    {
        return $this->hasOne(Grade::className(), ['g_program_id' => 'p_id'])->via('program')->where(['>', 'g_name', $this->g_name])->orderBy(['LENGTH(g_name)' => SORT_ASC, 'g_name' => SORT_ASC]);
    }

    public function getPrev()
    {
        return $this->hasOne(Grade::className(), ['g_program_id' => 'p_id'])->via('program')->where(['<', 'g_name', $this->g_name])->orderBy(['LENGTH(g_name)' => SORT_DESC, 'g_name' => SORT_DESC]);
    }

    public function getName()
    {
        $program = mb_strlen($this->program->p_name) < 50 ? $this->program->p_name : mb_substr($this->program->p_name, 0, 50) . '...';

        return sprintf('[%s] %s %s', $program, $this->g_name, Yii::t('app', 'gr.'));
    }

    public function getNameAbbr()
    {
        return sprintf('%s:%s', $this->program->p_abbr, $this->g_name);
    }

    public static function listAll($field = 'g_name')
    {
        $query = static::find()->joinWith('program')->orderBy(['program.p_name' => SORT_ASC, 'LENGTH([[g_name]])' => SORT_ASC, 'g_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'g_id', $field, 'program.p_name');
    }
}
