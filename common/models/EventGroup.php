<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event__group".
 *
 * @property integer $eg_id
 * @property integer $eg_event_id
 * @property integer $eg_group_id
 *
 * @property Event $event
 * @property Group $group
 */
class EventGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event__group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['eg_event_id', 'eg_group_id'], 'required'],
            [['eg_event_id', 'eg_group_id'], 'integer'],
            [['eg_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['eg_event_id' => 'e_id']],
            [['eg_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['eg_group_id' => 'g_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'eg_id' => Yii::t('app', 'Eg ID'),
            'eg_event_id' => Yii::t('app', 'Eg Event ID'),
            'eg_group_id' => Yii::t('app', 'Eg Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['e_id' => 'eg_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['g_id' => 'eg_group_id']);
    }
}
