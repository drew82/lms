<?php

namespace common\models;

class GradeQuery extends \yii\db\ActiveQuery
{

    public function byName()
    {
        return $this->orderBy(['LENGTH(g_name)' => SORT_ASC, 'g_name' => SORT_ASC]);
    }
}
