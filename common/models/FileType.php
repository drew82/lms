<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "file_type".
 *
 * @property integer $t_id
 * @property string $t_name
 * @property string $t_class
 * @property string $t_config
 *
 * @property File[] $files
 */
class FileType extends \yii\db\ActiveRecord
{
    protected $_plugin;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_name', 't_class'], 'required'],
            [['t_name', 't_class', 't_config'], 'string', 'max' => 255],
            [['t_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            't_id' => Yii::t('app', 'FlT ID'),
            't_name' => Yii::t('app', 'FlT Name'),
            't_class' => Yii::t('app', 'FlT Class'),
            't_config' => Yii::t('app', 'FlT Config'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['f_type_id' => 't_id']);
    }

    public function getPlugin()
    {
        if (!$this->_plugin) {
            $class = 'common\\models\\files\\' . $this->t_class;
            $config = parse_ini_string($this->t_config);
            $this->_plugin = new $class($config);
        }

        return $this->_plugin;
    }

    public static function listAll($field = 't_name')
    {
        $query = self::find()->orderBy(['t_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 't_id', $field);
    }

    public static function listClasses()
    {
        $classes = [];
        $files = \yii\helpers\FileHelper::findFiles(__DIR__ . '/files/', ['except' => ['BaseFile.php', 'FileInterface.php']]);

        foreach ($files as $path) {
            $classes[] = [
                'file' => basename($path, '.php'),
            ];
        }

        return \yii\helpers\ArrayHelper::map($classes, 'file', 'file');
    }
}
