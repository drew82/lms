<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "form".
 *
 * @property integer $f_id
 * @property integer $f_type_id
 * @property string $f_name
 *
 * @property Event[] $events
 * @property FormType $type
 * @property Plan[] $plans
 * 
 * @property Discipline[] $disciplines
 */
class Form extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_type_id', 'f_name'], 'required'],
            [['f_type_id'], 'integer'],
            [['f_name'], 'string', 'max' => 255],
            [['f_name'], 'unique'],
            [['f_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FormType::className(), 'targetAttribute' => ['f_type_id' => 't_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'f_id' => Yii::t('app', 'F ID'),
            'f_type_id' => Yii::t('app', 'F Type ID'),
            'f_name' => Yii::t('app', 'F Name'),
        ];
    }

    public static function find()
    {
        return new FormQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['e_form_id' => 'f_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FormType::className(), ['t_id' => 'f_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plan::className(), ['p_form_id' => 'f_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['d_id' => 'p_discipline_id'])->viaTable(Plan::tableName(), ['p_form_id' => 'f_id']);
    }

    public static function listAll($field = 'f_name')
    {
        $query = self::find()->joinWith('type')->orderBy([FormType::tableName() . '.t_name' => SORT_ASC, 'f_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'f_id', $field, 'type.t_name');
    }
}
