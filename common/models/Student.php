<?php

namespace common\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;

/**
 * This is the model class for table "student".
 *
 * @property integer $s_id
 * @property string $s_name_f
 * @property string $s_name_i
 * @property string $s_name_o
 * @property string $s_email
 * @property string $s_mobile
 * 
 * @property string $nameFull
 * @property string $nameShort
 *
 * @property Raise[] $raises
 * @property Visit[] $visits
 * 
 * @property Grade[] $grades
 * 
 * @property Raise[] $raisesCurrent
 * @property Grade[] $gradesCurrent
 * @property Raise $lastRaise
 */
class Student extends User
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['s_name_o'], 'default', 'value' => '***'],
            [['s_name_f', 's_name_i', 's_name_o'], 'required'],
            [['s_name_f', 's_name_i', 's_name_o'], 'string', 'max' => 255],
            // email
            [['s_email'], 'default', 'value' => function ($w) {
                return sprintf('?%s@%s', \yii\helpers\BaseInflector::slug($w->nameShort, ''), Yii::$app->params['baseDomain']);
            }],
            [['s_email'], 'required'],
            [['s_email'], 'unique'],
            [['s_email'], 'email', 'except' => 'create'],
            [['s_email'], 'email', 'checkDNS' => true, 'on' => 'create'],
            // mobile
            [['s_mobile'], PhoneInputValidator::className()],
        ];
    }

    public function behaviors()
    {
        return [
            'mobile' => [
                'class' => PhoneInputBehavior::className(),
                'phoneAttribute' => 's_mobile',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            's_id' => Yii::t('app', 'S ID'),
            's_name_f' => Yii::t('app', 'S Name F'),
            's_name_i' => Yii::t('app', 'S Name I'),
            's_name_o' => Yii::t('app', 'S Name O'),
            's_email' => Yii::t('app', 'S Email'),
            's_mobile' => Yii::t('app', 'S Mobile'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaises()
    {
        return $this->hasMany(Raise::className(), ['r_student_id' => 's_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visit::className(), ['v_student_id' => 's_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grade::className(), ['g_id' => 'r_grade_id'])->viaTable(Raise::tableName(), ['r_student_id' => 's_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaisesCurrent()
    {
        return $this->getRaises()->onDate(date('Y-m-d'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradesCurrent()
    {
        return $this->hasMany(Grade::className(), ['g_id' => 'r_grade_id'])->via('raisesCurrent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastRaise()
    {
        return $this->hasOne(Raise::class, ['r_student_id' => 's_id'])->joinWith('daterange')->orderBy(['daterange.d_till' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupInDaterange($daterangeId)
    {
        return $this->hasOne(Group::className(), ['g_id' => 'r_group_id'])->viaTable(Raise::tableName(), ['r_student_id' => 's_id'],
                function ($query) use ($daterangeId) {
                    /* @var $query \yii\db\ActiveQuery */
                    $query->andWhere(['r_daterange_id' => $daterangeId]);
                }
        );
    }

    public function getNameFull()
    {
        return sprintf('%s %s %s', $this->s_name_f, $this->s_name_i, $this->s_name_o);
    }

    public function getNameShort()
    {
        return sprintf('%s %s.%s.', $this->s_name_f, mb_substr($this->s_name_i, 0, 1, 'utf-8'), mb_substr($this->s_name_o, 0, 1, 'utf-8'));
    }

    public static function listAll($field = 's_name_f')
    {
        $query = self::find()->orderBy(['s_name_f' => SORT_ASC, 's_name_i' => SORT_ASC, 's_name_o' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 's_id', $field);
    }
}
