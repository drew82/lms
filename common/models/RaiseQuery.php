<?php

namespace common\models;

class RaiseQuery extends \yii\db\ActiveQuery
{

    public function includeRange($dt_start, $dt_end)
    {
        return $this->joinWith(Daterange::tableName())
            ->andWhere(['<=', 'daterange.d_from', $dt_start])
            ->andWhere(['>=', 'daterange.d_till', $dt_end]);
    }
    
    public function onDate($dt)
    {
        return $this->joinWith(Daterange::tableName())
            ->andWhere(['<=', 'daterange.d_from', $dt])
            ->andWhere(['>=', 'daterange.d_till', $dt]);
    }
}
