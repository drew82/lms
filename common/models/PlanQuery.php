<?php

namespace common\models;

class PlanQuery extends \yii\db\ActiveQuery
{

    public function form($id)
    {
        return $this->andWhere(['p_form_id' => $id]);
    }

    public function discipline($id)
    {
        return $this->andWhere(['p_discipline_id' => $id]);
    }

    public function study()
    {
        return $this->formType(\Yii::$app->params['form-type.study']);
    }

    public function control()
    {
        return $this->formType(\Yii::$app->params['form-type.control']);
    }

    protected function formType($type_id)
    {
        return $this->joinWith('form')->andWhere(['f_type_id' => $type_id]);
    }
}
