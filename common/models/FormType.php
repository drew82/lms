<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "form_type".
 *
 * @property integer $t_id
 * @property string $t_name
 *
 * @property Form[] $forms
 */
class FormType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_name'], 'required'],
            [['t_name'], 'string', 'max' => 255],
            [['t_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            't_id' => Yii::t('app', 'FmT ID'),
            't_name' => Yii::t('app', 'FmT Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasMany(Form::className(), ['f_type_id' => 't_id']);
    }

    public static function listAll($field = 't_name')
    {
        $query = self::find()->orderBy(['t_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 't_id', $field);
    }
}
