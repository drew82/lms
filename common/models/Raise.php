<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "raise".
 *
 * @property integer $r_id
 * @property integer $r_student_id
 * @property integer $r_grade_id
 * @property integer $r_group_id
 * @property integer $r_daterange_id
 *
 * @property Payment[] $payments
 * @property Student $student
 * @property Grade $grade
 * @property Group $group
 * @property Daterange $daterange
 */
class Raise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'raise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['r_student_id', 'r_grade_id', 'r_group_id', 'r_daterange_id'], 'required'],
            [['r_student_id', 'r_grade_id', 'r_group_id', 'r_daterange_id'], 'integer'],
            [['r_student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['r_student_id' => 's_id']],
            [['r_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['r_grade_id' => 'g_id']],
            [['r_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['r_group_id' => 'g_id']],
            [['r_daterange_id'], 'exist', 'skipOnError' => true, 'targetClass' => Daterange::className(), 'targetAttribute' => ['r_daterange_id' => 'd_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'r_id' => Yii::t('app', 'R ID'),
            'r_student_id' => Yii::t('app', 'R Student ID'),
            'r_grade_id' => Yii::t('app', 'R Grade ID'),
            'r_group_id' => Yii::t('app', 'R Group ID'),
            'r_daterange_id' => Yii::t('app', 'R Daterange ID'),
        ];
    }

    public static function find()
    {
        return new RaiseQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['p_raise_id' => 'r_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['s_id' => 'r_student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['g_id' => 'r_grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['g_id' => 'r_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDaterange()
    {
        return $this->hasOne(Daterange::className(), ['d_id' => 'r_daterange_id']);
    }
}
