<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property integer $p_id
 * @property integer $p_grade_id
 * @property integer $p_discipline_id
 * @property integer $p_form_id
 * @property integer $p_qty
 *
 * @property Grade $grade
 * @property Discipline $discipline
 * @property Form $form
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['p_grade_id', 'p_discipline_id', 'p_form_id'], 'required'],
            [['p_grade_id', 'p_discipline_id', 'p_form_id', 'p_qty'], 'integer'],
            [['p_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grade::className(), 'targetAttribute' => ['p_grade_id' => 'g_id']],
            [['p_discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['p_discipline_id' => 'd_id']],
            [['p_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['p_form_id' => 'f_id']],
            [['p_qty'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'p_id' => Yii::t('app', 'P ID'),
            'p_grade_id' => Yii::t('app', 'P Grade ID'),
            'p_discipline_id' => Yii::t('app', 'P Discipline ID'),
            'p_form_id' => Yii::t('app', 'P Form ID'),
            'p_qty' => Yii::t('app', 'P Qty'),
        ];
    }

    public static function find()
    {
        return new PlanQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(Grade::className(), ['g_id' => 'p_grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['d_id' => 'p_discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['f_id' => 'p_form_id']);
    }
}
