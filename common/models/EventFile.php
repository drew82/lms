<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event__file".
 *
 * @property integer $ef_id
 * @property integer $ef_event_id
 * @property integer $ef_file_id
 *
 * @property Event $event
 * @property File $file
 */
class EventFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event__file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ef_event_id', 'ef_file_id'], 'required'],
            [['ef_event_id', 'ef_file_id'], 'integer'],
            [['ef_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['ef_event_id' => 'e_id']],
            [['ef_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['ef_file_id' => 'f_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ef_id' => Yii::t('app', 'Ef ID'),
            'ef_event_id' => Yii::t('app', 'Ef Event ID'),
            'ef_file_id' => Yii::t('app', 'Ef File ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['e_id' => 'ef_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['f_id' => 'ef_file_id']);
    }
}
