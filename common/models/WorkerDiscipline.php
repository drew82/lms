<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "worker__discipline".
 *
 * @property integer $wd_id
 * @property integer $wd_worker_id
 * @property integer $wd_discipline_id
 *
 * @property Worker $worker
 * @property Discipline $discipline
 */
class WorkerDiscipline extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker__discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wd_worker_id', 'wd_discipline_id'], 'required'],
            [['wd_worker_id', 'wd_discipline_id'], 'integer'],
            [['wd_worker_id', 'wd_discipline_id'], 'unique', 'targetAttribute' => ['wd_worker_id', 'wd_discipline_id'], 'message' => 'The combination of Wd Worker ID and Wd Discipline ID has already been taken.'],
            [['wd_worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['wd_worker_id' => 'w_id']],
            [['wd_discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['wd_discipline_id' => 'd_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wd_id' => Yii::t('app', 'Wd ID'),
            'wd_worker_id' => Yii::t('app', 'Wd Worker ID'),
            'wd_discipline_id' => Yii::t('app', 'Wd Discipline ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['w_id' => 'wd_worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['d_id' => 'wd_discipline_id']);
    }
}
