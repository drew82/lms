<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daterange".
 *
 * @property integer $d_id
 * @property string $d_name
 * @property string $d_from
 * @property string $d_till
 *
 * @property Raise[] $raises
 * 
 * @property Group[] $groups
 */
class Daterange extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daterange';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_name', 'd_from', 'd_till'], 'required'],
            [['d_from', 'd_till'], 'safe'],
            [['d_name'], 'string', 'max' => 255],
            [['d_name'], 'unique'],
            ['d_from', 'compare', 'compareAttribute' => 'd_till', 'operator' => '<'],
            ['d_till', 'compare', 'compareAttribute' => 'd_from', 'operator' => '>'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'd_id' => Yii::t('app', 'Dr ID'),
            'd_name' => Yii::t('app', 'Dr Name'),
            'd_from' => Yii::t('app', 'Dr From'),
            'd_till' => Yii::t('app', 'Dr Till'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaises()
    {
        return $this->hasMany(Raise::className(), ['r_daterange_id' => 'd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['g_id' => 'r_group_id'])->viaTable(Raise::tableName(), ['r_daterange_id' => 'd_id']);
    }

    public function getDuration()
    {
        $dt_from = new \DateTime($this->d_from);
        $dt_till = new \DateTime($this->d_till);

        return $dt_till->getTimestamp() - $dt_from->getTimestamp();
    }

    /**
     * @return Daterange
     */
    public static function current()
    {
        return static::find()->where(['<=', 'd_from', date('Y-m-d')])->andWhere(['>=', 'd_till', date('Y-m-d')])->one();
    }

    public static function listAll($field = 'd_name')
    {
        $query = self::find()->orderBy(['d_from' => SORT_DESC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'd_id', $field);
    }
}
