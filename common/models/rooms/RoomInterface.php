<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

interface RoomInterface
{
    const ATTENDEE_ROLE_LISTENER = 'listener';
    const ATTENDEE_ROLE_PRESENTER = 'presenter';

    public function getUrl(Room $room, Event $event);

    public function getLoginUrl(Room $room, Event $event, $name, $email, $role);

    public function getLogo();
}
