<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

class SkypeRoom extends BaseRoom
{
    public function getUrl(Room $room, Event $event)
    {
        return sprintf('skype:%s?call', $room->r_number);
    }
}
