<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

class FlashRoom extends BaseRoom
{
    public $prefix;

    public function getUrl(Room $room, Event $event)
    {
        if (null == $event) {
            return sprintf('%s%s', $this->prefix, $room->r_number);
        } else {
            return sprintf('%s%s?%s', $this->prefix, $room->r_number, http_build_query([
                'd' => $event->discipline->d_name,
                'p' => $event->worker->nameFull
            ]));
        }
    }
}
