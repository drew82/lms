<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;
use ClickMeeting\Client\CurlClient;

/**
 * @property CurlClient $client
 */
class ClickmeetingRoom extends BaseRoom
{
    public $apiKey;
    protected $_client;

    public function getClient()
    {
        if (!$this->_client) {
            $this->_client = new CurlClient($this->apiKey);
        }

        return $this->_client;
    }

    public function getUrl(Room $room, Event $event)
    {
        $room = $this->client->getConference($room->r_number);

        return $room->conference->room_url;
    }

    public function getLoginUrl(Room $room, Event $event, $name, $email, $role)
    {
        return sprintf('%s?l=%s', $this->getUrl($room, $event), $this->getAutologinHash($room, $name, $email, $role));
    }

    protected function getAutologinHash(Room $room, $name, $email, $role)
    {
        $rst = $this->client->createAutologinUrl($room->r_number, [
            'email' => $email,
            'nickname' => $name,
            'role' => $role,
        ]);

        return $rst->autologin_hash;
    }

    protected function createRoom(Event $event)
    {
		$rst = $this->client->addConference([
			'name' => sprintf('[%s] %s (%s)', $event->dtStart->format('Y-m-d H:i'), $event->discipline->d_name, $event->worker->nameFull),
			'room_type' => 'meeting',
			'permanent_room' => false,
			'access_type' => 1,
			'custom_room_url_name' => md5($event->e_id . date('U')),
			'starts_at' => $event->dtStart->format(\DateTime::ISO8601),
			'duration' => $event->dtStart->diff($event->dtEnd)->format('%h:%I'),
			'settings' => [
				'show_on_personal_page' => false,
				'thank_you_emails_enabled' => false,
				'connection_tester_enabled' => false,
				'phonegateway_enabled' => false,
				'recorder_autostart_enabled' => true,
				'room_invite_button_enabled' => false,
				'social_media_sharing_enabled' => false,
				'connection_status_enabled' => true,
				'thank_you_page_url' => 'http://my.fdomgppu.ru/',
			],
		]);

        if (!$rst) {
            throw new \Exception('CM room was not created');
        }

        $id = \yii\helpers\ArrayHelper::getValue($rst, 'room.id');

        $room = new Room();
        $room->r_type_id = $event->room->type->t_id;
        $room->r_name = (string) $id;
        $room->save();

        $event->room->r_id = $room->r_id;
        $event->update(true, ['r_room_id']);

        return $id;
    }
}
