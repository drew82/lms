<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

abstract class BaseRoom extends \yii\base\Object implements RoomInterface
{
    public $logo;

    public function getUrl(Room $room, Event $event)
    {
        return '#';
    }

    public function getLoginUrl(Room $room, Event $event, $name, $email, $role)
    {
        return $this->getUrl($room, $event);
    }

    public function getLogo()
    {
        if (!$this->logo) {
            $className = (new \ReflectionClass($this))->getShortName();
            $words = \yii\helpers\BaseInflector::camel2words($className, false);
            $words_arr = explode(' ', $words);
            $this->logo = $words_arr[0];
        }

        return $this->logo;
    }
}
