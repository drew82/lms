<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

class ClassRoom extends BaseRoom
{
    public $address;

    public function getUrl(Room $room, Event $event)
    {
        return 'https://maps.yandex.ru/?' . http_build_query([
            'rtext' => '~' . $this->address,
            'rtt'=> 'mt',
        ]);
    }
}
