<?php

namespace common\models\rooms;

use common\models\Room;
use common\models\Event;

class LocationRoom extends BaseRoom
{
    public function getUrl(Room $room, Event $event)
    {
        return 'https://maps.yandex.ru/?' . http_build_query([
            'rtext' => '~' . $room->r_number,
            'rtt'=> 'mt',
        ]);
    }
}
