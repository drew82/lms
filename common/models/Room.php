<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property integer $r_id
 * @property integer $r_type_id
 * @property string $r_name
 * @property string $r_number
 *
 * @property Event[] $events
 * @property RoomType $type
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['r_type_id', 'r_name', 'r_number'], 'required'],
            [['r_type_id'], 'integer'],
            [['r_name', 'r_number'], 'string', 'max' => 255],
            [['r_type_id', 'r_number'], 'unique', 'targetAttribute' => ['r_type_id', 'r_number'], 'message' => 'The combination of R Type ID and R Number has already been taken.'],
            [['r_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => RoomType::className(), 'targetAttribute' => ['r_type_id' => 't_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'r_id' => Yii::t('app', 'R ID'),
            'r_type_id' => Yii::t('app', 'R Type ID'),
            'r_name' => Yii::t('app', 'R Name'),
            'r_number' => Yii::t('app', 'R Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['e_room_id' => 'r_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(RoomType::className(), ['t_id' => 'r_type_id']);
    }

    public static function listAll($field = 'r_name')
    {
        $query = static::find()->joinWith('type')->orderBy([RoomType::tableName() . '.t_name' => SORT_ASC, 'r_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'r_id', $field, 'type.t_name');
    }
}
