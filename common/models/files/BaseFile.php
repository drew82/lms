<?php

namespace common\models\files;

use common\models\EventFile;

abstract class BaseFile extends \yii\base\Object implements FileInterface
{
    public $logo;
    public $prefix;
    public $mime;

    public function getLogo()
    {
        if (!$this->logo) {
            $className = (new \ReflectionClass($this))->getShortName();
            $words = \yii\helpers\BaseInflector::camel2words($className, false);
            $words_arr = explode(' ', $words);
            $this->logo = $words_arr[0];
        }

        return $this->logo;
    }

    public function getUrl(EventFile $ef)
    {
        return $this->prefix . $ef->file->f_path;
    }
}
