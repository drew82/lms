<?php

namespace common\models\files;

use common\models\EventFile;

interface FileInterface
{
    public function getLogo();

    public function getUrl(EventFile $ef);
}
