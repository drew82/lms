<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mode".
 *
 * @property integer $m_id
 * @property string $m_name
 *
 * @property Event[] $events
 */
class Mode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_name'], 'required'],
            [['m_name'], 'string', 'max' => 255],
            [['m_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'm_id' => Yii::t('app', 'M ID'),
            'm_name' => Yii::t('app', 'M Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['e_mode_id' => 'm_id']);
    }

    public static function listAll($field = 'm_name')
    {
        $query = self::find()->orderBy(['m_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'm_id', $field);
    }
}
