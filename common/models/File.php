<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $f_id
 * @property integer $f_type_id
 * @property integer $f_worker_id
 * @property string $f_dt
 * @property string $f_path
 * @property string $f_descr
 *
 * @property EventFile[] $eventFiles
 * @property FileType $type
 * @property Worker $worker
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_dt'], 'default', 'value' => function ($model, $attributes) {
                return (new \DateTime(Yii::$app->timeZone))->format('Y-m-d H:i:s');
            }],
            [['f_type_id', 'f_worker_id', 'f_dt', 'f_path'], 'required'],
            [['f_type_id', 'f_worker_id'], 'integer'],
            [['f_path', 'f_descr'], 'string', 'max' => 255],
            [['f_type_id', 'f_path'], 'unique', 'targetAttribute' => ['f_type_id', 'f_path'], 'message' => 'The combination of F Type ID and F Path has already been taken.'],
            [['f_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FileType::className(), 'targetAttribute' => ['f_type_id' => 't_id']],
            [['f_worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['f_worker_id' => 'w_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'f_id' => Yii::t('app', 'F ID'),
            'f_type_id' => Yii::t('app', 'F Type ID'),
            'f_worker_id' => Yii::t('app', 'F Worker ID'),
            'f_dt' => Yii::t('app', 'F Dt'),
            'f_path' => Yii::t('app', 'F Path'),
            'f_descr' => Yii::t('app', 'F Descr'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventFiles()
    {
        return $this->hasMany(EventFile::className(), ['ef_file_id' => 'f_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FileType::className(), ['t_id' => 'f_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['w_id' => 'f_worker_id']);
    }
}
