<?php

namespace common\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;

/**
 * This is the model class for table "worker".
 *
 * @property integer $w_id
 * @property string $w_name_f
 * @property string $w_name_i
 * @property string $w_name_o
 * @property string $w_email
 * @property string $w_mobile
 *
 * @property string $nameFull
 * @property string $nameShort

 * @property Event[] $events
 * @property Discipline[] $disciplines
 */
class Worker extends User
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['w_name_o'], 'default', 'value' => '***'],
            [['w_name_f', 'w_name_i', 'w_name_o'], 'required'],
            [['w_name_f', 'w_name_i', 'w_name_o'], 'string', 'max' => 255],
            // email
            [['w_email'], 'default', 'value' => function ($w) {
                return sprintf('?%s@%s', \yii\helpers\BaseInflector::slug($w->nameShort, ''), Yii::$app->params['baseDomain']);
            }],
            [['w_email'], 'required'],
            [['w_email'], 'unique'],
            [['w_email'], 'email', 'except' => 'create'],
            [['w_email'], 'email', 'checkDNS' => true, 'on' => 'create'],
            // mobile
            [['w_mobile'], PhoneInputValidator::className()],
        ];
    }

    public function behaviors()
    {
        return [
            'mobile' => [
                'class' => PhoneInputBehavior::className(),
                'phoneAttribute' => 'w_mobile',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'w_id' => Yii::t('app', 'W ID'),
            'w_name_f' => Yii::t('app', 'W Name F'),
            'w_name_i' => Yii::t('app', 'W Name I'),
            'w_name_o' => Yii::t('app', 'W Name O'),
            'w_email' => Yii::t('app', 'W Email'),
            'w_mobile' => Yii::t('app', 'W Mobile'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['e_worker_id' => 'w_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['d_id' => 'wd_discipline_id'])->viaTable(WorkerDiscipline::tableName(), ['wd_worker_id' => 'w_id']);
    }

    public function getNameFull()
    {
        return sprintf('%s %s %s', $this->w_name_f, $this->w_name_i, $this->w_name_o);
    }

    public function getNameShort()
    {
        return sprintf('%s %s.%s.', $this->w_name_f, mb_substr($this->w_name_i, 0, 1, 'utf-8'), mb_substr($this->w_name_o, 0, 1, 'utf-8'));
    }

    public static function listAll($field = 'w_name_f')
    {
        $query = self::find()->orderBy(['w_name_f' => SORT_ASC, 'w_name_i' => SORT_ASC, 'w_name_o' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'w_id', $field);
    }
}
