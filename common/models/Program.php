<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "program".
 *
 * @property integer $p_id
 * @property string $p_name
 * @property string $p_code
 * @property string $p_abbr
 *
 * @property Grade[] $grades

 * @property Plan[] $plans
 * @property Discipline[] $disciplines
 * @property Form[] $forms
 */
class Program extends \yii\db\ActiveRecord
{
    const ZET = 36;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['p_name', 'p_code', 'p_abbr'], 'required'],
           [['p_name', 'p_code'], 'string', 'max' => 255],
           [['p_abbr'], 'string', 'max' => 4],
           [['p_name'], 'unique'],
           [['p_abbr'], 'unique'], 
           [['p_code'], 'unique'], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'p_id' => Yii::t('app', 'Pg ID'),
            'p_name' => Yii::t('app', 'Pg Name'),
            'p_code' => Yii::t('app', 'Pr Code'),
            'p_abbr' => Yii::t('app', 'Pr Abbr'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grade::className(), ['g_program_id' => 'p_id']);
    }

    public function getGradesCount()
    {
        return $this->getGrades()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plan::className(), ['p_grade_id' => 'g_id'])->viaTable(Grade::tableName(), ['g_program_id' => 'p_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->hasMany(Discipline::className(), ['d_id' => 'p_discipline_id'])->via('plans');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->hasMany(Form::className(), ['f_id' => 'p_form_id'])->via('plans');
    }

    public static function listAll($field = 'p_name')
    {
        $query = self::find()->orderBy(['p_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'p_id', $field);
    }
}
