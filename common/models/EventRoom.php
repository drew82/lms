<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "event__room".
 *
 * @property integer $er_id
 * @property integer $er_event_id
 * @property integer $er_room_id
 *
 * @property Event $event
 * @property Room $room
 */
class EventRoom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event__room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['er_event_id', 'er_room_id'], 'required'],
            [['er_event_id', 'er_room_id'], 'integer'],
            [['er_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['er_event_id' => 'e_id']],
            [['er_room_id'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['er_room_id' => 'r_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'er_id' => Yii::t('app', 'Er ID'),
            'er_event_id' => Yii::t('app', 'Er Event ID'),
            'er_room_id' => Yii::t('app', 'Er Room ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['e_id' => 'er_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['r_id' => 'er_room_id']);
    }
}
