<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "visit".
 *
 * @property integer $v_id
 * @property integer $v_student_id
 * @property integer $v_event_id
 * @property double $v_presence
 * @property double $v_mark
 *
 * @property Student $student
 * @property Event $event
 */
class Visit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'visit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['v_student_id', 'v_event_id', 'v_presence', 'v_mark'], 'required'],
            [['v_student_id', 'v_event_id'], 'integer'],
            [['v_presence', 'v_mark'], 'number'],
            [['v_student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['v_student_id' => 's_id']],
            [['v_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['v_event_id' => 'e_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'v_id' => Yii::t('app', 'V ID'),
            'v_student_id' => Yii::t('app', 'V Student ID'),
            'v_event_id' => Yii::t('app', 'V Event ID'),
            'v_presence' => Yii::t('app', 'V Presence'),
            'v_mark' => Yii::t('app', 'V Mark'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['s_id' => 'v_student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['e_id' => 'v_event_id']);
    }
}
