<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "room_type".
 *
 * @property integer $t_id
 * @property string $t_name
 * @property string $t_class
 * @property string $t_config
 *
 * @property Room[] $rooms
 *
 * @property rooms\RoomInterface $plugin
 */
class RoomType extends \yii\db\ActiveRecord
{
    protected $_plugin;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_name', 't_class'], 'required'],
            [['t_name', 't_class', 't_config'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            't_id' => Yii::t('app', 'RT ID'),
            't_name' => Yii::t('app', 'RT Name'),
            't_class' => Yii::t('app', 'RT Class'),
            't_config' => Yii::t('app', 'RT Config'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['r_type_id' => 't_id']);
    }

    public function getPlugin()
    {
        if (!$this->_plugin) {
            $class = 'common\\models\\rooms\\' . $this->t_class;
            $config = parse_ini_string($this->t_config);
            $this->_plugin = new $class($config);
        }

        return $this->_plugin;
    }

    public static function listAll($field = 't_name')
    {
        $query = self::find()->orderBy(['t_name' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 't_id', $field);
    }

    public static function listClasses()
    {
        $classes = [];
        $files = \yii\helpers\FileHelper::findFiles(__DIR__ . '/rooms/', ['except' => ['BaseRoom.php', 'RoomInterface.php']]);

        foreach ($files as $path) {
            $classes[] = [
                'file' => basename($path, '.php'),
            ];
        }

        return \yii\helpers\ArrayHelper::map($classes, 'file', 'file');
    }
}
