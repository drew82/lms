<?php

namespace common\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "event".
 *
 * @property integer $e_id
 * @property integer $e_worker_id
 * @property integer $e_discipline_id
 * @property integer $e_form_id
 * @property integer $e_mode_id
 * @property string $e_dt_start
 * @property string $e_dt_end
 * @property string $e_comment
 * 
 * @property DateTime $dtStart
 * @property DateTime $dtEnd
 * @property boolean $isOnline
 * @property boolean $isPassed
 *
 * @property Worker $worker
 * @property Discipline $discipline
 * @property Form $form
 * @property Mode $mode
 * @property Visit[] $visits
 *
 * @property EventGroup[] $eventGroups
 * @property Group[] $groups
 * @property EventRoom[] $eventRooms
 * @property Room[] $rooms
 * @property EventFile[] $eventFiles
 * @property File[] $files
 * @property Raise[] $raises
 * @property Student[] $students
 * 
 * @property integer $duration event duration in seconds
 * 
 * @property array $groupIds
 * @property array $roomIds
 */
class Event extends \yii\db\ActiveRecord
{
    const ACADEMIC_HOUR_SECONDS = 2700;

    public $date;
    public $pair_id;
    protected $_groupIds;
    protected $_roomIds;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_worker_id', 'e_discipline_id', 'e_form_id', 'e_mode_id'], 'required'],
            [['e_worker_id', 'e_discipline_id', 'e_form_id', 'e_mode_id'], 'integer'],
            [['e_comment'], 'string', 'max' => 255],
            // start-end
            [['date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['pair_id'], 'integer'],
            [['pair_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => Pairgrid::className(), 'targetAttribute' => ['pair_id' => 'p_id']],
            [['e_dt_start'], 'default', 'value' => function ($model, $attributes) {
                return sprintf('%s %s', $this->date, Pairgrid::findOne($this->pair_id)->p_start);
            }, 'skipOnError' => true, 'when' => function ($model) {
                return $model->date && $model->pair_id;
                }],
            [['e_dt_end'], 'default', 'value' => function ($model, $attributes) {
                return sprintf('%s %s', $this->date, Pairgrid::findOne($this->pair_id)->p_end);
            }, 'skipOnError' => true, 'when' => function ($model) {
                return $model->date && $model->pair_id;
            }],
            [['e_dt_start', 'e_dt_end'], 'required'],
            [['e_dt_start', 'e_dt_end'], 'date', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            [['e_dt_start'], 'compare', 'compareAttribute' => 'e_dt_end', 'operator' => '<', 'when' => function ($model) { return $model->e_dt_end; }],
            [['e_dt_end'], 'compare', 'compareAttribute' => 'e_dt_start', 'operator' => '>', 'when' => function ($model) { return $model->e_dt_start; }],
            // related
            [['e_worker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Worker::className(), 'targetAttribute' => ['e_worker_id' => 'w_id']],
            [['e_discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['e_discipline_id' => 'd_id']],
            [['e_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Form::className(), 'targetAttribute' => ['e_form_id' => 'f_id']],
            [['e_mode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mode::className(), 'targetAttribute' => ['e_mode_id' => 'm_id']],
            // groups
            [['groupIds'], 'required'],
            [['groupIds'], 'each', 'rule' => ['integer']],
            // rooms
            [['roomIds'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'e_id' => Yii::t('app', 'E ID'),
            'e_worker_id' => Yii::t('app', 'E Worker ID'),
            'e_discipline_id' => Yii::t('app', 'E Discipline ID'),
            'e_form_id' => Yii::t('app', 'E Form ID'),
            'e_mode_id' => Yii::t('app', 'E Mode ID'),
            'e_dt_start' => Yii::t('app', 'E Dt Start'),
            'e_dt_end' => Yii::t('app', 'E Dt End'),
            'e_comment' => Yii::t('app', 'E Comment'),
        ];
    }

    public static function find()
    {
        return new EventQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['w_id' => 'e_worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['d_id' => 'e_discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Form::className(), ['f_id' => 'e_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMode()
    {
        return $this->hasOne(Mode::className(), ['m_id' => 'e_mode_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visit::className(), ['v_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventGroups()
    {
        return $this->hasMany(EventGroup::className(), ['eg_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRooms()
    {
        return $this->hasMany(EventRoom::className(), ['er_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventFiles()
    {
        return $this->hasMany(EventFile::className(), ['ef_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['g_id' => 'eg_group_id'])->viaTable(EventGroup::tableName(), ['eg_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Room::className(), ['r_id' => 'er_room_id'])->viaTable(EventRoom::tableName(), ['er_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['f_id' => 'ef_file_id'])->viaTable(EventFile::tableName(), ['ef_event_id' => 'e_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaises()
    {
        return $this->hasMany(Raise::className(), ['r_grade_id' => 'g_id'])->includeRange($this->e_dt_start, $this->e_dt_end)->via('groups');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['s_id' => 'r_student_id'])->via('raises');
    }

    public function getGroupIds()
    {
        if (!$this->_groupIds) {
            $this->_groupIds = \yii\helpers\ArrayHelper::getColumn($this->eventGroups, 'eg_group_id');
        }

        return $this->_groupIds;
    }

    public function setGroupIds($arr)
    {
        $this->_groupIds = $arr;
    }

    public function getRoomIds()
    {
        if (!$this->_roomIds) {
            $this->_roomIds = \yii\helpers\ArrayHelper::getColumn($this->eventRooms, 'er_room_id');
        }

        return $this->_roomIds;
    }

    public function setRoomIds($arr)
    {
        $this->_roomIds = $arr;
    }

    public function getDtStart()
    {
        return new DateTime($this->e_dt_start);
    }

    public function getDtEnd()
    {
        return new DateTime($this->e_dt_end);
    }

    public function getDuration()
    {
        return $this->getDtEnd()->getTimestamp() - $this->getDtStart()->getTimestamp();
    }

    public function getIsOnline()
    {
        $now = new DateTime(Yii::$app->timeZone);
        $shift_before = new \DateInterval(sprintf('PT%dS', Yii::$app->params['event.shift.before']));
        $shift_after = new \DateInterval(sprintf('PT%dS', Yii::$app->params['event.shift.after']));

        return $now >= $this->dtStart->sub($shift_before) && $now <= $this->dtEnd->add($shift_after);
    }

    public function getIsPassed()
    {
        return new DateTime(Yii::$app->timeZone) >= $this->dtEnd;
    }

    public function linkGroups()
    {
        $this->unlinkAll('groups', true);
        foreach ($this->groupIds as $g_id) {
            $this->link('groups', Group::findOne($g_id));
        }
    }

    public function linkRooms()
    {
        $this->unlinkAll('rooms', true);
        foreach ($this->roomIds as $r_id) {
            $this->link('rooms', Room::findOne($r_id));
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->linkGroups();
        $this->linkRooms();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        foreach ($this->eventGroups as $eg) {
            $eg->delete();
        }

        foreach ($this->eventRooms as $er) {
            $er->delete();
        }

        foreach ($this->eventFiles as $ef) {
            $ef->delete();
        }

        return parent::beforeDelete();
    }
}
