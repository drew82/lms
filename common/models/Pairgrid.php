<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pairgrid".
 *
 * @property integer $p_id
 * @property string $p_name
 * @property string $p_start
 * @property string $p_end
 * 
 * @property \DateTime $start
 * @property \DateTime $end
 */
class Pairgrid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pairgrid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['p_name', 'p_start', 'p_end'], 'required'],
            [['p_start', 'p_end'], 'safe'],
            [['p_name'], 'string', 'max' => 255],
            [['p_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'p_id' => Yii::t('app', 'Pr ID'),
            'p_name' => Yii::t('app', 'Pr Name'),
            'p_start' => Yii::t('app', 'Pr Start'),
            'p_end' => Yii::t('app', 'Pr End'),
        ];
    }

    public function getStart()
    {
        return new \DateTime($this->p_start);
    }

    public function getEnd()
    {
        return new \DateTime($this->p_end);
    }

    public function getDuration()
    {
        return round(($this->getEnd()->getTimestamp() - $this->getStart()->getTimestamp()) / 60);
    }

    public function getInterval()
    {
        return sprintf('%s - %s', $this->getStart()->format('H:i'), $this->getEnd()->format('H:i'));
    }

    public static function listAll($field = 'interval')
    {
        $query = self::find()->orderBy(['p_start' => SORT_ASC]);

        return \yii\helpers\ArrayHelper::map($query->all(), 'p_id', $field);
    }
}
