<?php

namespace common\models;

class EventQuery extends \yii\db\ActiveQuery
{
    public function currentAndFuture()
    {
        return $this->andWhere(['>=', 'e_dt_end', $this->_now()]);
    }

    public function future()
    {
        return $this->andWhere(['>=', 'e_dt_start', $this->_now()]);
    }

    public function passed()
    {
        return $this->andWhere(['<=', 'e_dt_end', $this->_now()]);
    }

    public function groups(array $ids)
    {
        return $this->joinWith('groups')->andWhere(['in', 'g_id', $ids]);
    }

    protected function _now()
    {
        return (new \DateTime(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
    }
}
