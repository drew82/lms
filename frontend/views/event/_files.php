<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $event common\models\Event */
?>

<?php
foreach ($event->eventFiles as $ef) {
    echo Html::a(Html::img(sprintf('/i/f/%s.png', $ef->file->type->plugin->getLogo())) . $ef->file->f_descr,
                ['event-file/view', 'id' => $ef->ef_id],
                [
                    'data' => [
                        'mime' => $ef->file->type->plugin->mime,
                        'url' => $ef->file->type->plugin->getUrl($ef),
                    ],
                ]);
}
?>
