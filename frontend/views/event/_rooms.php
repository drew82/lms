<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $event common\models\Event */
?>

<div class="<?= $event->isOnline ? 'online' : 'offline' ?>">

    <?php
    foreach ($event->eventRooms as $er) {
        echo Html::a(Html::img(sprintf('/i/r/%s.png', $er->room->type->plugin->getLogo())) . $er->room->r_name,
                    ['event-room/enter', 'id' => $er->er_id]);
    }
    ?>

</div>