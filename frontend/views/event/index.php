<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use kop\y2sp\ScrollPager;
use yii\bootstrap\Modal;
use nterms\mediaelement\MediaElementAsset;
use nterms\mediaelement\MediaElement;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if ('future' == $type) {
    $this->registerMetaTag(['http-equiv' => 'refresh', 'content' => 300]);
}
$this->registerJsFile('@web/js/event-list.js', ['depends' => [MediaElementAsset::className()]]);
$this->registerCssFile('@web/css/event-list.css');

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Modal::begin(['id' => 'modal', 'size' => Modal::SIZE_LARGE, 'header' => '<i></i>']); ?>
    <?= MediaElement::widget([
        'mediaOptions' => ['id' => 'player'],
        'width' => '568',
        'height' => '378',
    ]);
    ?>
<?php Modal::end(); ?>

<div class="event-index">

    <?php $groups = $searchModel->daterange_id ? common\models\Daterange::findOne($searchModel->daterange_id)->getGroups() : Group::find(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'options' => [
            'class' => 'responsive-table',
        ],
        'tableOptions' => [
            'class' => 'table table-striped',
        ],
        'columns' => [
            [
                'attribute' => 'e_dt_start',
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    $f = Yii::$app->formatter;
                    return sprintf('%s<br />(%s)<br />%s &minus; %s',
                            $f->asDate($e->e_dt_start), $f->asDate($e->e_dt_start, 'EEEE'),
                            $f->asTime($e->e_dt_start), $f->asTime($e->e_dt_end));
                },
                'label' => Yii::t('app', 'Date / time'),
                'format' => 'html',
                'contentOptions' => ['class' => 'text-center text-nowrap'],
            ],
            [
                'attribute' => 'group_id',
                'filter' => ArrayHelper::map($groups->orderBy('g_name')->all(), 'g_id', 'g_name'),
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    return implode('<br />', ArrayHelper::getColumn($e->getGroups()->orderBy('g_name')->all(), 'g_name'));
                },
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
                'contentOptions' => ['class' => 'text-nowrap'],
            ],
            [
                'attribute' => 'discipline',
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    return $e->discipline->d_name . ($e->e_comment ? Html::tag('br') . $e->e_comment : '');
                },
                'label' => Yii::t('app', 'D Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'e_worker_id',
                'value' => function ($e) {
                    /* @var $e common\models\Event */
                    return sprintf('%s', $e->worker->nameFull);
                },
                'filter' => \common\models\Worker::listAll('nameFull', 'w_name_f'),
                'label' => Yii::t('app', 'W Name Full'),
            ],
            [
                'attribute' => 'e_form_id',
                'value' => function ($e) {
                    /* @var $e common\models\Event */
                    return sprintf('%s%s', 1001 == $e->mode->m_id ? 'интернет-' : '', $e->form->f_name);
                },
                'filter' => false,
                'label' => Yii::t('app', 'F Name'),
            ],
            [
                'value' => function ($e) {
                    /* @var $e common\models\Event */
                    return $this->render('_rooms', ['event' => $e]);
                },
                'format' => 'html',
                'visible' => 'future' == $type && !Yii::$app->user->isGuest,
                'contentOptions' => ['class' => 'rooms'],
            ],
            [
                'value' => function ($e) {
                    /* @var $e common\models\Event */
                    return $this->render('_files', ['event' => $e]);
                },
                'format' => 'raw',
                'visible' => 'passed' == $type && !Yii::$app->user->isGuest,
                'contentOptions' => ['class' => 'files'],
            ],
        ],
        'pager' => [
            'class' => ScrollPager::className(),
            'container' => '.responsive-table tbody',
            'item' => 'tr',
            'delay' => 0,
            'negativeMargin' => 150,
            'triggerOffset' => PHP_INT_MAX,
            'enabledExtensions' => [
                ScrollPager::EXTENSION_TRIGGER,
                ScrollPager::EXTENSION_NONE_LEFT,
            ],
        ],
    ]); ?>

</div>
