<?php

namespace frontend\controllers;

use Yii;
use common\models\Student;
use common\models\Worker;
use common\models\Event;
use frontend\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

class EventController extends Controller
{

    /**
     * Lists all future events (including current).
     * @return mixed
     */
    public function actionFuture()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->future(ArrayHelper::merge(['EventSearch' => $this->defaultValues()], Yii::$app->request->queryParams));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => 'future',
        ]);
    }

    /**
     * Lists all passed events.
     * @return mixed
     */
    public function actionPassed()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->passed(ArrayHelper::merge(['EventSearch' => $this->defaultValues()], Yii::$app->request->queryParams));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => 'passed',
        ]);
    }

    /**
     * @return array
     */
    protected function defaultValues()
    {
        $curDr = \common\models\Daterange::current();

        return [
            'daterange_id' => $curDr ? $curDr->d_id : null,
            'group_id' => Yii::$app->user->identity instanceof Student && $curDr ? Yii::$app->user->identity->getGroupInDaterange($curDr->d_id)->one()->g_id : null,
            'e_worker_id' => Yii::$app->user->identity instanceof Worker ? Yii::$app->user->identity->w_id : null,
        ];
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
