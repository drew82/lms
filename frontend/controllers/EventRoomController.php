<?php

namespace frontend\controllers;

use Yii;
use common\models\EventRoom;
use common\models\rooms\RoomInterface;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EventRoomController extends Controller
{
    public function actionEnter($id)
    {
        $rt = $this->findModel($id);
        $user = Yii::$app->user->identity;

        if ($user instanceof \common\models\Worker) {
            $name = $user->nameFull;
            $email = $user->w_email;
            $role = RoomInterface::ATTENDEE_ROLE_PRESENTER;
        } elseif ($user instanceof \common\models\Student) {
            $name = sprintf('%s %s', $user->s_name_f, $user->s_name_i);
            $email = $user->s_email;
            $role = RoomInterface::ATTENDEE_ROLE_LISTENER;
        }

        $this->redirect($rt->room->type->plugin->getLoginUrl($rt->room, $rt->event, $name, $email, $role));
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventRoom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventRoom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
