<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;
use common\models\Daterange;
use common\models\Group;
use common\models\Discipline;

/**
 * EventSearch represents the model behind the search form about `common\models\Event`.
 */
class EventSearch extends Event
{
    public $daterange_id;
    public $group_id;
    public $discipline;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_id', 'e_worker_id', 'e_discipline_id', 'e_form_id', 'e_mode_id'], 'integer'],
            //
            ['daterange_id', 'integer'],
            ['group_id', 'integer'],
            ['discipline', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function future($params)
    {
        $query = Event::find()->currentAndFuture()
                        ->with(['discipline', 'worker', 'form', 'mode', 'eventGroups', 'eventRooms'])
                        ->orderBy(['e_dt_start' => SORT_ASC, 'e_discipline_id' => SORT_ASC]);

        return $this->search($query, $params);
    }

    public function passed($params)
    {
        $query = Event::find()->passed()
                        ->with(['discipline', 'worker', 'form', 'mode', 'eventGroups'])
                        ->orderBy(['e_dt_start' => SORT_DESC, 'e_discipline_id' => SORT_ASC]);

        return $this->search($query, $params);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param \common\models\EventQuery $query
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($query, $params)
    {
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'e_id' => $this->e_id,
            'e_worker_id' => $this->e_worker_id,
            'e_discipline_id' => $this->e_discipline_id,
            'e_form_id' => $this->e_form_id,
            'e_mode_id' => $this->e_mode_id,
        ]);

        if ($this->daterange_id) {
            $dr = Daterange::findOne($this->daterange_id);
            $query->andFilterWhere(['between', 'e_dt_start', $dr->d_from . ' 00:00:00', $dr->d_till . ' 23:59:59']);
        }

        if ($this->group_id) {
            $query->joinWith('groups')->andFilterWhere([Group::tableName() . '.g_id' => $this->group_id]);
        }

        if ($this->discipline) {
            $query->joinWith('discipline')->andFilterWhere(['like', Discipline::tableName() . '.d_name', $this->discipline]);
        }

        return $dataProvider;
    }
}
