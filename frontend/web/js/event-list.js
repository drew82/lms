
$(document).on('click', 'table a', function (event) {
    event.preventDefault();
    $('#modal').modal('show');
    var title = buildTitle($(this).parent().parent());
    $('#modal .modal-header i').html(title);
    play($(this).data('mime'), $(this).data('url'));
});

$('#modal').on('hidden.bs.modal', function () {
    $('#player')[0].player.pause();
});

function play(mime, url) {
    // remove
    $('#player')[0].player.remove();
    // set source
    $('#player').html('<source type="' + mime + '" src="' + url + '">');
    // init player
    new MediaElementPlayer("#player", {
	success: function (video) {
	    video.load();
	    video.play();
	},
    });
}

function buildTitle(tr) {
    date = tr.children('td:eq(0)').html();
    discipline = tr.children('td:eq(2)').html();

    return discipline + '<br />' + date.replace(/<br>/g, ' ');
}