<?php

namespace app\components;

use common\models\Student;
use common\models\Worker;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use yii\httpclient\Exception;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $email = ArrayHelper::getValue($attributes, 'emails.0.value');
//        $id = ArrayHelper::getValue($attributes, 'id'); // google unique client id

        // Student exist
        if ($student = Student::findOne(['s_email' => $email])) {
            Yii::$app->user->login($student, Yii::$app->params['rememberMeDuration']);
        // Worker exist
        } elseif ($worker = Worker::findOne(['w_email' => $email])) {
            Yii::$app->user->login($worker, Yii::$app->params['rememberMeDuration']);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'User "{email}" does not exist', ['email' => $email]));
        }
    }
}
