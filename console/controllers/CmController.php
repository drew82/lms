<?php

namespace console\controllers;

class CmController extends \yii\console\Controller
{
    public $client;

    public function init()
    {
		$apiKey = 'us069d96b6251982fef8176bb23fc2c3dbd80a4638';
		$this->client = new \ClickMeeting\Client\CurlClient($apiKey);

        parent::init();
    }

    public function actionPing()
    {
        var_dump($this->client->ping());
    }

    public function actionRooms()
    {
        var_dump($this->client->getConferences());
    }

    public function actionRecordings($roomId)
	{
		$rst = $this->client->getRecordings($roomId);
		print_r($rst);
	}
	
	public function actionAttendees($roomId)
	{
		$sessions = $this->client->getSessions($roomId);
		print_r($sessions);
        $lastSession = end($sessions);
        $attendees = $this->client->getSessionAttendees($roomId, $lastSession->id);
        print_r($attendees);
	}
	
	public function actionRoom($event_id)
	{
		$apiKey = 'us069d96b6251982fef8176bb23fc2c3dbd80a4638';
		$cmc = new \ClickMeeting\Client\CurlClient($apiKey);
		$interval_before = new \DateInterval('PT15M');
		$interval_after = new \DateInterval('PT1M');

//		$cnfs = $cmc->getConferences();
//		print_r($cnfs);

		$e = \common\models\Event::findOne($event_id);
//		print_r($e->attributes);
		
		$r = [
			'starts_at' => $e->dtStart->sub($interval_after)->format(\DateTime::ISO8601),
			'duration' => $e->dtStart->sub($interval_after)->diff($e->dtEnd)->format('%h:%I'),
		];
		print_r($r);
//		\Yii::$app->end();
		
//		$rst = $cmc->addConference([
//			'name' => sprintf('[%s] %s (%s)', $e->dtStart->format('Y-m-d H:i'), $e->discipline->d_name, $e->worker->name_full),
//			'room_type' => 'meeting',
//			'permanent_room' => false,
//			'access_type' => 1,
//			'custom_room_url_name' => md5($event_id . date('U')),
//			'starts_at' => $e->dtStart->sub($interval_after)->format(\DateTime::ISO8601),
//			'duration' => $e->dtStart->sub($interval_after)->diff($e->dtEnd)->format('%h:%I'),
//			'settings' => [
//				'show_on_personal_page' => false,
//				'thank_you_emails_enabled' => false,
//				'connection_tester_enabled' => false,
//				'phonegateway_enabled' => false,
//				'recorder_autostart_enabled' => true,
//				'room_invite_button_enabled' => false,
//				'social_media_sharing_enabled' => false,
//				'connection_status_enabled' => true,
//				'thank_you_page_url' => 'http://rasp.fdomgppu.ru/',
//			],
//		]);
//		var_dump($rst);
		
		do {
		$url1 = $cmc->createAutologinUrl(760667, [
			'email' => 'admin@fdomgppu.ru',
			'nickname' => 'Игнашев Семен',
			'role' => 'host',
			'password' => 'DUjgsROhpKc2LHOmpJW6qTAwnP5ynRO8YKkN0WwDf9P90YQEvAP10YVt0XUDgqP80YKDiHO8YKkNDUjgsRNjomLm',
		]);
		var_dump($url1);
		} while (true);

//		$url2 = $cmc->createAutologinUrl(760667, [
//			'email' => 'kaposhkoan@fdomgppu.ru',
//			'nickname' => 'Капошко Андрей',
//			'role' => 'listener',
//		]);
//		var_dump($url2);
	}
}
