<?php

namespace console\controllers;

use Yii;
use console\models\PresenceGoogleSheet;

/**
 * Update presence google spreadsheet header
 */
class PresenceController extends \yii\console\Controller
{
    public function actionAddEvents($spreadsheetId)
    {
        /* @var $service \Google_Service_Sheets */
        $service = Yii::$app->sheets->getService();

        foreach ($service->spreadsheets->get($spreadsheetId)->sheets as $sheet) {
            //
            echo PHP_EOL . $sheet->properties->title . PHP_EOL;
            // create model
            $presenceGoogleSheet = new PresenceGoogleSheet([
                'service' => Yii::$app->sheets->getService(),
                'file' => $spreadsheetId,
                'page' => $sheet,
            ]);
            // fill data
            $presenceGoogleSheet->addEvents();
        }
    }

    public function actionListStudents($spreadsheetId)
    {
        /* @var $service \Google_Service_Sheets */
        $service = Yii::$app->sheets->getService();

        foreach ($service->spreadsheets->get($spreadsheetId)->sheets as $sheet) {
            //
            echo PHP_EOL . $sheet->properties->title . PHP_EOL;
            // create model
            $presenceGoogleSheet = new PresenceGoogleSheet([
                'service' => Yii::$app->sheets->getService(),
                'file' => $spreadsheetId,
                'page' => $sheet,
            ]);
            // fill data
            $presenceGoogleSheet->listStudents();
        }
    }

}
