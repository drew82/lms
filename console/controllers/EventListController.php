<?php

namespace console\controllers;

use console\models\EventLocal;
use console\models\EventRemote;

use yii\base\Exception;

/**
 * Sync local (DB) events list with remote (CSV)
 */
class EventListController extends \yii\console\Controller
{
    protected $_err = [];

    public function actionSync($url)
    {
        $remoteEvents = $this->readRemoteEvents($url);
        $localEvents = $this->readLocalEvents();
        //
        $addEvents = array_diff_key($remoteEvents, $localEvents);
        $delEvents = array_diff_key($localEvents, $remoteEvents);
        //
        foreach ($addEvents as $ne) {
            echo PHP_EOL;
            print_r($ne->info);
            try {
                $ne->add();
                echo 'ADDED+++' . PHP_EOL;
            } catch (Exception $exc) {
                echo 'ERROR add:' . PHP_EOL . $exc->getMessage() . PHP_EOL;
            }
        }
        //
        foreach ($delEvents as $oe) {
            echo PHP_EOL;
            print_r($oe->info);
            try {
                $oe->delete();
                echo 'REMOVED---' . PHP_EOL;
            } catch (Exception $exc) {
                echo 'ERROR remove:' . PHP_EOL . $exc->getMessage() . PHP_EOL;
            }
        }
        //
        if (!empty($this->_err)) {
            throw new Exception(implode(PHP_EOL, $this->_err));
        }
    }

    /**
     * @return EventRemote[]
     */
    protected function readRemoteEvents($url)
    {
        $ret = [];
        // read data
        $lines = (explode("\r\n", file_get_contents($url)));
        // remove header
        array_shift($lines);
        // process
        foreach ($lines as $line) {
            // read csv
            $inp = str_getcsv($line);
            //
            $ie = new EventRemote();
            $ie->inp_date = $inp[0];
            $ie->inp_groups = $inp[4];
            $ie->inp_time_start = $inp[5];
            $ie->inp_time_end = $inp[6];
            $ie->inp_discipline = $inp[7];
            $ie->inp_worker = $inp[9];
            $ie->inp_form = $inp[10];
            $ie->parse();
            //
            if ($ie->isOk()) {
                try {
                    $ret[$ie->hashMd5] = $ie;
                } catch (Exception $e) {
                    $this->_err[] = 'date=' . $inp[0] . ': ' . $e->getMessage();
                }
            }
        }

        return $ret;
    }

    /**
     * @return EventLocal[]
     */
    protected function readLocalEvents()
    {
        $ret = [];

        foreach (EventLocal::find()->all() as $e) {
            $ret[$e->hashMd5] = $e;
        }

        return $ret;
    }
}
