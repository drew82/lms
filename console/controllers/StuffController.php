<?php

namespace console\controllers;

use yii\console\Exception;
use yii\helpers\VarDumper;

/**
 * Stuff
 */
class StuffController extends \yii\console\Controller
{
    /**
     * Disciplines natural sort
     */
    public function actionDisciplinesNatsort()
    {
        \common\models\Discipline::natsort();
    }

    public function actionCreateDateranges($yearStart = null, $yearEnd = null)
    {
        // set current year by default
        $yearStart = $yearStart ? $yearStart : date('Y');
        $yearEnd = $yearEnd ? $yearEnd : date('Y');

        for ($i = $yearStart; $i <= $yearEnd; $i++) {
            // next year
            $j = $i + 1;
            // 1 sem
            $dr1 = new \common\models\Daterange();
            $dr1->d_name = sprintf('%d/%02d(%d)', $i, substr($j, 2, 2), 1);
            $dr1->d_from = $i . '-09-01';
            $dr1->d_till = $j . '-01-25';
            $this->_checkSave($dr1);
            // 2 sem
            $dr2 = new \common\models\Daterange();
            $dr2->d_name = sprintf('%d/%02d(%d)', $i, substr($j, 2, 2), 2);
            $dr2->d_from = $j . '-02-01';
            $dr2->d_till = $j . '-06-25';
            $this->_checkSave($dr2);
        }
    }

    protected function _checkSave(\yii\db\ActiveRecord $model)
    {
        if (!$model->save()) {
            throw new Exception(VarDumper::dumpAsString($model->attributes) . VarDumper::dumpAsString($model->getErrors()));
        }
    
        error_log("New %s created: %s\n", $model->className(), VarDumper::dumpAsString($model->attributes));
    }
}
