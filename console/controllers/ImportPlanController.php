<?php

namespace console\controllers;

use yii\console\Exception;
use yii\helpers\VarDumper;

use common\models\Grade;
use common\models\Discipline;
use common\models\Form;
use common\models\Plan;
use common\modules\vis\models\DisciplineMap as VisDM;

/**
 * Import data from csv-file (plan)
 */
class ImportPlanController extends \yii\console\Controller
{
    protected static $url = 'https://docs.google.com/spreadsheets/d/16S15UFzLVLyyUsXKwh8jfczQQ51yuuOhsYs2z3-QnyI/pub?gid=1594803539&single=true&output=csv';

    public function actionFromCsv($programId, $url)
    {
        $data = file($url);
        $data = array_slice($data, 4);
        foreach ($data as &$l) {
            $l = str_getcsv($l);
            if ($l[0]) {
//                print_r($l);
                $d = $this->_getDiscipline($l[1], $l[2]);
                $g = $this->_getGrade($programId, $l[14]);
                if ($l[7])
                    $p = $this->_createPlan($g, $d, $this->_getForm('Лек'), $l[7]);
                if ($l[8])
                    $p = $this->_createPlan($g, $d, $this->_getForm('Пр'), $l[8]);
                if ($l[9])
                    $p = $this->_createPlan($g, $d, $this->_getForm('Сем'), $l[9]);
                if ($l[10])
                    $p = $this->_createPlan($g, $d, $this->_getForm('СРС'), $l[10]);
                $p = $this->_createPlan($g, $d, $this->_getForm($l[21]), $l[11]);

                $dm = new VisDM();
                $dm->dm_vis_id = $l[0];
                $dm->dm_lms_id = $d->d_id;
                $dm->dm_grade_id = $g->g_id;
                $this->_checkSave($dm);
            }
        }
    }

    public function actionPlanClean()
    {
        \Yii::$app->db->createCommand()->checkIntegrity(false)->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl1_program_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl2_program_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl1_grade_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl2_grade_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl1_discipline_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{mdl2_discipline_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{vis_program_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{vis_discipline_map}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{program}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{grade}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{plan}}')->execute();
        \Yii::$app->db->createCommand()->truncateTable('{{discipline}}')->execute();
        \Yii::$app->db->createCommand()->checkIntegrity(true)->execute();
    }

    protected function _createPlan(Grade $g, Discipline $d, Form $f, $qty)
    {
        $p = new Plan();
        $p->p_grade_id = $g->g_id;
        $p->p_discipline_id = $d->d_id;
        $p->p_form_id = $f->f_id;
        $p->p_qty = $qty;
        $this->_checkSave($p);
    }

    protected function _getDiscipline($code, $name)
    {
        $d = Discipline::findOne([
            'd_code' => $code,
            'd_name' => $name,
        ]);

        if (!$d) {
            $d = new Discipline();
            $d->d_code = $code;
            $d->d_name = $name;
            $this->_checkSave($d);
        }

        return $d;
    }

    protected function _getGrade($prorgam_id, $name)
    {
        $g = Grade::findOne([
                'g_program_id' => $prorgam_id,
                'g_name' => $name,
        ]);

        if (!$g) {
            $g = new Grade();
            $g->g_program_id = $prorgam_id;
            $g->g_name = $name;
            $this->_checkSave($g);
        }

        return $g;
    }

    protected function _getForm($name)
    {
        $f = Form::findOne([
                'f_name' => $name,
        ]);

        if (!$f) {
            $f = new Form();
            $f->f_name = $name;
            $f->f_type_id = FormType::find()->orderBy('t_id')->one()->t_id;
            $this->_checkSave($f);
        }

        return $f;
    }

    protected function _checkSave(\yii\db\ActiveRecord $model)
    {
        if (!$model->save()) {
            throw new Exception(VarDumper::dumpAsString($model->getErrors()) . VarDumper::dumpAsString($model->attributes));
        }
    }
}
