<?php

namespace console\controllers;

use console\models\EventLocal;
use console\models\EventRemote;

use yii\base\Exception;

/**
 * Update local (DB) events rooms and records from remote (CSV)
 */
class EventDataController extends \yii\console\Controller
{
    protected $_err = [];

    public function actionSync($url)
    {
        $remoteEvents = $this->readRemoteEvents($url);
        echo sprintf('Found %d remote events', count($remoteEvents)) . PHP_EOL;
        $localEvents = $this->readLocalEvents();
        echo sprintf('Found %d local events', count($localEvents)) . PHP_EOL;

        foreach ($remoteEvents as $hash => $re) {
            // if local event exist
            if (!array_key_exists($hash, $localEvents)) {
                continue;
            }
            // get local event
            $le = $localEvents[$hash];
            // future 
            if (!$le->isPassed) {
                $le->setRooms($re->rooms);
            // passed
            } else {
                $le->setRecordings($re->recordings);
            }
        }
        //
        if (!empty($this->_err)) {
            throw new Exception(implode(PHP_EOL, $this->_err));
        }
    }

    /**
     * @return EventRemote[]
     */
    protected function readRemoteEvents($url)
    {
        $ret = [];
        // read data
        $lines = (explode("\r\n", file_get_contents($url)));
        // remove header
        array_shift($lines);
        // process
        foreach ($lines as $line) {
            // read csv
            $inp = str_getcsv($line);
            if (!$inp[1]) continue;
            //
            $ie = new EventRemote();
            $ie->inp_date = $inp[1];
            $ie->inp_time_start = $inp[2];
            $ie->inp_time_end = $inp[3];
            $ie->inp_discipline = $inp[4];
            $ie->inp_worker = $inp[5];
            $ie->inp_form = $inp[6];
            $ie->inp_groups = $inp[7];
            $ie->inp_recordings = [$inp[8], $inp[9], $inp[10], $inp[11]];
            $ie->inp_rooms = [$inp[16], $inp[17]];
            $ie->parse();
            //
            if ($ie->isOk()) {
                try {
                    $ret[$ie->hashMd5] = $ie;
                } catch (Exception $e) {
                    $thhis->_err[] = 'guid=' . $inp[0] . ': ' . $e->getMessage();
                }
            }
        }

        return $ret;
    }

    /**
     * @return EventLocal[]
     */
    protected function readLocalEvents()
    {
        $ret = [];

        foreach (EventLocal::find()->all() as $e) {
            $ret[$e->hashMd5] = $e;
        }

        return $ret;
    }
}
