<?php

namespace console\models;

use Yii;
use common\models\Event;
use common\models\Worker;
use common\models\Discipline;
use common\models\Form;
use common\models\Mode;
use common\models\Group;
use common\models\Program;
use common\models\Room;
use common\models\File;
use yii\base\Exception;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

/**
 * @property Worker $worker
 * @property Discipline $discipline
 * @property Form $form
 * @property Mode $mode
 * @property Group[] $groups
 * @property Room[] $rooms
 * @property File[] $recordings
 *
 * @property Group $oneGroup
 * @property Program $programProbable
 *
 * @property string $dt_start
 * @property string $dt_end
 * 
 * @property string $hash
 * @property string $hashMd5
 */
class EventRemote extends \yii\base\Object
{
    public $inp_date;
    public $inp_groups;
    public $inp_time_start;
    public $inp_time_end;
    public $inp_discipline;
    public $inp_worker;
    public $inp_form;
    public $inp_recordings = [];
    public $inp_rooms = [];
    //
    public $dt_start;
    public $dt_end;
    public $comment;
    //
    protected $_hash;

    public function getHash()
    {
        if (!$this->_hash) {
            $this->_hash = sprintf('w=%s_d=%s_f=%s_s=%s_e=%s_g=%s',
                    $this->worker->w_id, $this->discipline->d_id, $this->form->f_id, $this->dt_start, $this->dt_end,
                    implode('+', ArrayHelper::getColumn($this->groups, 'g_id')));
        }

        return $this->_hash;
    }

    public function getHashMd5()
    {
        return md5($this->getHash());
    }

    public function getInfo()
    {
        return [
            $this->dt_start,
            $this->dt_end,
            implode(', ', ArrayHelper::getColumn($this->groups, 'g_name')),
            $this->discipline->name,
            $this->form->f_name,
            $this->worker->nameFull,
        ];
    }

    public function add()
    {
        $e = new Event();
        $e->e_discipline_id = $this->discipline->d_id;
        $e->e_form_id = $this->form->f_id;
        $e->e_worker_id = $this->worker->w_id;
        $e->e_mode_id = preg_match('%^интернет%ui', $this->inp_form) ? Mode::findOne(1001)->m_id : Mode::findOne(1000)->m_id;
        $e->e_dt_start = $this->dt_start;
        $e->e_dt_end = $this->dt_end;
        $e->e_comment = $this->comment;
        $e->groupIds = ArrayHelper::getColumn($this->groups, 'g_id');
        if (!$e->save()) {
            throw new Exception(VarDumper::dumpAsString($e->attributes) . VarDumper::dumpAsString($e->getErrors()));
        }
    }

    public function parse()
    {
        //
        $date = \DateTime::createFromFormat('d.m.Y', $this->inp_date)->format('Y-m-d');
        $time_start = (4 == strlen($this->inp_time_start)) ? '0' . $this->inp_time_start : $this->inp_time_start;
        $time_end = (4 == strlen($this->inp_time_end)) ? '0' . $this->inp_time_end : $this->inp_time_end;
        $this->dt_start = sprintf('%s %s:00', $date, $time_start);
        $this->dt_end = sprintf('%s %s:00', $date, $time_end);
        //
        if ('-' == $this->inp_worker) {
            $this->inp_worker = '- - -';
        }
        //
        $this->inp_worker = preg_replace('%^.+?,\s+%ui', '', $this->inp_worker);
        //
        if ('Илюхина Нина Николаевна Куляцкая Мария Георгиевна' == $this->inp_worker) {
            $this->inp_worker = 'Куляцкая Мария Георгиевна';
        }
        //
        if (preg_match('%Час\s+\w+%ui', $this->inp_discipline)) {
            $this->inp_form = 'собрание';
        }
        // separator:  discipline | comment
        if (preg_match('%^(.+?)\s*\|\s*(.+?)$%', $this->inp_discipline, $out)) {
            $this->inp_discipline = $out[1];
            $this->comment = $out[2];
        }
        //
//        $this->inp_form = preg_replace('%практическое занятие%u', 'практические занятия', $this->inp_form);
        //
        if (preg_match('%(.+?)\s+\((\d) группа\)%u', $this->inp_form, $out)) {
                $this->inp_form = $out[1];
            $this->comment = $out[2] . ' группа';
        }
        //
        foreach ($this->inp_rooms as $name => &$number) {
            if ('-' == $number || !$number) {
                unset($this->inp_rooms[$name]);
            }
            if (preg_match('%^https\://fdomgppu\.clickmeeting\.com/(.+?)$%', $number)) {
                $number = '760531';
            }
        }
    }

    public function isOk()
    {
        return !preg_match('%перенос%ui', $this->inp_discipline)
            && !preg_match('%Челпанов%ui', $this->inp_discipline);
    }

    public function getGroups()
    {
        $groups = [];
        $names = preg_split('%,\s*%', $this->inp_groups);
        sort($names);

        foreach ($names as $name) {
            if ($group = Group::findOne(['g_name' => $name])) {
                $groups[] = $group;
            } else {
                throw new Exception("Группа '$name' не существует");
            }
        }
        
        return $groups;
    }

    public function getWorker()
    {
        list($f, $i, $o) = explode(' ', $this->inp_worker);

        if (!$worker = Worker::findOne(['w_name_f' => $f, 'w_name_i' => $i, 'w_name_o' => $o])) {
            throw new Exception("Worker '$this->inp_worker' does not exist");
        }

        return $worker;
    }

    public function getForm()
    {
        $name = str_replace('интернет-', '', $this->inp_form);

        if (!$form = Form::findOne(['f_name' => $name])) {
            throw new Exception("Форма '$name' не существует");
        }

        return $form;
    }

    public function getOneGroup()
    {
        return $this->groups[0];
    }

    public function getProgramProbable()
    {
        if (!$pp = $this->oneGroup->getProgramProbable()) {
            throw new Exception(sprintf('Probable program not found for group "%s"', $this->oneGroup->g_name));
        }

        return $pp;
    }

    public function getDiscipline()
    {
        // Бакалавриат 2016 Актуализированный
        if ($this->programProbable->p_id == 1007) {
            if ($this->inp_discipline == 'Иностранный язык (английский)') {
                $this->inp_discipline = 'Иностранный язык';
//                $this->comment = 'английский';
            }
            if ($this->inp_discipline == 'Общая психология (Введение)') {
                $this->inp_discipline = 'Общая психология';
//                $this->comment = 'Введение';
            }
        }
        // Магистратура 2016 Актуализированный
        if ($this->programProbable->p_id == 1009) {
            if ($this->inp_discipline == 'Производственная практика') {
                $this->inp_discipline = 'Производственная практика (Расср.)';
//                $this->comment = '';
            }
        }
        // 1В 2014 год поступления
        if ($this->programProbable->p_id == 1004) {
            if ($this->inp_discipline == 'Производственная практика') {
                $this->inp_discipline = 'Производственная практика (рассред.)';
//                $this->comment = '';
            }
        }

        if (!$discipline = $this->programProbable->getDisciplines()->where(['d_name' => $this->inp_discipline])->one()) {
            throw new Exception(sprintf('Дисциплина "%s" не найдена в программе "%s" (группа "%s")',
                $this->inp_discipline, $this->programProbable->p_name, $this->oneGroup->g_name));
        }

        return $discipline;
    }

    public function getRecordings()
    {
        $ret = [];

        foreach ($this->inp_recordings as $path) {
            if (empty($path)) {
                continue;
            }
            switch (pathinfo($path, PATHINFO_EXTENSION)) {
                case 'flv':
                    $typeId = Yii::$app->params['recording.type.flv'];
                    break;
                case 'mp4':
                    $typeId = Yii::$app->params['recording.type.mp4'];
                    break;
                default:
                    $typeId = Yii::$app->params['recording.type.youtube'];
                    break;
            }
            if (!$file = File::findOne([
                'f_type_id' => $typeId,
                'f_worker_id' => Yii::$app->params['recording.workerId'],
                'f_path' => $path,
            ])) {
                $file = new File();
                $file->f_type_id = $typeId;
                $file->f_worker_id = Yii::$app->params['recording.workerId'];
                $file->f_dt = $this->dt_start;
                $file->f_path = $path;
                if (!$file->save()) {
                    throw new Exception(VarDumper::dumpAsString($file->attributes) . VarDumper::dumpAsString($file->getErrors()));
                }
            }
            $ret[] = $file;
        }

        return $ret;
    }

    public function getRooms()
    {
        $ret = [];

        foreach ($this->inp_rooms as $number) {
            if (!$room = Room::findOne(['r_number' => $number])) {
                throw new Exception("Комната '$number' не существует");
            }
            $ret[] = $room;
        }

        return $ret;
    }
}
