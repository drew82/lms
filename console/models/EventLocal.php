<?php

namespace console\models;

use Yii;
use common\models\Event;
use common\models\File;
use common\models\EventFile;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\base\Exception;

/**
 * @property string $hash
 * @property string $hashMd5
 */
class EventLocal extends Event
{
    public function getHash()
    {
        return sprintf('w=%s_d=%s_f=%s_s=%s_e=%s_g=%s',
            $this->e_worker_id, $this->e_discipline_id, $this->e_form_id, $this->e_dt_start, $this->e_dt_end,
            implode('+', ArrayHelper::getColumn($this->getGroups()->orderBy('g_name')->all(), 'g_id')));
    }

    public function getHashMd5()
    {
        return md5($this->getHash());
    }

    public function getInfo()
    {
        return [
            $this->e_dt_start,
            $this->e_dt_end,
            implode(', ', ArrayHelper::getColumn($this->getGroups()->orderBy('g_name')->all(), 'g_name')),
            $this->discipline->name,
            $this->form->f_name,
            $this->worker->nameFull,
        ];
    }

    /**
     * @param Room[] $rooms
     */
    public function setRooms($rooms = [])
    {
        $roomIds = ArrayHelper::getColumn($rooms, 'r_id');

        if ($roomIds != $this->roomIds) {
            $this->roomIds = $roomIds;
            $this->linkRooms();
        }
    }

    /**
     * @param File[] $newFiles
     */
    public function setRecordings($newFiles = [])
    {
        /* @var $oldFiles File[] */
        $typeIds = [Yii::$app->params['recording.type.youtube'], Yii::$app->params['recording.type.flv'], Yii::$app->params['recording.type.mp4']];
        $oldFiles = $this->getFiles()->where(['in', 'f_type_id', $typeIds])->all();

        $newIds = ArrayHelper::getColumn($newFiles, 'f_id');
        $oldIds = ArrayHelper::getColumn($oldFiles, 'f_id');

        if ($oldIds != $newIds) {
            // delete
            foreach ($oldFiles as $file) {
                EventFile::findOne(['ef_event_id' => $this->e_id, 'ef_file_id' => $file->f_id])->delete();
            }
            // add
            foreach ($newFiles as $file) {
                $ef = new EventFile();
                $ef->ef_event_id = $this->e_id;
                $ef->ef_file_id = $file->f_id;
                if (!$ef->save()) {
                    throw new Exception(VarDumper::dumpAsString([$ef->attributes, $ef->getErrors()]));
                }
            }
        }
    }
}
