<?php

namespace console\models;

use common\models\Group;
use common\models\Event;
use yii\base\Exception;

class PresenceGoogleSheet extends \yii\base\Object
{
    /**
     * @var \Google_Service_Sheets
     */
    public $service;

    /**
     * @var string
     */
    public $file;

    /**
     * @var \Google_Service_Sheets_Sheet
     */
    public $page;

    /**
     * @var string
     */
    protected $_groupNames;
    /**
     * @var integer[]
     */
    protected $_groupIds = [];
    /**
     * @var integer
     */
    protected $_lastColumn;
    /**
     * @var string
     */
    protected $_lastDate;

    public function init()
    {
        $this->_groupNames = $this->getValues('A1')[0][0];
        //
        $line = $this->getValues('2:2')[0];
        //
        $this->_lastDate = \DateTime::createFromFormat('d.m.Y', end($line))->format('Y-m-d');
        $this->_lastColumn = count($line);
        //
        echo $this->_groupNames . PHP_EOL;
        echo $this->_lastDate . PHP_EOL;
        echo $this->num2alpha($this->_lastColumn) . PHP_EOL;
        //
        parent::init();
    }

    public function listStudents()
    {
        $students = [];

        foreach ($this->getGroupIds() as $gId) {
            $group = Group::findOne($gId);
            foreach ($group->studentsCurrent as $student) {
                $students[] = [$student->s_name_f, $student->s_name_i . ' ' . $student->s_name_o, $group->g_name];
            }
        }
        usort($students, function ($a, $b) {
            return $a[0] > $b[0];
        });
        $this->setValues('A9', $students);
    }

    public function addEvents()
    {
        foreach ($this->getEvents() as $event) {
            $this->insert($event);
        }
    }

    protected function insert(Event $event)
    {
        $column = $this->num2alpha($this->_lastColumn);
        $range = $column . '1';

        $vr = [
            [$event->e_id, ''],
            [$event->dtStart->format('d.m.Y')],
            [\Yii::$app->formatter->asDate($event->dtStart, 'E')],
            [sprintf('%s-%s', $event->dtStart->format('H:i'), $event->dtEnd->format('H:i'))],
            [$event->discipline->d_name],
            [$event->worker->nameShort],
            [$event->form->f_name],
        ];

        $this->setValues($range, $vr);
        $this->_lastColumn++;
    }

    protected function getGroupIds()
    {
        if (!$this->_groupIds) {
            foreach (preg_split('%,\s*%', $this->_groupNames) as $groupName) {
                if (!$group = Group::findOne(['g_name' => $groupName])) {
                    throw new Exception("Group '$groupName' does not exist");
                }
                $this->_groupIds[] = $group->g_id;
            }
            if (empty($this->_groupIds)) {
                throw new Exception('No groups set');
            }
        }
        return $this->_groupIds;
    }

    protected function num2alpha($n)
    {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1) {
            $r = chr($n % 26 + 0x41) . $r;
        }
        return $r;
    }

    protected function getValues($range)
    {
        echo 'read: ' . $range . PHP_EOL;
        $range = $this->page->properties->title . '!' . $range;
        return $this->service->spreadsheets_values->get($this->file, $range)->values;
    }

    protected function setValues($range, $values)
    {
        echo 'write: ' . $range . PHP_EOL;
        $range = $this->page->properties->title . '!' . $range;
        $vr = new \Google_Service_Sheets_ValueRange();
        $vr->setRange($range);
        $vr->setValues($values);

        return $this->service->spreadsheets_values->update($this->file, $range, $vr, ['valueInputOption' => 'USER_ENTERED']);
    }

    protected function getEvents()
    {
        return Event::find()
            ->groups($this->getGroupIds())
            ->andWhere(['>', 'e_dt_start', $this->_lastDate . ' 23:59:00'])
            ->andWhere(['<=', 'e_dt_start', date('Y-m-d') . ' 23:59:00'])
            ->orderBy(['e_dt_start' => SORT_ASC, 'e_discipline_id' => SORT_ASC])
            ->all();
    }
}
