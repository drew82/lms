<?php

namespace backend\controllers;

use Yii;
use common\models\Program;
use backend\models\ProgramSearch;
use backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;

/**
 * ProgramController implements the CRUD actions for Program model.
 */
class ProgramController extends BaseController
{
    /**
     * Lists all Program models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProgramSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Program model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Program model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Program();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Program model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->p_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Program model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPlan($id, $pdf = false)
    {
        $model = $this->findModel($id);

        if (false === $pdf) {
            return $this->render('plan', [
                    'model' => $model,
            ]);
        }

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_DOWNLOAD,
            'content' => $this->renderPartial('plan', [
                'model' => $model,
            ]),
            'filename' => $model->p_name . '_plan.pdf',
            'cssFile' => '@backend/views/program/plan.css',
            'options' => ['title' => sprintf('%s "%s"', \Yii::t('app', 'Pg Name'), $model->p_name)],
            'methods' => [
                'SetHeader' => [ sprintf('%s "%s"', \Yii::t('app', 'Pg Name'), $model->p_name) ],
                'SetFooter' => ['{PAGENO} / {nb}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Finds the Program model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Program the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Program::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
