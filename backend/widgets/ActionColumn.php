<?php

namespace backend\widgets;

use Yii;
use yii\helpers\Html;

class ActionColumn extends \yii\grid\ActionColumn
{
    public function init()
    {
        parent::init();

        $this->headerOptions = \yii\helpers\ArrayHelper::merge($this->headerOptions, ['nowrap' => 'nowrap']);
        $this->grid->layout = "{items}\n{pager}";
        $this->grid->summary = '{begin} - {end} / {totalCount}';
        $this->header = $this->grid->renderSummary();
    }

    protected function renderFilterCellContent()
    {
        return Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success', 'data-pjax' => 0]);
    }
}
