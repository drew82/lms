<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Visit */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Visit',
]) . $model->v_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Visits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->v_id, 'url' => ['view', 'id' => $model->v_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="visit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
