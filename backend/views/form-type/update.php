<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FormType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'FormType',
]) . $model->t_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->t_id, 'url' => ['view', 'id' => $model->t_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="form-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
