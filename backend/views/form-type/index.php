<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FormTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Form Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-type-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 't_name',
                'label' => Yii::t('app', 'Name'),
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
