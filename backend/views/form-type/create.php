<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FormType */

$this->title = Yii::t('app', 'Create Form Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
