<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Forms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'f_name',
                'label' => Yii::t('app', 'Name'),
            ],
            [
                'attribute' => 'f_type_id',
                'value' => 'type.t_name',
                'filter' => \common\models\FormType::listAll(),
                'label' => Yii::t('app', 'T Name'),
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
