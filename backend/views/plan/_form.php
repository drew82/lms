<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'p_grade_id')->dropDownList(\common\models\Grade::listAll())->label(Yii::t('app', 'G Name')) ?>

    <?= $form->field($model, 'p_discipline_id')->widget(Select2::className(), [
            'data' => \common\models\Discipline::listAll(),
    ])->label(Yii::t('app', 'D Name')) ?>

    <?= $form->field($model, 'p_form_id')->dropDownList(\common\models\Form::listAll())->label(Yii::t('app', 'F Name')) ?>

    <?= $form->field($model, 'p_qty')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
