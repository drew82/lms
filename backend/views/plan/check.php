<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \backend\models\PlanCheck */

$this->title = Yii::t('app', 'Plans Check');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-check-index">

<?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-2">
            <?= $form->field($model, 'daterangeId')->label(Yii::t('app', 'Dr Name'))
                        ->dropDownList(ArrayHelper::map($model->getDateranges()->all(), 'd_id', 'd_name'), [
                            'prompt' => '',
                            'onchange' => 'this.form.submit()',
                        ]) ?>
        </div>

        <div class="col-xs-4">
            <?php if ($model->daterangeId): ?>
                <?= $form->field($model, 'groupId')->label(Yii::t('app', 'Gp Name'))
                        ->dropDownList(ArrayHelper::map($model->getGroups()->all(), 'g_id', 'g_name'), [
                            'prompt' => '',
                            'onchange' => 'this.form.submit()',
                        ]) ?>
            <?php endif; ?>
        </div>

        <div class="col-xs-6">
            <?php if ($model->groupId): ?>
                <?= $form->field($model, 'gradeId')->label(Yii::t('app', 'G Name'))
                        ->dropDownList(ArrayHelper::map($model->getGrades()->all(), 'g_id', 'name'), [
                            'prompt' => '',
                            'onchange' => 'this.form.submit()',
                        ]) ?>
            <?php endif; ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php if ($model->gradeId): ?>

    <table class="table table-bordered table-hover">
        
        <thead>
            <tr>
                <th rowspan="2"><?= Yii::t('app', 'D Code') ?></th>
                <th rowspan="2"><?= Yii::t('app', 'D Name') ?></th>
            <?php foreach ($model->getForms()->all() as $f): ?>
                <td colspan="3" class="text-center"><?= $f->f_name ?></td>
            <?php endforeach; ?>
            </tr>
            <tr>
            <?php foreach ($model->getForms()->all() as $f): ?>
                <td class="text-center small"><?= Yii::t('app', 'planned') ?></td>
                <td class="text-center small"><?= Yii::t('app', 'actual') ?></td>
                <td class="text-center small">&Delta;</td>
            <?php endforeach; ?>
            </tr>
        </thead>

        <tbody>
        <?php foreach ($model->getDisciplines()->all() as $d): ?>
            <tr>
                <td><?= $d->d_code ?></td>
                <td><?= $d->d_name ?></td>
            <?php foreach ($model->getForms()->all() as $f): ?>
                <td class="text-right"><?= $model->hideNull($model->planQty($d, $f)) ?></td>
                <td class="text-right"><?= $model->hideNull($model->actualQty($d, $f)) ?></td>
                <td class="text-right text-warning"><?= $model->signed($model->hideNull($model->deltaQty($d, $f))) ?></td>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>

<?php endif; ?>

</div>
