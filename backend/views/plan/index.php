<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Plans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
                'attribute' => 'p_grade_id',
                'value' => 'grade.name',
                'filter' => \common\models\Grade::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'p_discipline_id',
                'value' => 'discipline.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'p_discipline_id',
                    'data' => \common\models\Discipline::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
            ],
            [
                'attribute' => 'p_form_id',
                'value' => 'form.f_name',
                'filter' => \common\models\Form::listAll(),
                'label' => Yii::t('app', 'F Name'),
                'headerOptions' => ['class' => 'col-xs-2'],
            ],
            [
                'attribute' => 'p_qty',
                'headerOptions' => ['class' => 'col-xs-1'],
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
