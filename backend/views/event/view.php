<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Event */

$this->title = $model->e_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->e_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->e_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'e_id',
            'worker.nameFull',
            'discipline.name',
            'form.f_name',
            'mode.m_name',
            'e_dt_start',
            'e_dt_end',
            'e_comment',
            [
                'label' => Yii::t('app', 'Groups'),
                'value' => implode(', ', ArrayHelper::getColumn($model->groups, 'g_name')),
            ],
            [
                'label' => Yii::t('app', 'Rooms'),
                'value' => implode(', ', ArrayHelper::getColumn($model->rooms, 'r_name')),
            ],
        ],
    ]) ?>

</div>
