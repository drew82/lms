<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\EventSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'e_id') ?>

    <?= $form->field($model, 'e_worker_id') ?>

    <?= $form->field($model, 'e_discipline_id') ?>

    <?= $form->field($model, 'e_form_id') ?>

    <?= $form->field($model, 'e_mode_id') ?>

    <?php // echo $form->field($model, 'e_dt_start') ?>

    <?php // echo $form->field($model, 'e_dt_end') ?>

    <?php // echo $form->field($model, 'e_comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
