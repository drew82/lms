<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form ActiveForm */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Event',
]) . $model->e_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->e_id, 'url' => ['view', 'id' => $model->e_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="event-update-rooms">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'e_id',
            'worker.nameFull',
            'discipline.name',
            'form.f_name',
            'mode.m_name',
            'e_dt_start',
            'e_dt_end',
            'e_comment',
            [
                'label' => Yii::t('app', 'Groups'),
                'value' => implode(', ', ArrayHelper::getColumn($model->groups, 'g_name')),
            ],
            [
                'label' => Yii::t('app', 'Rooms'),
                'value' => implode(', ', ArrayHelper::getColumn($model->rooms, 'r_name')),
            ],
        ],
    ]) ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'roomIds')->widget(Select2::className(), [
        'data' => common\models\Room::listAll(),
        'pluginOptions' => [
            'multiple' => true,
        ],
    ])->label(Yii::t('app', 'Rooms')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
