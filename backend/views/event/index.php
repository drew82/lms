<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use kop\y2sp\ScrollPager;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="event-index">

    <?php
    $dateranges = [];
	foreach (\common\models\Daterange::find()->orderBy(['d_from' => SORT_DESC])->limit(6)->all() as $dr) {
		$dateranges[$dr->d_name] = [sprintf('"%s"', $dr->d_from), sprintf('"%s"', $dr->d_till)];
	}
    ?>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'e_dt_start',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'dt_start_range',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'YYYY-MM-DD',
                        ],
                        'ranges' => $dateranges,
                    ],
                ]),
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    $f = Yii::$app->formatter;
                    return sprintf('%s<br />(%s)<br />%s &minus; %s',
                            $f->asDate($e->e_dt_start), $f->asDate($e->e_dt_start, 'EEEE'),
                            $f->asTime($e->e_dt_start), $f->asTime($e->e_dt_end));
                },
                'label' => Yii::t('app', 'Date / time'),
                'format' => 'html',
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'group_id',
                'filter' => \common\models\Group::listAll(),
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    return implode('<br />', \yii\helpers\ArrayHelper::getColumn($e->getGroups()->orderBy('g_name')->all(), 'g_name'));
                },
                'label' => Yii::t('app', 'Groups'),
                'format' => 'html',
            ],
            [
                'attribute' => 'e_discipline_id',
                'value' => 'discipline.name',
                'value' => function ($e) {
                    /* @var $e \common\models\Event */
                    return $e->discipline->name . ($e->e_comment ? Html::tag('br') . Html::tag('strong', $e->e_comment) : '');
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'e_discipline_id',
                    'data' => \common\models\Discipline::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'e_worker_id',
                'value' => 'worker.nameShort',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'e_worker_id',
                    'data' => \common\models\Worker::listAll('nameShort', 'w_name_f'),
                ]),
                'label' => Yii::t('app', 'W Name Short'),
            ],
            [
                'attribute' => 'e_form_id',
                'value' => 'form.f_name',
                'filter' => \common\models\Form::listAll(),
                'label' => Yii::t('app', 'F Name'),
            ],
            [
                'attribute' => 'e_mode_id',
                'value' => 'mode.m_name',
                'filter' => \common\models\Mode::listAll(),
                'label' => Yii::t('app', 'M Name'),
            ],

            [
                'class' => 'backend\widgets\ActionColumn',
                'template' => '{view} {update} {room} {delete}',
                'buttons' => [
                    'room' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-modal-window"></span>',
                        Url::to(['event/update-rooms', 'id' => $model->e_id]));
                    },
                ],
            ],
        ],
        'pager' => [
            'class' => ScrollPager::className(),
            'container' => '.responsive-table tbody',
            'item' => 'tr',
            'delay' => 0,
            'negativeMargin' => 150,
            'triggerOffset' => PHP_INT_MAX,
            'enabledExtensions' => [
                ScrollPager::EXTENSION_TRIGGER,
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
