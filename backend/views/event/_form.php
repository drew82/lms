<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Event */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="event-form">

    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin(['enableClientValidation' => false, 'options' => ['data-pjax' => true]]); ?>

    <div class="form-group clearfix">
        <div class="col-xs-7">
            <?php $disciplineQuery = $model->e_form_id ? $model->form->getDisciplines() : common\models\Discipline::find(); ?>
            <?php $disciplines = $disciplineQuery->orderBy(['d_name' => SORT_ASC, 'd_natsort' => SORT_ASC])->all(); ?>
            <?= $form->field($model, 'e_discipline_id', ['enableClientValidation' => false])->widget(Select2::className(), [
                    'data' => ArrayHelper::map($disciplines, 'd_id', 'name'),
                    'options' => [
                        'prompt' => '',
                        'onchange' => '$(this).closest("form").submit()',
                    ],
            ])->label(Yii::t('app', 'D Name')) ?>
        </div>

        <div class="col-xs-5">
            <?= $form->field($model, 'e_worker_id')->label(Yii::t('app', 'W Name Full'))->widget(Select2::className(), [
                    'data' => common\models\Worker::listAll('nameFull', 'w_name_f'),
            ]) ?>
        </div>
    </div>

    <div class="form-group clearfix">
        <div class="col-xs-4">
            <?php $formQuery = $model->e_discipline_id ? $model->discipline->getForms() : common\models\Form::find(); ?>
            <?php $forms = $formQuery->joinWith('type')->orderBy([common\models\FormType::tableName(). '.t_name' => SORT_ASC, 'f_name' => SORT_ASC])->all(); ?>
            <?= $form->field($model, 'e_form_id')->dropDownList(ArrayHelper::map($forms, 'f_id', 'f_name', 'type.t_name'), [
                    'prompt' => '',
                    'onchange' => '$(this).closest("form").submit()',
            ])->label(Yii::t('app', 'F Name')) ?>

            <?= $form->field($model, 'e_mode_id')->label(Yii::t('app', 'M Name'))->dropDownList(common\models\Mode::listAll(), [
                    'prompt' => '',
            ]) ?>
        </div>

        <div class="col-xs-3">
            <?= $form->field($model, 'date')->widget(DatePicker::className(), ArrayHelper::merge(Yii::$container->getDefinitions()[DatePicker::className()], [
                    'pluginOptions' => [
                        'orientation' => 'bottom',
                    ],
            ]))->label(Yii::t('app', 'Date')) ?>

            <?= $form->field($model, 'pair_id')->label(Yii::t('app', 'Pr Name'))->dropDownList(common\models\Pairgrid::listAll(),[
                    'prompt' => '',
                    'onchange' => '$(this).closest("form").submit()',
            ]) ?>
        </div>

        <div class="col-xs-5">
            <?= $form->field($model, 'e_dt_start')->textInput() ?>

            <?= $form->field($model, 'e_dt_end')->textInput() ?>
        </div>
    </div>

    <?php if ($model->e_discipline_id && $model->e_form_id): ?>
        <?php $groups = \common\models\Group::find()->distinct()->joinWith(['grades.plans'])
                ->where(['plan.p_discipline_id' => $model->e_discipline_id, 'plan.p_form_id' => $model->e_form_id])
                ->orderBy('g_name')->all(); ?>
        <?= $form->field($model, 'groupIds')->widget(Select2::className(), [
            'data' => ArrayHelper::map($groups, 'g_id', 'g_name'),
            'pluginOptions' => [
                'multiple' => true,
            ],
        ])->label(Yii::t('app', 'Groups')) ?>
    <?php endif; ?>

    <?= $form->field($model, 'roomIds')->widget(Select2::className(), [
        'data' => common\models\Room::listAll(),
        'pluginOptions' => [
            'multiple' => true,
        ],
    ])->label(Yii::t('app', 'Rooms')) ?>

    <?= $form->field($model, 'e_comment')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

</div>
