<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProgramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Programs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-index">

<?php //Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'p_name',
                'label' => Yii::t('app', 'Name'),
            ],
            'p_code',
            'p_abbr',
            [
                'attribute' => 'gradesCount',
                'label' => Yii::t('app', 'Grades Count'),
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->gradesCount, Url::to(['grade/index', 'GradeSearch[g_program_id]' => $data->p_id]));
                },
            ],
            [
                'label' => Yii::t('app', 'Plans'),
                'format' => 'html',
                'value' => function ($data) {
                    return sprintf('%s %s',
                        Html::a('html', Url::to(['program/plan', 'id' => $data->p_id])),
                        Html::a('pdf', Url::to(['program/plan', 'id' => $data->p_id, 'pdf' => true])));
                },
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php //Pjax::end(); ?></div>
