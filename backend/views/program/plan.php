<?php

use yii\helpers\Html;
use common\models\Form;

/* @var $this yii\web\View */
/* @var $model common\models\Program */

$this->title = Yii::t('app', 'Pg Name') . ' "' . $model->p_name . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->p_id, 'url' => ['view', 'id' => $model->p_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Plan');

$i = 1;
?>

<h1><?= Html::encode($this->title) ?></h1>

<table class="table table-bordered table-condensed">

	<tr>
        <td colspan="3" rowspan="2" class="text-center"><?= Yii::t('app', 'D Name') ?></td>
		<td colspan="<?= $model->getForms()->study()->count() ?>" class="text-center">Объем учебной нагрузки, ак.ч.</td>
        <td colspan="<?= $model->getGrades()->count() ?>" class="text-center">Распределение по семестрам</td>
	</tr>

	<tr>
		<?php foreach ($model->getForms()->study()->all() as $f): ?>
			<td class="text-center"><svg width='10' height='100'><text transform='rotate(90,0,0)'><?= $f->f_name ?></text></svg></td>
		<?php endforeach; ?>
		<?php foreach ($model->getGrades()->byName()->all() as $g): ?>
			<td class="text-center"><?= $g->g_name ?></td>
		<?php endforeach; ?>
	</tr>

	<?php foreach ($model->getDisciplines()->orderBy('d_natsort')->all() as $d): ?>
		<tr>
			<td class="text-right"><?= $i++ ?></td>
			<td><?= $d->d_code ?></td>
			<td><?= $d->d_name ?></td>
			<?php foreach ($model->getForms()->study()->all() as $f): ?>
				<td class="text-right"><?= $model->getPlans()->discipline($d->d_id)->form($f->f_id)->sum('p_qty') ?>	</td>
			<?php endforeach; ?>
			<?php foreach ($model->getGrades()->byName()->all() as $g): ?>
				<?php $qty = $g->getPlans()->study()->discipline($d->d_id)->sum('p_qty'); ?>
				<td class="text-right"><?= $qty ? sprintf('%.1f', $qty / \common\models\Program::ZET) : '' ?></td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>

	<tr>
		<td colspan="3"></td>
		<?php foreach ($model->getForms()->study()->all() as $fs): ?>
			<td class="text-right"><?= $model->getPlans()->form($fs->f_id)->sum('p_qty') ?></td>
		<?php endforeach; ?>
		<?php foreach ($model->getGrades()->byName()->all() as $g): ?>
			<?php $qty = $g->getPlans()->study()->sum('p_qty'); ?>
			<td class="text-right"><?= $qty ? sprintf('%.1f', $qty / \common\models\Program::ZET) : '' ?></td>
		<?php endforeach; ?>
	</tr>

	<?php foreach ($model->getForms()->control()->all() as $fc): ?>
		<tr>
			<td colspan="3" class="text-right"><?= $fc->f_name ?></td>
			<td colspan="<?= $model->getForms()->study()->count() ?>"></td>
			<?php foreach ($model->grades as $g): ?>
				<?php $qty = $g->getPlans()->form($fc->f_id)->count(); ?>
				<td class="text-right"><?= $qty ? $qty : '' ?></td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>

</table>
