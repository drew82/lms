<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WorkerDiscipline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-discipline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wd_worker_id')->dropDownList(common\models\Worker::listAll('nameFull', 'w_name_f'))->label(Yii::t('app', 'W Name Full')) ?>

    <?= $form->field($model, 'wd_discipline_id')->dropDownList(common\models\Discipline::listAll())->label(Yii::t('app', 'D Name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
