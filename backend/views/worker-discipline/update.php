<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WorkerDiscipline */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Worker Discipline',
]) . $model->wd_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Worker Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wd_id, 'url' => ['view', 'id' => $model->wd_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="worker-discipline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
