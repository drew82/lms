<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\WorkerDisciplineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Worker Disciplines');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-discipline-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'wd_worker_id',
                'value' => 'worker.nameFull',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'wd_worker_id',
                    'data' => common\models\Worker::listAll('nameFull', 'w_name_f'),
                ]),
                'label' => Yii::t('app', 'W Name Full'),
            ],
            [
                'attribute' => 'wd_discipline_id',
                'value' => 'discipline.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'wd_discipline_id',
                    'data' => \common\models\Discipline::listAll(),
                ]),
                'label' => Yii::t('app', 'D Name'),
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
