<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Discipline */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Discipline',
]) . $model->d_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->d_id, 'url' => ['view', 'id' => $model->d_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="discipline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
