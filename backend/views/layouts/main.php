<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use kartik\daterange\DateRangePicker;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['name.short'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => Yii::t('app', 'Events'), 'url' => ['/event/index']],
		['label' => Yii::t('app', 'VIS'), 'items' => [
			['label' => Yii::t('app', 'Program Map'), 'url' => ['/vis/program-map/index']],
			['label' => Yii::t('app', 'Discipline Map'), 'url' => ['/vis/discipline-map/index']],
			['label' => Yii::t('app', 'Student Map'), 'url' => ['/vis/student-map/index']],
		]],
		['label' => Yii::t('app', 'MDL-1'), 'items' => [
			['label' => Yii::t('app', 'Program Map'), 'url' => ['/mdl1/program-map/index']],
			['label' => Yii::t('app', 'Grade Map'), 'url' => ['/mdl1/grade-map/index']],
			['label' => Yii::t('app', 'Discipline Map'), 'url' => ['/mdl1/discipline-map/index']],
			['label' => Yii::t('app', 'Worker Map'), 'url' => ['/mdl1/worker-map/index']],
			['label' => Yii::t('app', 'Student Map'), 'url' => ['/mdl1/student-map/index']],
			['label' => Yii::t('app', 'Enrollments'), 'url' => ['/mdl1/enrollment/index']],
		]],
		['label' => Yii::t('app', 'MDL-2'), 'items' => [
			['label' => Yii::t('app', 'Program Map'), 'url' => ['/mdl2/program-map/index']],
			['label' => Yii::t('app', 'Grade Map'), 'url' => ['/mdl2/grade-map/index']],
			['label' => Yii::t('app', 'Discipline Map'), 'url' => ['/mdl2/discipline-map/index']],
			['label' => Yii::t('app', 'Worker Map'), 'url' => ['/mdl2/worker-map/index']],
			['label' => Yii::t('app', 'Student Map'), 'url' => ['/mdl2/student-map/index']],
			['label' => Yii::t('app', 'Enrollments'), 'url' => ['/mdl2/enrollment/index']],
		]],
		['label' => Yii::t('app', 'Control'), 'items' => [
	        ['label' => Yii::t('app', 'Plans'), 'url' => ['/plan/index']],
	        ['label' => Yii::t('app', 'Raises'), 'url' => ['/raise/index']],
	        ['label' => Yii::t('app', 'Visits'), 'url' => ['/visit/index']],
		]],
		['label' => Yii::t('app', 'Library'), 'items' => [
			['label' => Yii::t('app', 'Programs'), 'url' => ['/program/index']],
			['label' => Yii::t('app', 'Grades'), 'url' => ['/grade/index']],
			['label' => Yii::t('app', 'Form Types'), 'url' => ['/form-type/index']],
			['label' => Yii::t('app', 'Forms'), 'url' => ['/form/index']],
			['label' => Yii::t('app', 'Room Types'), 'url' => ['/room-type/index']],
			['label' => Yii::t('app', 'Rooms'), 'url' => ['/room/index']],
			['label' => Yii::t('app', 'File Types'), 'url' => ['/file-type/index']],
			['label' => Yii::t('app', 'Files'), 'url' => ['/file/index']],
			['label' => Yii::t('app', 'Modes'), 'url' => ['/mode/index']],
			['label' => Yii::t('app', 'Dateranges'), 'url' => ['/daterange/index']],
			['label' => Yii::t('app', 'Pairgrids'), 'url' => ['/pairgrid/index']],
			['label' => Yii::t('app', 'Disciplines'), 'url' => ['/discipline/index']],
			['label' => Yii::t('app', 'Workers'), 'url' => ['/worker/index']],
			['label' => Yii::t('app', 'Worker Disciplines'), 'url' => ['/worker-discipline/index']],
			['label' => Yii::t('app', 'Groups'), 'url' => ['/group/index']],
			['label' => Yii::t('app', 'Students'), 'url' => ['/student/index']],
		]],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/auth', 'authclient' => 'google']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app', 'Logout') . ' [' . Yii::$app->user->identity->nameShort . ']',
                ['class' => 'btn btn-link']
            )
            . Html::endForm()
            . '</li>';
    }
	echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->params['name.short'] ?> <?= date('Y') ?></p>
        <p class="pull-left">, tz: <?= Yii::$app->getTimeZone() ?>, diff <?= date('P') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
