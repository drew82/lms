<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RoomType */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Room Type',
]) . $model->t_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Room Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->t_id, 'url' => ['view', 'id' => $model->t_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="room-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
