<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\RoomTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Room Types');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-type-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 't_name',
                'label' => Yii::t('app', 'Name'),
            ],
            [
                'attribute' => 't_class',
                'filter' => \common\models\RoomType::listClasses(),
            ],
            't_config',

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
