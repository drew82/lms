<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Daterange */

$this->title = Yii::t('app', 'Create Daterange');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dateranges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daterange-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
