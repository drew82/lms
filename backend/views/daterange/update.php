<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Daterange */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Daterange',
]) . $model->d_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dateranges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->d_id, 'url' => ['view', 'id' => $model->d_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="daterange-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
