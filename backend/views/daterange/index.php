<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\DaterangeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dateranges');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daterange-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'd_name',
                'label' => Yii::t('app', 'Name'),
            ],
            'd_from',
            'd_till',
            [
                'attribute' => 'duration',
                'value' => function ($d) {
                    /* @var $d \common\models\Daterange */
                    return Yii::$app->formatter->asDuration($d->getDuration());
                },
                'label' => Yii::t('app', 'Duration'),
            ],
            
            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
