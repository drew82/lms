<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Daterange */

$this->title = $model->d_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dateranges'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daterange-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->d_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->d_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'd_id',
            'd_name',
            'd_from',
            'd_till',
        ],
    ]) ?>

</div>
