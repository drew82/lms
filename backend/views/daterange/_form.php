<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Daterange */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daterange-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'd_name')->textInput(['maxlength' => true])->label(Yii::t('app', 'Name')) ?>

    <?= $form->field($model, 'd_from')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'd_till')->widget(DatePicker::className()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
