<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Raise */

$this->title = Yii::t('app', 'Create Raise');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Raises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raise-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
