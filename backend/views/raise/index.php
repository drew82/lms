<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RaiseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Raises');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raise-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'r_student_id',
                'value' => 'student.nameShort',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'r_student_id',
                    'data' => \common\models\Student::listAll('nameShort'),
                ]),
                'label' => Yii::t('app', 'S Name Short'),
            ],
            [
                'attribute' => 'r_grade_id',
                'value' => 'grade.name',
                'filter' => \common\models\Grade::listAll(),
                'label' => Yii::t('app', 'G Name'),
                'format' => 'html',
            ],
            [
                'attribute' => 'r_group_id',
                'value' => 'group.g_name',
                'filter' => \common\models\Group::listAll(),
                'label' => Yii::t('app', 'Gp Name'),
            ],
            [
                'attribute' => 'r_daterange_id',
                'value' => 'daterange.d_name',
                'filter' => \common\models\Daterange::listAll(),
                'label' => Yii::t('app', 'Dr Name'),
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
