<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Raise */

$this->title = $model->r_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Raises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="raise-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->r_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->r_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'r_id',
            'r_student_id',
            'r_grade_id',
            'r_daterange_id',
        ],
    ]) ?>

</div>
