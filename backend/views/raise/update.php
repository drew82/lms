<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Raise */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Raise',
]) . $model->r_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Raises'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->r_id, 'url' => ['view', 'id' => $model->r_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="raise-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
