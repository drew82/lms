<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Raise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="raise-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'r_student_id')->dropDownList(common\models\Student::listAll('nameFull', 's_name_f'))->label(Yii::t('app', 'S Name Full')) ?>

    <?= $form->field($model, 'r_grade_id')->dropDownList(common\models\Grade::listAll())->label(Yii::t('app', 'G Name')) ?>

    <?= $form->field($model, 'r_group_id')->dropDownList(common\models\Group::listAll())->label(Yii::t('app', 'Gp Name')) ?>

    <?= $form->field($model, 'r_daterange_id')->dropDownList(common\models\Daterange::listAll())->label(Yii::t('app', 'Dr Name')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
