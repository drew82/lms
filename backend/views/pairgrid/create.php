<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Pairgrid */

$this->title = Yii::t('app', 'Create Pairgrid');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pairgrids'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pairgrid-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
