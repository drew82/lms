<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\PairgridSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pairgrids');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pairgrid-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'attribute' => 'p_name',
                'label' => Yii::t('app', 'Name'),
            ],
            'p_start',
            'p_end',
            [
                'attribute' => 'duration',
                'label' => Yii::t('app', 'Pr Minutes'),
            ],

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
