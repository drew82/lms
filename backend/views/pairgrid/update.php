<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pairgrid */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pairgrid',
]) . $model->p_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pairgrids'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->p_id, 'url' => ['view', 'id' => $model->p_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pairgrid-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
