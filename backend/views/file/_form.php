<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'f_type_id')->dropDownList(\common\models\FileType::listAll())->label(Yii::t('app', 'FlT Name')) ?>

    <?= $form->field($model, 'f_worker_id')->dropDownList(common\models\Worker::listAll('nameFull'))->label(Yii::t('app', 'W Name Full')) ?>

    <?= $form->field($model, 'f_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'f_descr')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
