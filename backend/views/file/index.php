<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'f_dt',
            [
                'attribute' => 'f_type_id',
                'value' => 'type.t_name',
                'filter' => common\models\FileType::listAll(),
                'label' => Yii::t('app', 'FlT Name'),
            ],
            [
                'attribute' => 'f_worker_id',
                'value' => 'worker.nameShort',
                'filter' => common\models\Worker::listAll('nameShort'),
                'label' => Yii::t('app', 'W Name Short'),
            ],
            'f_path',
            'f_descr',

            ['class' => 'backend\widgets\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
