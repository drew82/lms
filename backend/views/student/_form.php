<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 's_name_f')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_name_i')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_name_o', ['enableClientValidation' => false])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_email', ['enableClientValidation' => false])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_mobile')->widget(PhoneInput::className(), [
        'jsOptions' => [
            'initialCountry' => Yii::$app->params['baseCountry'],
            'nationalMode' => false,
            'allowDropdown' => false,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
