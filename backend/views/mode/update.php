<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Mode */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mode',
]) . $model->m_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->m_id, 'url' => ['view', 'id' => $model->m_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mode-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
