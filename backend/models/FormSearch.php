<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Form;
use common\models\FormType;

/**
 * FormSearch represents the model behind the search form about `common\models\Form`.
 */
class FormSearch extends Form
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_id', 'f_type_id'], 'integer'],
            [['f_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Form::find()->joinWith('type');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'f_name' => [
                        'asc' => ['f_name' => SORT_ASC, FormType::tableName() . '.t_name' => SORT_ASC],
                        'desc' => ['f_name' => SORT_DESC, FormType::tableName() . '.t_name' => SORT_ASC],
                    ],
                    'f_type_id' => [
                        'asc' => [FormType::tableName() . '.t_name' => SORT_ASC, 'f_name' => SORT_ASC],
                        'desc' => [FormType::tableName() . '.t_name' => SORT_DESC, 'f_name' => SORT_ASC],
                    ],
                ],
                'defaultOrder' => ['f_type_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'f_id' => $this->f_id,
            'f_type_id' => $this->f_type_id,
        ]);

        $query->andFilterWhere(['like', 'f_name', $this->f_name]);

        return $dataProvider;
    }
}
