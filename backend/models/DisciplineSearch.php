<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Discipline;

/**
 * DisciplineSearch represents the model behind the search form about `common\models\Discipline`.
 */
class DisciplineSearch extends Discipline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_id', 'd_natsort'], 'integer'],
            [['d_code', 'd_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discipline::find(false);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'd_code' => [
                       'asc' => ['d_natsort' => SORT_ASC, 'd_name' => SORT_ASC],
                       'desc' => ['d_natsort' => SORT_DESC, 'd_name' => SORT_ASC],
                    ],
                    'd_name' => [
                       'asc' => ['d_name' => SORT_ASC, 'd_natsort' => SORT_ASC],
                       'desc' => ['d_name' => SORT_DESC, 'd_natsort' => SORT_ASC],
                    ],
                    'd_natsort',
                ],
                'defaultOrder' => ['d_name' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'd_id' => $this->d_id,
            'd_natsort' => $this->d_natsort,
        ]);

        $query->andFilterWhere(['like', 'd_code', $this->d_code])
            ->andFilterWhere(['like', 'd_name', $this->d_name]);

        return $dataProvider;
    }
}
