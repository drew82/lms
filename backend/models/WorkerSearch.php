<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Worker;

/**
 * WorkerSearch represents the model behind the search form about `common\models\Worker`.
 */
class WorkerSearch extends Worker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['w_id'], 'integer'],
            [['w_name_f', 'w_name_i', 'w_name_o', 'w_email', 'w_mobile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Worker::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'w_name_f' => [
                        'asc' => ['w_name_f' => SORT_ASC, 'w_name_i' => SORT_ASC, 'w_name_o' => SORT_ASC],
                        'desc' => ['w_name_f' => SORT_DESC, 'w_name_i' => SORT_DESC, 'w_name_o' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => ['w_name_f' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
        ]);

        $query->andFilterWhere(['like', 'w_name_f', $this->w_name_f])
            ->andFilterWhere(['like', 'w_name_i', $this->w_name_i])
            ->andFilterWhere(['like', 'w_name_o', $this->w_name_o])
            ->andFilterWhere(['like', 'w_mobile', $this->w_mobile])
            ->andFilterWhere(['like', 'w_email', $this->w_email]);

        return $dataProvider;
    }
}
