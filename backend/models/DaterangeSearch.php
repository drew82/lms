<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Daterange;

/**
 * DaterangeSearch represents the model behind the search form about `common\models\Daterange`.
 */
class DaterangeSearch extends Daterange
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_id'], 'integer'],
            [['d_name', 'd_from', 'd_till'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Daterange::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'd_name',
                    'd_from',
                    'd_till',
                ],
                'defaultOrder' => ['d_name' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'd_id' => $this->d_id,
            'd_from' => $this->d_from,
            'd_till' => $this->d_till,
        ]);

        $query->andFilterWhere(['like', 'd_name', $this->d_name]);

        return $dataProvider;
    }
}
