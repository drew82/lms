<?php

namespace backend\models;

use yii\base\Model;
use common\models\Daterange;
use common\models\Group;
use common\models\Grade;
use common\models\Discipline;
use common\models\Form;
use common\models\Plan;
use common\models\Event;

class PlanCheck extends Model
{
    public $daterangeId;
    public $groupId;
    public $gradeId;

    public function rules()
    {
        return [
            [['daterangeId', 'groupId', 'gradeId'], 'integer'],
        ];
    }

    /**
     * @return Daterange
     */
    public function getDaterange()
    {
        return Daterange::findOne($this->daterangeId);
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return Group::findOne($this->groupId);
    }

    /**
     * @return Grade
     */
    public function getGrade()
    {
        return Grade::findOne($this->gradeId);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDateranges()
    {
        return Daterange::find()->orderBy(['d_from' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->getDaterange()->getGroups()->orderBy(['g_name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->getGroup()->getGrades()->orderBy(['g_name' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForms()
    {
        return $this->getGrade()->getForms()->study();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplines()
    {
        return $this->getGrade()->getDisciplines()->orderBy(['d_natsort' => SORT_ASC]);
    }

    public function planQty(Discipline $d, Form $f)
    {
        $plan = Plan::findOne([
            'p_grade_id' => $this->gradeId,
            'p_discipline_id' => $d->d_id,
            'p_form_id' => $f->f_id,
        ]);

        return $plan ? $plan->p_qty : 0;
    }

    public function actualQty(Discipline $d, Form $f)
    {
        $seconds = 0;

        $dr = $this->getDaterange();

        /* @var $events Event[] */
        $events = Event::find()
            ->where([
                'e_discipline_id' => $d->d_id,
                'e_form_id' => $f->f_id,
            ])
            ->andWhere(['>=', 'e_dt_start', $dr->d_from . ' 00:00:00'])
            ->andWhere(['<=', 'e_dt_end', $dr->d_till . ' 23:59:59']);

        foreach ($events->all() as $e) {
            $seconds += $e->duration;
        }

        return $seconds / Event::ACADEMIC_HOUR_SECONDS;
    }

    public function deltaQty(Discipline $d, Form $f)
    {
        return $this->actualQty($d, $f) - $this->planQty($d, $f);
    }

    public function signed($value)
    {
        if ($value > 0) {
            return '+' . $value;
        } elseif ($value < 0) {
            return '&minus;' . abs($value);
        } else {
            return $value;
        }
    }

    public function hideNull($value)
    {
        return 0 == $value ? null : $value;
    }
}
