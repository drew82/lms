<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\File;

/**
 * FileSearch represents the model behind the search form about `common\models\File`.
 */
class FileSearch extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['f_id', 'f_type_id', 'f_worker_id'], 'integer'],
            [['f_dt', 'f_path', 'f_descr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['f_dt' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'f_id' => $this->f_id,
            'f_type_id' => $this->f_type_id,
            'f_worker_id' => $this->f_worker_id,
        ]);

        $query->andFilterWhere(['like', 'f_dt', $this->f_dt])
            ->andFilterWhere(['like', 'f_path', $this->f_path])
            ->andFilterWhere(['like', 'f_descr', $this->f_descr]);

        return $dataProvider;
    }
}
