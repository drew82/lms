<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Raise;

/**
 * RaiseSearch represents the model behind the search form about `common\models\Raise`.
 */
class RaiseSearch extends Raise
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['r_id', 'r_student_id', 'r_grade_id', 'r_group_id', 'r_daterange_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // join is for sorting
        $query = Raise::find()->joinWith(['student', 'daterange', 'grade.program', 'group']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'r_student_id' => [
                        'asc' => [
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'daterange.d_from' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'student.s_name_f' => SORT_DESC,
                            'student.s_name_i' => SORT_DESC,
                            'student.s_name_o' => SORT_DESC,
                            'daterange.d_from' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                    ],
                    'r_daterange_id' => [
                        'asc' => [
                            'daterange.d_from' => SORT_ASC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'daterange.d_from' => SORT_DESC,
                            'student.s_name_f' => SORT_ASC,
                            'student.s_name_i' => SORT_ASC,
                            'student.s_name_o' => SORT_ASC,
                            'grade.g_name' => SORT_ASC,
                        ],
                    ],
                ],
                'defaultOrder' => ['r_student_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'r_id' => $this->r_id,
            'r_student_id' => $this->r_student_id,
            'r_grade_id' => $this->r_grade_id,
            'r_group_id' => $this->r_group_id,
            'r_daterange_id' => $this->r_daterange_id,
        ]);

        return $dataProvider;
    }
}
