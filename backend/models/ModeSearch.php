<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Mode;

/**
 * ModeSearch represents the model behind the search form about `common\models\Mode`.
 */
class ModeSearch extends Mode
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_id'], 'integer'],
            [['m_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'm_name',
                ],
                'defaultOrder' => ['m_name' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'm_id' => $this->m_id,
        ]);

        $query->andFilterWhere(['like', 'm_name', $this->m_name]);

        return $dataProvider;
    }
}
