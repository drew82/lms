<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Grade;

/**
 * GradeSearch represents the model behind the search form about `common\models\Grade`.
 */
class GradeSearch extends Grade
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['g_id', 'g_program_id'], 'integer'],
            [['g_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Grade::find()->joinWith('program');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'g_program_id' => [
                        'asc' => ['program.p_name' => SORT_ASC, 'LENGTH([[g_name]])' => SORT_ASC, 'g_name' => SORT_ASC],
                        'desc' => ['program.p_name' => SORT_DESC, 'LENGTH([[g_name]])' => SORT_ASC, 'g_name' => SORT_ASC],
                    ],
                ],
                'defaultOrder' => ['g_program_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'g_id' => $this->g_id,
            'g_program_id' => $this->g_program_id,
        ]);

        $query->andFilterWhere(['like', 'g_name', $this->g_name]);

        return $dataProvider;
    }
}
