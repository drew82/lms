<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student;

/**
 * StudentSearch represents the model behind the search form about `common\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['s_id'], 'integer'],
            [['s_name_f', 's_name_i', 's_name_o', 's_email', 's_mobile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    's_name_f' => [
                        'asc' => ['s_name_f' => SORT_ASC, 's_name_i' => SORT_ASC, 's_name_o' => SORT_ASC],
                        'desc' => ['s_name_f' => SORT_DESC, 's_name_i' => SORT_DESC, 's_name_o' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => ['s_name_f' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            's_id' => $this->s_id,
        ]);

        $query->andFilterWhere(['like', 's_name_f', $this->s_name_f])
            ->andFilterWhere(['like', 's_name_i', $this->s_name_i])
            ->andFilterWhere(['like', 's_name_o', $this->s_name_o])
            ->andFilterWhere(['like', 's_email', $this->s_email])
            ->andFilterWhere(['like', 's_mobile', $this->s_mobile]);

        return $dataProvider;
    }
}
