<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Event;
use common\models\Group;

/**
 * EventSearch represents the model behind the search form about `common\models\Event`.
 */
class EventSearch extends Event
{
    public $dt_start_range;
    public $group_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_id', 'e_worker_id', 'e_discipline_id', 'e_form_id', 'e_mode_id'], 'integer'],
            [['e_comment', 'dt_start_range'], 'safe'],
            ['group_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find()->orderBy(['e_dt_start' => SORT_ASC, 'e_discipline_id' => SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'e_id' => $this->e_id,
            'e_worker_id' => $this->e_worker_id,
            'e_discipline_id' => $this->e_discipline_id,
            'e_form_id' => $this->e_form_id,
            'e_mode_id' => $this->e_mode_id,
            'e_dt_start' => $this->e_dt_start,
            'e_dt_end' => $this->e_dt_end,
        ]);

        $query->andFilterWhere(['like', 'e_comment', $this->e_comment]);

        if ($this->dt_start_range) {
            list($dt_L, $dt_R) = explode(' - ', $this->dt_start_range);
            $query->andFilterWhere(['between', 'e_dt_start', $dt_L . ' 00:00:00', $dt_R . ' 23:59:59']);
        }

        if ($this->group_id) {
            $query->groups([$this->group_id]);
        }

        return $dataProvider;
    }
}
