<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pairgrid;

/**
 * PairgridSearch represents the model behind the search form about `common\models\Pairgrid`.
 */
class PairgridSearch extends Pairgrid
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['p_id'], 'integer'],
            [['p_name', 'p_start', 'p_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pairgrid::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'p_name',
                    'p_start',
                    'p_end',
                ],
                'defaultOrder' => ['p_start' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'p_id' => $this->p_id,
            'p_start' => $this->p_start,
            'p_end' => $this->p_end,
        ]);

        $query->andFilterWhere(['like', 'p_name', $this->p_name]);

        return $dataProvider;
    }
}
