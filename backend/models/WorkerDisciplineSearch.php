<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\WorkerDiscipline;

/**
 * WorkerDisciplineSearch represents the model behind the search form about `common\models\WorkerDiscipline`.
 */
class WorkerDisciplineSearch extends WorkerDiscipline
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wd_id', 'wd_worker_id', 'wd_discipline_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkerDiscipline::find()->joinWith(['worker', 'discipline']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'wd_worker_id' => [
                        'asc' => [
                            'worker.w_name_f' => SORT_ASC,
                            'worker.w_name_i' => SORT_ASC,
                            'worker.w_name_o' => SORT_ASC,
                            'discipline.d_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'worker.w_name_f' => SORT_DESC,
                            'worker.w_name_i' => SORT_DESC,
                            'worker.w_name_o' => SORT_DESC,
                            'discipline.d_name' => SORT_ASC,
                        ],
                    ],
                    'wd_discipline_id' => [
                        'asc' => [
                            'discipline.d_name' => SORT_ASC,
                            'worker.w_name_f' => SORT_ASC,
                            'worker.w_name_i' => SORT_ASC,
                            'worker.w_name_o' => SORT_ASC,
                        ],
                        'desc' => [
                            'discipline.d_name' => SORT_DESC,
                            'worker.w_name_f' => SORT_ASC,
                            'worker.w_name_i' => SORT_ASC,
                            'worker.w_name_o' => SORT_ASC,
                        ],
                    ],
                ],
                'defaultOrder' => ['wd_worker_id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wd_id' => $this->wd_id,
            'wd_worker_id' => $this->wd_worker_id,
            'wd_discipline_id' => $this->wd_discipline_id,
        ]);

        return $dataProvider;
    }
}
