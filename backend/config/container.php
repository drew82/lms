<?php

Yii::$container->set('yii\grid\GridView', [
    'options' => [
        'class' => 'responsive-table',
    ],
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
]);

Yii::$container->set('kartik\date\DatePicker', [
    'type' => kartik\date\DatePicker::TYPE_INPUT,
    'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd',
        'todayHighlight' => true,
    ],
]);

Yii::$container->set('kartik\time\TimePicker', [
    'pluginOptions' => [
        'showMeridian' => false,
        'minuteStep' => 10,
    ],
]);

Yii::$container->set('kartik\daterange\DateRangePicker', [
    'pluginOptions' => [
        'locale' => [
            'format' => 'YYYY-MM-DD',
        ],
        'autoApply' => true,
    ],
]);

Yii::$container->set('kartik\select2\Select2', [
    'options' => [
        'placeholder' => '',
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'dropdownAutoWidth' => true,
    ],
]);
